<?php
Route::get('/', 'WebPageController@index')->name('u_home');

Route::get('/about', 'WebPageController@about')->name('about');

// Route::get('/about', 'WebPageController@about')->name('about');

Route::get('/achievements/{type?}', 'WebPageController@achievements')->name('achievements');

Route::get('/upcoming-batch/{type?}', 'WebPageController@upcomingBatch')->name('upcoming-batch');

Route::get('/gallery/{type?}', 'WebPageController@gallery')->name('gallery');

Route::get('/faqs/{type?}', 'WebPageController@faqs')->name('faqs');

Route::get('/smart-study/{type?}', 'WebPageController@smartStudy')->name('smart-study');

Route::get('/downloads/{type?}', 'WebPageController@downloads')->name('downloads');

Route::get('/course-pricing/{type?}', 'WebPageController@coursePrices')->name('course-pricing');

Route::get('/lecture-videos/{type?}', 'WebPageController@lectureVideos')->name('lecture-videos');

Route::get('/contact', 'WebPageController@contacts')->name('contact');

Route::get('/blogs', 'WebPageController@blogs')->name('blogs');

Route::get('/checkout/{id}','OrderController@checkout')->name('checkout');

Route::post('/placeorder','OrderController@getPay')->name('placeorder');	

Route::post('/paystatus','OrderController@orderStatus')->name('orderStatus');

Route::get('/booked/order/{id}/{status?}', 'OrderController@previous')->name('previous');


Route::get('/dashboard', function () {
	return view('samples.dashboard');
});

Route::get('/table', function () {
	return view('samples.table');
});
Route::get('/form', function () {
	return view('samples.form');
});
Route::get('/d-login', function () {
	return view('common.login');
});




Route::group(['prefix' => 'candidate'], function() {
	
	Route::group(['prefix' => 'test'], function() {

		Route::get('/register', function () {
			return view('candidates.register');
		});
		Route::get('/login', 'StudentController@index');
		
	});

});

Route::group(['prefix' => 'candidate', 'middleware' => 'candidate', 'as' => 'candidate.'], function() {

	Route::group(['prefix' => 'test', 'as' => 'test.'], function() {

		Route::get('/', 'StudentController@home')->name('c_home');
		
		Route::get('/start/{id}', 'TestController@start')->name('start');
		
		Route::get('/startTest', 'TestController@startTest')->name('startTest');
		
		Route::get('test', 'TestController@index')->name('student.test');

		Route::post('test', 'TestController@store')->name('send-test');
		
		Route::get('/finish', 'TestController@finish')->name('finish');
		
		Route::get('/solution/{id}', 'TestController@solution')->name('solution');
		// Route::get('/solution/{id}', 'StudentController@solution')->name('solution');
		
	});
	Route::post('/searchMobile', 'StudentController@searchMobile');		

	Route::get('package-can-test/{id}', 'PackageController@getCanTest')->name('pack-can-test');

});

Route::resource('test', 'TestController');

Route::resource('candidate', 'CandidateController');

Route::resource('client', 'ClientController');

Route::post('checkEmail', 'ClientController@checkEmail');

Route::post('addBType', 'ClientController@addBType');

Route::resource('degree', 'DegreeController');

Route::resource('question', 'QuestionController');

Route::group(['prefix' => 'admin', 'as' => 'admin.'], function() {

	// Route::get('/register', function () {
	// 	return view('candidates.register');
	// });
	Route::get('/login', 'AdminController@login')->name('login');


});
Route::get('get-chart', 'TestController@getChart')->name('get-chart');
Route::group(['prefix' => 'admin', 'middleware' => 'admin', 'as' => 'admin.'],function (){

	Route::get('/', 'AdminController@index')->name('home');

	Route::get('/student/create', 'StudentController@create');

	Route::get('/student/edit/{id}', 'StudentController@edit');

	Route::post('/student/edit', 'StudentController@update');

	Route::post('/student/create', 'StudentController@store');

	Route::post('/searchMobile', 'StudentController@searchMobile');

	Route::resource('question', 'QuestionController');
	
	Route::get('add-question/{sub}/{test}', 'QuestionController@addQuestion')->name('add-question');

	Route::get('all-test', 'AdminController@allTest')->name('all-test');
		
	
	Route::get('subjects/{id}', 'AdminController@subjects')->name('subjects');
	
	Route::resource('subject', 'SubjectController');
	
	Route::get('results', 'AdminController@results')->name('result');

	Route::get('test-result/{id}', 'AdminController@result');
	
	Route::get('all-candidates', 'AdminController@allCandidates')->name('all-candidates');
	// Route::get('all-candidates', 'AdminController@allCandidates')->name('all-candidates');
	// purchases
	Route::get('all-questions/{sub}/{test}', 'AdminController@allQuestions')->name('all-questions');
	
	Route::resource('test', 'TestController');

	Route::resource('package', 'PackageController');
	
	Route::resource('purchase', 'PurchaseController');

	Route::get('package-test/{id}', 'PackageController@getTest')->name('pack-test');

	Route::post('/batch', 'AdminController@storeBatch');

	Route::resource('achievement', 'AchievementController');

	Route::resource('upcoming-batch', 'UpcomingBatchController');
	
	Route::resource('gallery', 'GalleryController');
	
	Route::resource('faq', 'FaqController');
	
	Route::resource('smart-study', 'SmartStudyController');
	
	Route::resource('download', 'DownloadController');

	Route::resource('blog', 'BlogController');
	
	Route::resource('testimonial', 'TestimonialController');

	Route::resource('banner', 'BannerController');

	Route::resource('lecture', 'LectureController');
	
	Route::resource('whats-new', 'WhatsNewController');
	
	Route::resource('enquiry', 'EnquiryController');

	Route::get('ach-type', 'AchievementController@achType')->name('ach-type');

});



// Route::resource('/record', 'RecordController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Route::post('/getDatatable', 'CandidateController@getDataTable');

Route::post('/getClients', 'ClientController@getClients');

Route::post('/getClientData', 'ClientController@getClientData');

Route::get('/getRemarkBox/{id}', 'ClientController@getRemarkBox');



Route::get('/getStatus', 'CandidateController@getStatus');

Route::get('/getAllRemarks/{id}', 'ClientController@getAllRemarks');

Route::get('/getAllMobile/{id}', 'ClientController@getAllMobile');

Route::post('/designation', 'CandidateController@storeDes');

Route::get('/getFixed', 'ClientController@fixTable');

Route::group(['middleware' => 'auth'], function(){
	Route::get('/getCandidates', 'CandidateController@getCandidates')->name('getCandidates');	
	Route::get('/excel',function(){return view('excel');});

	Route::post('/ExcelUpload','CandidateController@importExcel');
});
Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
