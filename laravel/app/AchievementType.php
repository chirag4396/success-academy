<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AchievementType extends Model
{
    public $primaryKey = 'at_id';
    
    protected $fillable = ['at_title', 'at_pack_type'];

    public $timestamps = false;

    public function achievements()
    {
    	return $this->hasMany(\App\Achievement::class, 'ach_a_type');
    }
    public function type()
    {
    	return $this->belongsTo(\App\PackageType::class, 'at_pack_type');
    }
}
