<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TestLog extends Model
{
    protected $fillable = ['tlog_id', 'tlog_qus_id', 'tlog_answer', 'tlog_test_id', 'tlog_user_id', 'tlog_correct', 'tlog_start_id'];

    public $timestamps = false;

    public $primaryKey = 'tlog_id';
}
