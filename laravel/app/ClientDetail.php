<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientDetail extends Model
{
    protected $fillable = ['c_id', 'c_name', 'c_business', 'c_status', 'c_address', 'c_called', 'c_user_id', 'c_business_type', 'c_created_at', 'c_updated_at'];

    CONST CREATED_AT = 'c_created_at';

    CONST UPDATED_AT = 'c_updated_at';

    public $primaryKey = 'c_id';
    
    public function mobile(){
    	return $this->hasMany(ClientMobile::class, 'cm_client_id','c_id');
    }
    
    public function type(){
        return $this->belongsTo(ClientBusinessType::class, 'c_business_type','cbt_id');
    }
    
    public function email(){
        return $this->hasMany(ClientEmail::class, 'ce_client_id', 'c_id');
    }
    
    public function remark(){
        return $this->hasMany(ClientRemark::class,'cr_client_id' ,'c_id');
    }

    public function status(){
    	return $this->belongsTo(StatusDetail::class, 'c_status', 'sta_id');
    }
}
