<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;

class WebPageController extends Controller
{
	public function index(){
		return view('user.index');
	}

	public function about(){
		return view('user.about');
	}

	public function achievements($type = null) {
		return view('user.achievements')->with(['type' => $type]);
	}

	public function upcomingBatch($type = null){
		return view('user.upcoming-batch')->with(['type' => $type]);;
	}

	public function gallery($type = null){
		return view('user.gallery')->with(['type' => $type]);;
	}

	public function faqs($type = null){
		return view('user.faqs')->with(['type' => $type]);;
	}

	public function smartStudy($type = null){	
		return view('user.smart-study')->with(['type' => $type]);;
	}

	public function downloads($type = null){	
		return view('user.downloads')->with(['type' => $type]);;
	}

	public function coursePrices($type = null){	
		return view('user.course-prices')->with(['type' => $type]);;
	}

	public function lectureVideos($type = null){	
		return view('user.lecture-videos')->with(['type' => $type]);;
	}

	public function contacts(){	
		return view('user.contact');
	}

	public function blogs(){	
		return view('user.blogs')->with(['blogs' => Blog::paginate(1)]);
	}
}
