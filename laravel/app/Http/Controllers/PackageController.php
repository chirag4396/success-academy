<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\PackageDetail;
use App\Test;
use App\Question;
use Carbon\Carbon;
use App\Http\Traits\GetData;
class PackageController extends Controller
{
    use GetData;
    public function getTest($id){
        $tests = Test::where('test_package',$id)->get();
        $data = ['pack' => PackageDetail::find($id), 'tr' => []];
        foreach ($tests as $key => $test) {
        // echo $key;    
            array_push($data['tr'], '<tr>'
                .'<td>'.$test->test_title.'</td>'
                .'<td>'.$test->test_hours.' mins</td>'
                .'<td>'.Question::where('qus_test_id',$test->test_id)->get()->count().'</td>'
                .'<td>'.Carbon::parse($test->test_taken_at)->toFormattedDateString().'</td>'
                .'<td>'
                .'<a href="'.url('/admin/all-questions/'.$test->test_id).'" class="btn btn-success">View Questions</a> | <a href="'.url('admin/add-question/'.$test->test_id).'" class="btn btn-success">Add Questions</a>'
                .'</td>'
                .'</tr>');
        }

        return $data;
    }

    public function getCanTest($id){
        $tests = Test::where('test_package',$id)->get();
        $data = ['pack' => PackageDetail::find($id), 'tr' => []];
        foreach ($tests as $key => $test) {
            $disable = Carbon::parse($test->test_disable_at);
            $now = Carbon::now();

            $diff = $now->diffInDays($disable);
            $b = ($diff <= 0) ? '<b>Expired</b>' : '<a href="'.url('/candidate/test/start/'.$test->test_id).'" class="btn btn-success">Start Test</a>';


            array_push($data['tr'], '<tr>'
                .'<td>'.$test->test_title.'</td>'
                .'<td>'.$test->test_hours.' mins</td>'
                .'<td>'.Question::where('qus_test_id',$test->test_id)->get()->count().'</td>'
                .'<td>'.Carbon::parse($test->test_disable_at)->toFormattedDateString().'</td>'
                .'<td>'
                .$b
                .'</td>'
                .'</tr>');
        }

        return $data;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.view_packages')->with(['packs' => PackageDetail::where(['pack_delete_status' => 0])->get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create_package');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {        
        $data['msg'] = 'error';

        if(!PackageDetail::where(['pack_title' => $r->title, 'pack_type' => $r->type])->first()){
            $r->title = ucfirst($r->title);            
            $n = $this->changeKeys('pack_', $r->all());            
            $n['pack_user_id'] = Auth::id();
            $data['msg'] = PackageDetail::create($n) ? 'success' : 'error';

        }else{
            $data['msg'] = 'exist';
        }   
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.edit_package')->with(['pack' => PackageDetail::find($id)]);
        // return $this->removePrefix(PackageDetail::find($id)->toArray());        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, $id)
    {
        $data['msg'] = 'error';
        
        $pack = PackageDetail::find($id);        

        $r->title = ucfirst($r->title);            
        $n = $this->changeKeys('pack_', $r->all());

        $data['msg'] = $pack->update($n) ? 'successU' : 'error';        
        
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pack = PackageDetail::find($id);

        $data['msg'] = $pack->update(['pack_delete_status' => 1]) ? 'delete' : 'error';        

        return $data;
    }
}
