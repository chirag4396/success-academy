<?php

namespace App\Http\Controllers;

use App\WhatsNew;
use Illuminate\Http\Request;
use App\Http\Traits\GetData;
use Illuminate\Database\QueryException;

class WhatsNewController extends Controller
{
    use GetData;

    protected $res = ['msg' => 'error'];
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.view_whats_new')->with(['whatsNew' => WhatsNew::get()]);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create_whats_new');                
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        try {
            $wn = $this->changeKeys('wn_',$r->all());

            $this->res['msg'] = WhatsNew::create($wn) ? 'success' : 'error';

        } catch (QueryException $e) {
            $this->res['msg'] = 'error';        
            $this->res['error'] = $e;        
        }
        return $this->res;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WhatsNew  $whatsNew
     * @return \Illuminate\Http\Response
     */
    public function show(WhatsNew $whatsNew)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\WhatsNew  $whatsNew
     * @return \Illuminate\Http\Response
     */
    public function edit(WhatsNew $whatsNew)
    {
        return $this->removePrefix($whatsNew->toArray());                
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WhatsNew  $whatsNew
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WhatsNew $whatsNew)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WhatsNew  $whatsNew
     * @return \Illuminate\Http\Response
     */
    public function destroy(WhatsNew $whatsNew)
    {
        //
    }
}
