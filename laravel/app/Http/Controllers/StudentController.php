<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Test;
use App\Question;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;
use App\Purchase;

class StudentController extends Controller
{
    public function index(){
    	return view('common.login');
    }

    public function home(){
//        join('start_test_logs','start_test_logs.st_test_id', '=', 'tests.test_id')->where('start_test_logs.st_user_id','!=', Auth::user()->id)->
    	$now = Carbon::now('Asia/Kolkata');
    	// $gtest = Test::join('start_test_logs','start_test_logs.st_test_id', '=', 'tests.test_id')->where('start_test_logs.st_user_id','=', Auth::user()->id)->where('test_package',Auth::user()->batch)->get();
    	// $test = Test::where('test_package',Auth::user()->batch)->get();
        $pack = Purchase::where('pur_user_id',Auth::id())->get();
    	return view('candidates.test.home')->with(['packs' => $pack]);
    }

    public function solution($id){
    	$qus = Question::where('qus_test_id',$id)->get();
    	return view('candidates.test.solutions')->with(['qus' => $qus]);
    }
    public function create()
    {
        // if(Auth::guest()){
        //     return view('register');
        // }
        return view('admin.create-student');
    }
    public function store(Request $request)
    {
        return User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
            'visible_password' => $request['password'],
            'type' => $request['type'],
            'batch' => $request['batch'],
            'mobile' => $request['mobile'],
            'p_mobile' => $request['p_mobile'],
        ]) ? 'OK' : 'NO';
    }
    public function edit($id)
    {
        return view('admin.edit-student')->with(['user'=>User::find($id)]);
    }

    public function update(Request $request)
    {
        return User::where('id',$request->id)->update([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
            'visible_password' => $request['password'],
            'batch' => $request['batch'],
            'mobile' => $request['mobile'],
            'p_mobile' => $request['p_mobile']
        ]) ? 'OK' : 'NO';
    }

    public function searchMobile(Request $r){
        return User::where('mobile','like',$r->mobile)->count();
    }
}

