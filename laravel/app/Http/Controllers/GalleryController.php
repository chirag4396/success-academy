<?php

namespace App\Http\Controllers;

use App\Gallery;
use Illuminate\Http\Request;
use App\Http\Traits\GetData;
use Illuminate\Database\QueryException;

class GalleryController extends Controller
{
    use GetData;

    protected $res = ['msg' => 'error'];

    protected $path = 'images/galleries/';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.view_galleries')->with(['galleries' => Gallery::get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create_gallery');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        try {
            $gal = $this->changeKeys('gal_',$r->all());
            $gal['gal_title'] = ucfirst($gal['gal_title']);

            if ($r->hasFile('img')) {
                list($regular) = $this->uploadFiles($r, $gal['gal_title'], 'img', [$this->path]);
                $gal['gal_img'] = $regular;
            }
                        

            $this->res['msg'] = Gallery::create($gal) ? 'success' : 'error';

        } catch (QueryException $e) {
            $this->res['msg'] = 'error';
            // return $e;        
        }
        return $this->res;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function show(Gallery $gallery)
    {
        return $this->removePrefix($gallery->toArray());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function edit(Gallery $gallery)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Gallery $gallery)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function destroy(Gallery $gallery)
    {
        //
    }
}
