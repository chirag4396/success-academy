<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Test;
use App\Subject;
use App\User;
use App\Batch;

class AdminController extends Controller
{
    public function login(){
        return view('common.admin-login');
    }
    public function index(){        
    	return view('admin.index');
    }
    public function allTest(){        
        return view('admin.all-test')->with(['tests'=>Test::all()]);
    }
    public function subjects($id){        
        return view('admin.subjects')->with(['test' => Test::find($id)]);
    }
    public function allCandidates(){        
    	// return User::with('batch')->get();
        $usr = User::where('type','!=',101)->get();
        return view('admin.all-candidates')->with(['candidates'=> $usr]);
    }
    public function allQuestions($sub, $test){
        // $test = Test::with('question')->find($id);
        $subject = Subject::find($sub);
        $test = Test::find($test);

        return view('admin.view-question')->with(['subject' => $subject, 'test' => $test]);
    }
    public function results(){
        return view('admin.results')->with(['tests'=>Test::all()]);
    }
    public function result($id){
        $test = Test::find($id);
        $students =  User::with('startLog')->join('start_test_logs','start_test_logs.st_user_id','=','users.id')->where('start_test_logs.st_test_id',$test->test_id)->get();

        return view('admin.test-result')->with(['students'=>$students,'test'=>$test]);
    }
    public function storeBatch(Request $request){
        $co = Batch::where('batch_name',$request->batch_name)->count();
        if($co > 0){
            return 'exist';
        }
        return Batch::create($request->all());
    }


//    public function checkBatch(Request $r){
//    }
}
