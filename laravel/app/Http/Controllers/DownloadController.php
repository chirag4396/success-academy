<?php

namespace App\Http\Controllers;

use App\Download;
use Illuminate\Http\Request;
use App\Http\Traits\GetData;
use Illuminate\Database\QueryException;

class DownloadController extends Controller
{
    use GetData;

    protected $res = ['msg' => 'error'];

    protected $path = 'pdfs/downloads/';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.view_downloads')->with(['downloads' => Download::get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create_download');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        try {
            $d = $this->changeKeys('d_',$r->all());

            if ($r->hasFile('pdf')) {
                $exe = $r->file('pdf')->getClientOriginalExtension();
                $filename = strtolower(str_replace(' ', '-', $d['d_title'])).'-'.$d['d_type'].'.'.$exe;
                $r->file('pdf')->move($this->path, $filename);
                $d['d_pdf'] = $this->path.$filename;
            }
            
            $this->res['msg'] = Download::create($d) ? 'success' : 'error';

        } catch (QueryException $e) {
            $this->res['msg'] = 'error';
            $this->res['exc'] = $e;

        }
        return $this->res;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Download  $download
     * @return \Illuminate\Http\Response
     */
    public function show(Download $download)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Download  $download
     * @return \Illuminate\Http\Response
     */
    public function edit(Download $download)
    {
        return $this->removePrefix($download->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Download  $download
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Download $download)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Download  $download
     * @return \Illuminate\Http\Response
     */
    public function destroy(Download $download)
    {
        //
    }
}
