<?php

namespace App\Http\Controllers;

use App\Achievement;
use App\AchievementType;
use Illuminate\Http\Request;
use App\Http\Traits\GetData;
use Illuminate\Database\QueryException;

class AchievementController extends Controller
{
    use GetData;

    protected $res = ['msg' => 'error'];

    protected $path = 'images/achievements/';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.view_achievements')->with(['achievements' => Achievement::get()]);        
    }

    public function achType(Request $r){
        // return $r->all();
        return $this->removePrefix(AchievementType::where('at_pack_type', $r->id)->get()->toArray());
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create_achievement');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        try {
            $ach = $this->changeKeys('ach_',$r->all());
            $ach['ach_name'] = ucfirst($ach['ach_name']);                            

            if ($r->hasFile('img')) {
                list($regular) = $this->uploadFiles($r, $ach['ach_name'], 'img', [$this->path]);
                $ach['ach_img'] = $regular;
            }
            
            // return $ach;

            $this->res['msg'] = Achievement::create($ach) ? 'success' : 'error';

        } catch (QueryException $e) {
            $this->res['msg'] = 'error';        
        }
        return $this->res;          
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Achievement  $achievement
     * @return \Illuminate\Http\Response
     */
    public function show(Achievement $achievement)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Achievement  $achievement
     * @return \Illuminate\Http\Response
     */
    public function edit(Achievement $achievement)
    {
        return $this->removePrefix($achievement->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Achievement  $achievement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Achievement $achievement)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Achievement  $achievement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Achievement $achievement)
    {
        //
    }
}
