<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\PaymentHistory;
use App\OrderDetail;
use App\PackageDetail;
use App\Purchase;
use App\Http\Traits\GetData;
use PDF;

class OrderController extends Controller
{
    use GetData;
    protected $MERCHANT_KEY = "bsZoB71m";
    protected $SALT = "gk4bqFWSQu";
    protected $PAYU_BASE_URL = "https://secure.payu.in";
    protected $action = '';
    protected $hash = '';
    protected $txnid = '';
    protected $formError = 0;

    public function index(Request $r)
    {
        $order = OrderDetail::where('order_status', $r->type)->get();
        return view('admin.view_orders')->with(['orders' => $order]);
    }

    public function checkout($id, Request $r)
    {
//        return Package::find($id);
        list($posted, $this->txnid) = $this->listd($r);

        return view('user.pay')->with(['package' => PackageDetail::find($id), 'hash' => $this->hash, 'formError' => $this->formError, 'action' => url('placeorder'), 'MERCHANT_KEY' => $this->MERCHANT_KEY, 'txnid' => $this->txnid, 'data' => $r->all()]);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        // return OrderDetail::find($id);
        $order = OrderDetail::find($id);
        return view('admin.email.invoice')->with(['order' => $order]);
        // join('products', 'products.pro_id', '=', 'enquiries.enq_pro_id')->join('images', 'images.img_pro_id', '=', 'products.pro_id')->join('flavors', 'flavors.flavor_id', '=', 'products.pro_flavor')->where('images.img_display_priority',1)->find($id);
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }


    // public function getOrder(Request $r){
    //     list($posted, $this->txnid) = $this->listd($r);

    //     return view('user-panel.pay')->with(['hash' => $this->hash, 'formError' => $this->formError, 'action' => url('placeorder'), 'MERCHANT_KEY' => $this->MERCHANT_KEY, 'txnid' => $this->txnid]);
    // }

    public function getPay(Request $r)
    {
        // return $r->all();
        $posted = array();
        if (!empty($r)) {

            foreach ($r->all() as $key => $value) {
                $posted[$key] = $value;

            }
        }

        // parse_str($r['all'], $otherData);

        // return $posted;
        if (empty($posted['txnid'])) {
            $this->txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
        } else {

            // return $r->all();
            $userId = $this->addUser($posted);
            // $details = UserDetail::where(['ud_user_id' => $userId, 'ud_mobile' => $posted['phone']])->orWhere('ud_address', 'like', '%' . $posted['address'] . '%')->first();
            // if (!$details) {
            //     $userDetail = UserDetail::create([
            //         'ud_address' => $posted['address'],
            //         'ud_pincode' => $posted['pincode'],
            //         'ud_city' => $posted['city'],
            //         'ud_mobile' => $posted['phone'],
            //         'ud_user_id' => $userId
            //     ]);
            // } else {
            //     $userDetail = $details;
            // }

            // if ($userDetail) {
                // $grandTotal = preg_replace('/₹|\.00/', '', $otherData['grand']);
                // $shipping = preg_replace('/₹|\.00/', '', $otherData['shipping']);

            $orderDetail = OrderDetail::create([
                'order_d_id' => 'IPSA' . rand(99999, 4) . $userId,
                'order_total' => $posted['amount'],
                'order_user_id' => $userId,
                'order_status' => 1,
                'order_package' => $posted['package']
            ]);
            
                //     // return $otherData;  
                //     for ($item = 1; $item <= $otherData['itemCount']; $item++) {
                //         $d = [];
                //         $a = explode(',', $otherData['item_options_' . $item]);
                //         foreach ($a as $key => $value) {
                //             $n = explode(':', $value);
                //             $d[trim($n[0])] = $n[1];
                //         }
                //         OrderProduct::create([
                //             'op_order_id' => $orderDetail->order_id,
                //             'op_'.(isset($d['pro']) ? 'product': 'offer').'_id' => isset($d['pro']) ? $d['pro'] : $d['offer'],
                //             'op_quantity' => $otherData['item_quantity_' . $item],
                //             'op_price' => $otherData['item_price_' . $item]
                //         ]);

                //     }
                // }
            // }

            // $posted['productinfo'] = $r->all;
            // return $posted;
            // $r->session()->put('productinfo', $r->all);
            $r->session()->put('order_id', $orderDetail->order_id);
            // $r->session()->put('user_detail', $userDetail->ud_id);
            $this->txnid = $posted['txnid'];
        }
        
        $hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
        if (empty($posted['hash']) && sizeof($posted) > 0) {
            if (
                empty($posted['key'])
                || empty($posted['txnid'])
                || empty($posted['amount'])
                || empty($posted['firstname'])
                || empty($posted['email'])
                || empty($posted['phone'])
                || empty($posted['productinfo'])
                || empty($posted['surl'])
                || empty($posted['furl'])
                || empty($posted['service_provider'])
            ) {
                $this->formError = 1;
                // return $posted;
            } else {
                $hashVarsSeq = explode('|', $hashSequence);
                $hash_string = '';
                foreach ($hashVarsSeq as $hash_var) {
                    $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
                    $hash_string .= '|';
                }

                $hash_string .= $this->SALT;


                $this->hash = strtolower(hash('sha512', $hash_string));
                $this->action = $this->PAYU_BASE_URL . '/_payment';
            }
        } elseif (!empty($posted['hash'])) {
            $this->hash = $posted['hash'];
            $this->action = $this->PAYU_BASE_URL . '/_payment';
        }

        return view('user.pay')->with([
            'hash' => $this->hash,
            'formError' => $this->formError,
            'action' => $this->action,
            'MERCHANT_KEY' => $this->MERCHANT_KEY,
            'txnid' => $this->txnid,
            'posted' => $posted
        ]);
    }

    public function orderStatus(Request $r)
    {
        // return $r->all();
        $status = $r['status'];
        $firstname = $r["firstname"];
        $amount = $r["amount"];
        $txnid = $r["txnid"];
        $posted_hash = $r["hash"];
        $key = $r["key"];
        $productinfo = $r["productinfo"];
        $email = $r["email"];
        $salt = $this->SALT;

        if (isset($r["additionalCharges"])) {
            $additionalCharges = $r["additionalCharges"];
            $retHashSeq = $additionalCharges . '|' . $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;

        } else {

            $retHashSeq = $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;

        }
        $hash = hash("sha512", $retHashSeq);

        if ($hash != $posted_hash) {
            return "Invalid Transaction. Please try again";
        } else {
            $status = false;
            // parse_str($r->session()->get('productinfo'),$otherData);
            // $ud = UserDetail::find($r->session()->get('user_detail'));
            // return $r->all();
            $orderId = $r->session()->get('order_id');
            $orderDetail = OrderDetail::where('order_id', $r->session()->get('order_id'))->first();
            $orderDetail->update([
                'order_status' => ($r->status == 'success' ? 2 : 3)
            ]);

            $ph = PaymentHistory::create([
                'ph_order_id' => $orderId,
                'ph_user_id' => Auth::user()->id,
                'ph_email' => $r->email,
                'ph_phone' => is_null($r->phone) ? $ud->ud_mobile : $r->phone,
                'ph_status' => $r->status,
                'ph_txn_id' => $r->txnid,
                'ph_amount' => $r->amount,
                'ph_fullname' => $r->firstname,
                'ph_bank_ref_num' => $r->bank_ref_num,
                'ph_bankcode' => $r->bankcode,
                'ph_unmapped' => $r->unmappedstatus,
                'ph_added_on' => $r->addedon
            ]);

            parse_str($r->session()->get('productinfo'), $otherData);
            
            if ($ph) {
                
                // $pro = [];
                // $pro2 = [];
                
                // foreach ($orderDetail->products()->get() as $p) {
                //     if(!is_null($p->op_product_id)){
                //         $pro[] = str_limit($p->product->pro_title, 20);
                //         $pro2[] = $p->product->pro_title;
                //     }else{
                //         $pro[] = str_limit($p->offer->offer_title, 20);
                //         $pro2[] = $p->offer->offer_title;
                //     }
                // }
                // $all = implode(',', $pro);
                $pack = PackageDetail::find($orderDetail->order_package);
                $msg = 'Order Placed: Your order for ' . $pack->pack_title . ' with order ID ' . $orderDetail->order_d_id . ' of amount Rs.' . $orderDetail->order_total . ' has been received. We will contact you soon.';

                if ($r->status == 'success') {
                    Purchase::create([
                        'pur_user_id' => Auth::user()->id,
                        'pur_pack_id' => $orderDetail->order_package,
                        'pur_status' => 1
                    ]);
                    $this->sendSMS($ph->ph_phone, $msg);
                    // $this->sendEmail($r->email, $r->firstname, ['id' => $orderId, 'msg' => $msg], 'notification');
                    
                    $status = true;
                }

            }
            // return $otherData;
            return redirect()->route('previous',['id' => $orderDetail->order_id, 'status' => $status]);

            // return view('user-panel.previous_checkout')->with(['data' => $otherData, 'payment' => $r->all()]);
        }
    }

    public function listd(Request $r)
    {

        $posted = array();
        if (!empty($r)) {
            foreach ($r->all() as $key => $value) {
                $posted[$key] = $value;

            }
        }

        if (empty($posted['txnid'])) {
            $this->txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
        } else {
            $this->txnid = $posted['txnid'];
        }

        return [$posted, $this->txnid];
    }

    public function invoice($id)
    {
        $order = OrderDetail::find($id);
        // return view('admin.email.invoice')->with(['order' => $order]);
        $pdf = PDF::loadView('admin.email.invoice', compact('order'))->setPaper('a4')->setWarnings(false);
        return $pdf->download('invoice.pdf');
    }

    public function notification($id)
    {
        $c = CustomQuote::find($id);
        // return $c->enquiry->coe_query;
        $msg = 'Order Placed: Your order for Delectus with order ID LJ946027 of amount Rs.489.03 has been received. We will send you an update when your order is packed/shipped.';

        // return $this->sendEmail('ch.chhuchha@gmail.com', 'chirag chhuchha', $id, $msg);
        return view('admin.email.custom')->with(['cus' => CustomQuote::find($id)]);
        // ->with(['order' => OrderDetail::find($orderId)]);        
    }

    // public function custom($id)
    // {
    //     return view('user-panel.cart')->with(['cus' => CustomQuote::find($id)]);
    // }

    public function previous($id, $status = false, Request $r){
        parse_str($r->session()->get('productinfo'), $otherData);        
        $order = OrderDetail::where('order_user_id',Auth::user()->id)->find($id);
        return view('user.previous_checkout')->with(['order' => $order, 'status' => $status, 'data' => $otherData]);
    }
}
