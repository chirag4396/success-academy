<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use App\TestLog;
use App\Test;
use App\SkipDetail;
use App\StartTestLog;
use App\CandidateDetail;
use App\PackageDetail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use PDF;

class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $r)
    {
        $skippedLink = [];
        $attempted = [];
        $testLog = 1;
        $start = $r->session()->get('start');
        $test_id = $r->session()->get('test_id');
        // $se = StartTestLog::where('st_user_id',Auth::user()->id)->where('st_test_id',$test_id)->first();
        $se = StartTestLog::find($start);
        $tlog = TestLog::where('tlog_start_id', $start)->count();
        $total = Question::where('qus_test_id',$test_id)->get()->count();
        $attemptedQus = TestLog::where('tlog_start_id', $start)->where('tlog_answer', '!=', 0)->get();

        if($tlog == $total){
            $testLog = TestLog::where('tlog_start_id', $r->session()->get('start'))->where('tlog_answer', 0)->count();
        }
        // return Auth::user()->startLog[0]->st_end
        $c = Carbon::now();
        // $c = Carbon::parse(Auth::user()->startLog[0]->st_time);
        $e = Carbon::parse($se->st_end);

        $time = $c->diffInSeconds($e);

        $sec = gmdate('s',$time);
        $min = gmdate('i',$time);

        // return $times = explode(':',$d);

        // return number_format($times[1],2);
        // return Auth::user()->startLog[0]->test->test_hours;
        $questions = Question::where('qus_test_id',$test_id)->paginate(1);            

        // $skipped = SkipDetail::where('s_user_id',Auth::user()->id)->where('s_tst_id',$test_id)->get();
        $skipped = SkipDetail::where('s_start_id', $se->st_id)->get();
        foreach ($skipped as $key => $value) {
            $skippedLink[] = $value->s_qus_id;
        }
        foreach ($attemptedQus as $key => $value) {
            $attempted[] = $value->tlog_qus_id;
        }
        return view('candidates.test.test')->with([
            'questions' => $questions,
            'min' => $min,
            'sec' => $sec,
            'total' => $total,
            'skipped' => $skipped, 
            'test' => Test::find($test_id),
            'skippedLink' => $skippedLink,
            'tlog' => $testLog,
            'attempted' => $attempted
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $r)
    {
        return view('admin.create-test')->with(['pack' => PackageDetail::find($r->pack)]);
    }

    
    public function startTest(Request $request){
//        if (StartTestLog::where('st_test_id',$request->test_id)->where('st_user_id',Auth::user()->id)->first()) {
//            return 'giving';
//        }

        $testTime = Test::find($request->test_id)->test_hours;        
        $now = Carbon::now();        
        $t = StartTestLog::create([
            'st_test_id' => $request->test_id,
            'st_user_id' => Auth::user()->id,
            'st_end' => $now->addMinutes($testTime)
        ]);
        $request->session()->put('test_id', $request->test_id);
        $request->session()->put('start', $t->st_id);
        // return $t;
        return redirect()->route('candidate.test.student.test');
    }

    public function start($id){
        $test = Test::find($id);

        $disable = Carbon::parse($test->test_disable_at);

        $now = Carbon::now();

        $diff = $now->diffInDays($disable);

        if($diff <= 0){

            return redirect()->route('candidate.test.c_home');

        }

        if (StartTestLog::where('st_test_id',$id)->where('st_user_id',Auth::user()->id)->first()) {

            return view('candidates.test.start')->with(['id' => $id, 'exists' => true]);

        } 
        
        return view('candidates.test.start')->with(['id' => $id,'exists' => false]);

    }

    public function getChart(Request $r){
        
        $test_id = $r->session()->get('test_id');
    
        $start = $r->session()->get('start');
        
        $subjects = Test::find($test_id)->subjects()->get();

        foreach ($subjects as $sk => $sub) {            
        
            $questions = Question::where('qus_subject', $sub->sub_id)->get();
            
            $subQus = TestLog::where(['tlog_start_id' => $start, 'tlog_correct' => 1])->whereIn('tlog_qus_id', $questions->pluck('qus_id')->toArray())->pluck('tlog_qus_id')->toArray();

            $subjectScores[$sk]['total'] = $questions->sum('qus_id');
            
            $subjectScores[$sk]['score'] = Question::whereIn('qus_id', $subQus)->sum('qus_score');
    
            $subjectScores[$sk]['label'] = $sub->sub_title;
        
        }

        return $subjectScores;
    }

    public function finish(Request $r){

        $test_id = $r->session()->get('test_id');
        
        $start = $r->session()->get('start');

        $attemptedQus = TestLog::where('tlog_start_id', $start)->count();

        
        
        $skipped = Question::whereIn('qus_id', SkipDetail::whereIn('s_qus_id', TestLog::where('tlog_start_id', $start)->where('tlog_correct', 0)->pluck('tlog_qus_id'))->where('s_start_id', $start)->pluck('s_qus_id'));

        $attemptedTrue = TestLog::where('tlog_start_id', $start)->where('tlog_correct', 1)->count();
                
        $qus = Question::whereIn('qus_subject', Test::find($test_id)->subjects()->get()->pluck('sub_id')->toArray())->get();
        
        $scores = [];

        foreach ([0,1] as $key => $value) {
            $scores[] = Question::whereIn('qus_id', TestLog::where(['tlog_start_id' => $start, 'tlog_correct' => $value])->get()->pluck('tlog_qus_id')->toArray())->get()->sum(['qus_score']);
        }
        
        $testMarks = Question::where('qus_test_id', $test_id)->get()->sum(['qus_score']);
        
        // return $scores[0]-$skiped;

        return view('candidates.test.finish')->with([
            'attemptedQus' => $attemptedQus,
            'attemptedTrue' => $attemptedTrue,
            'skipped' => $skipped->count(),
            'trueMark' => $scores[1],
            'falseMark' => $scores[0] - $skipped->sum('qus_score'),
            'totalQus' => $qus->count(),
            'testMarks' => $testMarks,
            'qus' => $qus,
            'test_id' => $test_id
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $d = $request->all();
        foreach ($d as $key => $value) {            
            $d['test_'.$key] = $d[$key];    
            unset($d[$key]);
        }
        if(isset($request->hours)){
            $d['test_subjects'] = implode(',',$request->subjects);
            $t = Test::create($d);
            $data['msg'] = $t ? 'success' : 'NO';
            return $data;
        }
        if(!isset($request->option)){

            $request['option'] = NULL;
            
            SkipDetail::create([
                's_user_id' => Auth::user()->id,
                's_tst_id' => $request->session()->get('test_id'),
                's_qus_id' => $request['question'],
                's_page' => $request['page'],
                's_start_id' => $request->start
            ]);
        }else{
            SkipDetail::where('s_qus_id',$request['question'])->where('s_tst_id' ,$request->session()->get('test_id'))->where('s_user_id' ,Auth::user()->id)->delete();            
        }

        // return $request->all();
        $ans = ($request['option'] == Question::find($request['question'])->qus_answer);
        // var_dump($ans);
        // return $request->all();
        $te = TestLog::where(['tlog_qus_id' => $request['question'], 'tlog_start_id' => $request['start'], 'tlog_user_id' => Auth::user()->id])->first();
        if($te){
            // return $request['option'];
            if($te->tlog_answer == $request['option']){
                return 'OK';
            }
            return TestLog::where('tlog_id',$te->tlog_id)->update([
                'tlog_qus_id' => $request['question'],
                'tlog_answer' => $request['option'],
                'tlog_test_id' => $request->session()->get('test_id'),
                'tlog_user_id' => Auth::user()->id,
                'tlog_correct' => $ans
            ]) ? 'OK' : 'NO';
            
        }
        
        return TestLog::create([
            'tlog_qus_id' => $request['question'],
            'tlog_answer' => $request['option'],
            'tlog_test_id' => $request->session()->get('test_id'),
            'tlog_user_id' => Auth::user()->id,
            'tlog_correct' => $ans,
            'tlog_start_id' => $request['start']
        ]) ? 'OK' : 'NO';
        
        // return $request->all();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    // public function solution($id){
    //     $qus = Question::where('qus_test_id',$id)->get();
    //     return view('candidates.test.solutions')->with(['qus' => $qus]);
    // }
    public function solution($id)
    {
        $test = Test::find($id);
        $qus = Question::where('qus_test_id',$id)->get();
        // $order = Test::find($id);
        // return view('admin.email.invoice')->with(['order' => $order]);
        $pdf = PDF::loadView('candidates.test.solutions', compact('qus'))->setPaper('a4')->setWarnings(false);
        return $pdf->download($test->test_title.'.pdf');
    }
}
