<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Traits\GetData;
use App\ClientMobile;
use App\ClientEmail;
use App\ClientRemark;
use App\ClientDetail;
use App\StatusDetail;
use App\CallLog;
use App\ClientBusinessType;
use Yajra\Datatables\Datatables;
use Yajra\Datatables\Html\Builder;
use DB;
class ClientController extends Controller
{
	use GetData;

	public function getAllRemarks($id){
		return response()->json(ClientRemark::where('cr_client_id',$id)->orderBy('cr_id','desc')->get());
	}
	public function getAllMobile($id){
		return response()->json(ClientMobile::where('cm_client_id',$id)->orderBy('cm_id','desc')->get());
	}
	public function getClientData(Request $se){
		return ClientDetail::where('c_called',0)->where('c_user_id',Auth::user()->id)->count();
	}
	public function getClients(Request $se){
		$query = ClientDetail::with('status')->with('remark')->with('mobile')->with('email')->select('client_details.*');
		// if(isset($se->notice)){
		// 	return $query->where('c_called',0)->count();
		// }
		// if($se->data != '-1'){
		// 	$query = $query->where('c_designation',$se->data);
		// }
		$query = $query->orderBy('c_called','asc')->orderBy('c_created_at','asc')->where('c_user_id',Auth::user()->id)->get();
		return Datatables::of($query)->make(true);
	}

	public function index()
	{
		return view('clients.index');

	}

	public function getCandidates(){
		// return response()->json(CandidateDetail::get());
	}
	public function create()
	{
		if(Auth::guest()){
			return view('register');
		}
		return view('clients.register');
	}
	public function addBType(Request $r)
	{
		return ClientBusinessType::create($r->all());
	}
	public function checkEmail(Request $r){
		$c = ClientEmail::with('client')->select('client_emails.*')->where('ce_email',$r->val)->first();
		$d[] = $c;
		$d[] = $c ? 'OK' : 'NO';
		return $d;
	}
	public function store(Request $r)
	{
		//return $r->all();
		
		$id = ClientDetail::create([
			'c_business' => $r->business,
			'c_name' => $r->name,
			'c_address' => $r->address,
			'c_user_id' => Auth::user()->id,
			'c_business_type' => $r->cbt,
			])->c_id;

		if($id){


			foreach ($r->mobile as $key => $value) {
				ClientMobile::create([
					'cm_number' => str_replace(' ', '',$value),
					'cm_client_id' => $id,
					]);
			}


			foreach ($r->email as $key => $value) {
				ClientEmail::create([
					'ce_email' => $value,
					'ce_client_id' => $id,				
					]);
			}


			foreach ($r->remark as $key => $value) {

				ClientRemark::create([
					'cr_text' => $value,
					'cr_client_id' => $id,				
					]);
			}
		}	
		return $id ? 'OK' : 'NO';
	// 	if(CandidateDetail::where('c_mobile', $request->mobile)->count() == 0) {
	// 		$d = $request->all();

	// 		if (isset($d['type'])) {
	// 			if ($d['type'] != 0) {
	// 				if ($d['exp_year'] == '-1') {
	// 					unset($d['exp_year']);
	// 					if ($d['exp_month'] == '-1') {
	// 						$d['experience'] = 0;
	// 					} else {
	// 						$d['experience'] = $d['exp_month'];
	// 					}
	// 				} else {
	// 					if ($d['exp_month'] == '-1') {
	// 						$d['experience'] = $d['exp_year'];
	// 					} else {
	// 						$d['experience'] = $d['exp_year'] . '.' . $d['exp_month'];
	// 					}
	// 				}

	// 			} else {
	// 				$d['experience'] = 0;
	// 			}
	// 		} else {
	// 			unset($d['exp_year']);
	// 			unset($d['exp_month']);

	// 			$d['experience'] = 0;
	// 		}

	// 		foreach ($d as $key => $value) {
	// 			$d['c_' . $key] = $d[$key];
	// 			unset($d[$key]);
	// 		}
	// 		unset($d['c_type']);
	// 		unset($d['c_exp_month']);
	// 		if (isset($d['c_skill'])) {
	// 			$d['c_skills'] = implode(', ', $d['c_skill']);
	// 		}
	// 		unset($d['c_skill']);
	// 		$d['c_user_id'] = Auth::user()->id;
	// //         return $d;
	// 		$c = CandidateDetail::create($d);
	//             // return $c->c_id;
	// 		$request->session()->put('candidate', $c->c_id);

	// 		return $c ? 'OK' : 'NO';
	// 	}else{
	// 		return 'exist';
	// 	}
	}

	public function show($id)
	{
	        //
	}

	public function search(Request $r){
		// if(isset($r->mobile)){
		// 	return CandidateDetail::where('c_mobile','like', $r->mobile.'%')->count();
		// }
	}
	public function edit($id)
	{
		return response()->json(ClientDetail::with('email')->with('mobile')->with('remark')->find($id));
	}

	public function update(Request $r, $id)
	{
		if(isset($r->called)){
			$called = ClientDetail::find($id)->c_called == 0 ? 1 : 0;

			if(ClientDetail::where('c_id',$id)->update(['c_called'=>$called])){

				if($called == 1){
					CallLog::create([
						'log_can_id' => $id,
						'log_user_id' => Auth::user()->id
						]);
				}
			}
			return $called;
		}
		
		if(count($r->mid) > 0){
			foreach ($r->mobile as $key => $value) {
				ClientMobile::where('cm_id',$r->mid[$key])->update(['cm_number' => $value]);
			}
			unset($r['mobile']);
			unset($r['mid']);
		}
		if(count($r->rid) > 0){
			foreach ($r->remark as $key => $value) {
				ClientRemark::where('cr_id',$r->rid[$key])->update(['cr_text' => $value]);
			}
			unset($r['remark']);
			unset($r['rid']);
		}
		if(count($r->eid) > 0){
			foreach ($r->email as $key => $value) {
				ClientEmail::where('ce_id',$r->eid[$key])->update(['ce_email' => $value]);
			}
			unset($r['email']);
			unset($r['eid']);
		}
		
		if(isset($r['cbt'])){
			$r['business_type'] = $r->cbt;
			unset($r['cbt']);
		}
		// return $r->all();
		unset($r['_method']);

		$d = $r->all();
		foreach ($d as $key => $value) {
			$d['c_'.$key] = $d[$key];
			unset($d[$key]);
		}

		if(isset($d['c_remark'])){
			
			return ClientRemark::where('cr_id',$d['c_id'])->update(['cr_text' => $d['c_remark']]);
		}

		$c = ClientDetail::where('c_id',$id)->update($d);

		if(isset($d['c_status'])){
			return StatusDetail::find($d['c_status'])->sta_name;
		}
		return $c ? 'OK' : 'NO';
	}

	public function getRemarkBox($id){
		return ClientRemark::create(['cr_text' => null, 'cr_client_id' => $id])->cr_id;
	}

	public function destroy($id)
	{
		if(ClientDetail::where('c_id',$id)->delete()) {
            ClientRemark::where('cr_client_id',$id)->delete();
            ClientMobile::where('cm_client_id',$id)->delete();
            ClientEmail::where('ce_client_id',$id)->delete();
		    return 'OK';
        }
        return 'NO';
	}

	public function fixTable(){		
		$qry = DB::table('client_details')->select('c_id as cr_client_id')->whereNotIn('c_id',DB::table('client_remarks')->select('cr_client_id'))->get();		
		
		foreach ($qry as $value) {			
			ClientRemark::create(['cr_client_id' => $value->cr_client_id]);
		}
		
		return redirect('client');
	}
/*	public function importExcel(Request $request)
	{
		ini_set('max_execution_time', 3000);
	        // return $request->all();
		$landline = 'NULL';
		$mobile = 'NULL';   
	        // $exsist = [];
	        // $insert = [];
		$status = false;

		if($request->hasFile('import_file')){
			$path = $request->file('import_file')->getRealPath();

			$designation = $request['designation'];

			Excel::filter('chunk')->load($path)->chunk(250, function($results) use ($designation, $status) {

				foreach($results as $k => $row)
				{                   
					if(!empty($row->name)){
	                        // echo "<pre>";
	                        // print_r($row);
	                        // echo "</pre>";
	                    // if(empty($row->name))

	                    // echo $k.'</br>';
	                    // echo str_replace(' ', '',$row->mobile).'</br>';
						$mobile = filter_var((isset($row['telephone_numbers_mobile']) ? $row['telephone_numbers_mobile'] : $row['mobile']), FILTER_SANITIZE_NUMBER_INT);

						if(!$this->checkuser($mobile)){
							$name;$email;$mobile;$experience;$salary;

							/*Naukri Data*/                        
			// 				if(isset($row['total_experience'])){
			// 					$name = $row['name'];
			// 					$email = $row['e_mail_id'];                            

			// 					$exp = explode(' ',$row['total_experience']);
			// 					$sal = explode(' ',$row['annual_salary']);

			// 					$experience = ((isset($exp[0])) ? $exp[0] : 0).'.'.((isset($exp[2])) ? $exp[2] : 0);                            
			// 					$salary = (isset($sal[1])) ? $sal[1] : 0;
			// 				}                        
			// 				/*Quikr Data*/
			// 				else if(isset($row['role_experience_in_years'])){
			// 					$name = $row['name'];
			// 					$email = $row['email'];                            
			// 					$salary = ($row['current_salary_per_month'] * 12) / 100000;
			// 					$experience = $row['role_experience_in_years'] != '' ? $row['role_experience_in_years'] : 0;                        
			// 				}
			// 				/*Regular excel data*/
			// 				else{
			// 					$name = $row['name'];
			// 					$email = $row['email'];
			// 					$mobile = $row['mobile'];
			// 					$salary = $row['current_ctc'];
			// 					$experience = $row['experience'];
			// 				}                    

			// 				$remark = isset($row['remark']) ? $row['remark'] : '';

			// 				$insert[] = [
			// 				'c_name' => ucwords($name),
			// 				'c_email' => $email,
			// 				'c_mobile' => str_replace(' ', '',$mobile),
			// 				'c_current_ctc' => $salary,
			// 				'c_experience' => $experience,
			// 				'c_designation' => $designation,
			// 				'c_remark' => $remark,
			// 				'c_user_id' => Auth::user()->id
			// 				];                     
			// 			}         
			// 		}
			// 	}                

			// 	if(!empty($insert)){
			// 		CandidateDetail::insert($insert);
			// 	}

			// });

	// 	}

	// 	return back()->with(['success'=>'Successfully inserted']);
	// }
// */
						}
