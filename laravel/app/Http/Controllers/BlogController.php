<?php

namespace App\Http\Controllers;

use App\Blog;
use Illuminate\Http\Request;
use App\Http\Traits\GetData;
use Illuminate\Database\QueryException;

class BlogController extends Controller
{
    use GetData;

    protected $res = ['msg' => 'error'];

    protected $path = 'images/blogs/';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.view_blogs')->with(['blogs' => Blog::get()]);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create_blog');        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        try {
            $blog = $this->changeKeys('blog_',$r->all());
            $blog['blog_title'] = ucfirst($blog['blog_title']);

            if ($r->hasFile('img')) {
                list($regular) = $this->uploadFiles($r, $blog['blog_title'], 'img', [$this->path]);
                $blog['blog_img'] = $regular;
            }
                        

            $this->res['msg'] = Blog::create($blog) ? 'success' : 'error';

        } catch (QueryException $e) {
            $this->res['msg'] = 'error';
            // return $e;        
        }
        return $this->res;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit(Blog $blog)
    {
        return $this->removePrefix($blog->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Blog $blog)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blog $blog)
    {
        //
    }
}
