<?php

namespace App\Http\Controllers;

use App\UpcomingBatch;
use Illuminate\Http\Request;
use App\Http\Traits\GetData;
use Illuminate\Database\QueryException;

class UpcomingBatchController extends Controller
{
    use GetData;

    protected $res = ['msg' => 'error'];

    protected $path = 'images/upcoming-batches/';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.view_upcoming_batches')->with(['upcomingBatches' => UpcomingBatch::get()]);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create_upcoming_batch');    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        try {
            $ub = $this->changeKeys('ub_',$r->all());
            $ub['ub_title'] = ucfirst($ub['ub_title']);                            

            if ($r->hasFile('img')) {
                list($regular) = $this->uploadFiles($r, $ub['ub_title'], 'img', [$this->path]);
                $ub['ub_img'] = $regular;
            }
            
            // return $ub;

            $this->res['msg'] = UpcomingBatch::create($ub) ? 'success' : 'error';

        } catch (QueryException $e) {
            $this->res['msg'] = 'error';        
        }
        return $this->res;          
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UpcomingBatch  $upcomingBatch
     * @return \Illuminate\Http\Response
     */
    public function show(UpcomingBatch $upcomingBatch)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UpcomingBatch  $upcomingBatch
     * @return \Illuminate\Http\Response
     */
    public function edit(UpcomingBatch $upcomingBatch)
    {
        return $this->removePrefix($upcomingBatch->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UpcomingBatch  $upcomingBatch
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UpcomingBatch $upcomingBatch)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UpcomingBatch  $upcomingBatch
     * @return \Illuminate\Http\Response
     */
    public function destroy(UpcomingBatch $upcomingBatch)
    {
        //
    }
}
