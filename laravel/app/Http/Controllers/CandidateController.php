<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CandidateDetail;
use App\StatusDetail;
use Illuminate\Support\Facades\Auth;
use Excel;
use App\Designation;
use App\Http\Traits\GetData;
use Yajra\Datatables\Datatables;
use Yajra\Datatables\Html\Builder;
use App\Skill;
use App\CallLog;
class CandidateController extends Controller
{
    use GetData;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function getStatus{
    //     return response()->json(Skill::get());
    // }
    public function getStatus(){
        return response()->json(StatusDetail::where('sta_for_id',Auth::user()->type)->get());
    }

    public function getRemark($id){
        return response()->json(CandidateDetail::find($id));
    }

    public function getDatatable(Request $se){
        $query = CandidateDetail::with('status')->select('candidate_details.*');
        // ->where('status_details.sta_for_id',3);
        if(isset($se->notice)){
            
            return $query->where('can_called',0)->count();
        }
        if($se->data != '-1'){
            $query = $query->where('can_designation',$se->data);
        }
        $query = $query->orderBy('can_called','asc')->orderBy('can_created_at','desc')->where('can_user_id',Auth::user()->id)->get();
        return Datatables::of($query)->make(true);
    }

    public function index(Builder $builder)
    {
        // if(Auth::guest()){
        //     return redirect()->route('login');
        // }
        // if (request()->ajax()) {
        //        return Datatables::of(CandidateDetail::query())->make(true);
        //    }

        //    $html = $builder->columns([
        //                ['data' => 'can_id', 'name' => 'can_id', 'title' => 'Id'],
        //                ['data' => 'can_name', 'name' => 'can_name', 'title' => 'Name'],
        //                ['data' => 'can_email', 'name' => 'can_email', 'title' => 'Email'],
        //                ['data' => 'can_created_at', 'name' => 'can_created_at', 'title' => 'Created At'],
        //                ['data' => 'can_updated_at', 'name' => 'can_updated_at', 'title' => 'Updated At'],
        //            ]);
        // // return compact('html');
        //    return view('candidates.view-candidate', compact('html'));
        return view('candidates.view-candidate');
        // ->with(['candidates' => CandidateDetail::get()]);
    }

    public function getCandidates(){
        // if (request()->ajax()) {
        //        return Datatables::of(CandidateDetail::query())->make(true);
        //    }
        return response()->json(CandidateDetail::get());
        //    $html = $builder->columns([
        //                ['data' => 'can_id', 'name' => 'can_id', 'title' => 'Id'],
        //                ['data' => 'can_name', 'name' => 'can_name', 'title' => 'Name'],
        //                ['data' => 'can_email', 'name' => 'can_email', 'title' => 'Email'],
        //                ['data' => 'can_created_at', 'name' => 'can_created_at', 'title' => 'Created At'],
        //                ['data' => 'can_updated_at', 'name' => 'can_updated_at', 'title' => 'Updated At']),
        //            ]);

        //    return view('users.index', compact('html'));
        // return response()->json(CandidateDetail::get());
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::guest()){
            return view('register');
        }
        return view('admin-register');
    }
    public function storeDes(Request $request)
    {
        // return $request->all();
        return Designation::create($request->all());
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if(CandidateDetail::where('can_mobile', $request->mobile)->count() == 0) {
            $d = $request->all();

            if (isset($d['type'])) {
                if ($d['type'] != 0) {
                    if ($d['exp_year'] == '-1') {
                        unset($d['exp_year']);
                        if ($d['exp_month'] == '-1') {
                            $d['experience'] = 0;
                        } else {
                            $d['experience'] = $d['exp_month'];
                        }
                    } else {
                        if ($d['exp_month'] == '-1') {
                            $d['experience'] = $d['exp_year'];
                        } else {
                            $d['experience'] = $d['exp_year'] . '.' . $d['exp_month'];
                        }
                    }

                } else {
                    $d['experience'] = 0;
                }
            } else {
                unset($d['exp_year']);
                unset($d['exp_month']);

                $d['experience'] = 0;
            }

            foreach ($d as $key => $value) {
                $d['can_' . $key] = $d[$key];
                unset($d[$key]);
            }
            unset($d['can_type']);
            unset($d['can_exp_month']);
            if (isset($d['can_skill'])) {
                $d['can_skills'] = implode(', ', $d['can_skill']);
            }
            unset($d['can_skill']);
            if(!Auth::guest()){

                $d['can_user_id'] = Auth::user()->id;

            }else{
                $d['can_user_id'] = 0;
            }
//         return $d;
            $c = CandidateDetail::create($d);
            // return $c->can_id;
            $request->session()->put('candidate', $c->can_id);

            return $c ? 'OK' : 'NO';
        }else{
            return 'exist';
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function search(Request $r){
        if(isset($r->mobile)){
            return CandidateDetail::where('can_mobile','like', $r->mobile.'%')->count();
        }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
        return response()->json(CandidateDetail::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(isset($request->called)){
            $called = CandidateDetail::find($id)->can_called == 0 ? 1 : 0;

            if(CandidateDetail::where('can_id',$id)->update(['can_called'=>$called])){

                if($called == 1){
                    CallLog::create([
                        'log_can_id' => $id,
                        'log_user_id' => Auth::user()->id
                        ]);
                }
            }

            return $called;
        }
        unset($request['_method']);
        unset($request['id']);

        if (isset($request['type'])) {
            if ($request['can_type'] != 0) {
                if ($request['exp_year'] == '-1') {
                    unset($request['exp_year']);
                    if ($request['exp_month'] == '-1') {
                        $request['experience'] = 0;
                    } else {
                        $request['experience'] = $request['exp_month'];
                    }
                } else {
                    if ($request['exp_month'] == '-1') {
                        $request['experience'] = $request['exp_year'];
                    } else {
                        $request['experience'] = $request['exp_year'] . '.' . $request['exp_month'];
                    }
                }

            } else {
                $request['experience'] = 0;
            }
        } else {
            unset($request['exp_year']);
            unset($request['exp_month']);

            $request['experience'] = 0;
        }
        unset($request['exp_year']);
        unset($request['exp_month']);
        unset($request['type']);

        if($request['passout_year'] == '-1'){
            $request['passout_year'] = null;
        }
        $d = $request->all();
        foreach ($d as $key => $value) {            
            $d['can_'.$key] = $d[$key];
            unset($d[$key]);
        }
        

        $c = CandidateDetail::where('can_id',$id)->update($d);
        // var_dump($c);

        if(isset($d['can_status'])){
            return StatusDetail::find($d['can_status'])->sta_name;
        }
        return $c ? 'OK' : 'NO';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return CandidateDetail::where('can_id',$id)->delete() ? 'OK' : 'NO';
    }
    // public $status = false;
    public function importExcel(Request $request)
    {
        ini_set('max_execution_time', 3000);
        // return $request->all();
        $landline = 'NULL';
        $mobile = 'NULL';   
        // $exsist = [];
        // $insert = [];
        $status = false;

        if($request->hasFile('import_file')){
            $path = $request->file('import_file')->getRealPath();
            
            $designation = $request['designation'];

            Excel::filter('chunk')->load($path)->chunk(250, function($results) use ($designation, $status) {

                foreach($results as $k => $row)
                {                   
                    if(!empty($row->name)){
                        // echo "<pre>";
                        // print_r($row);
                        // echo "</pre>";
                    // if(empty($row->name))

                    // echo $k.'</br>';
                    // echo str_replace(' ', '',$row->mobile).'</br>';
                        $mobile = filter_var((isset($row['telephone_numbers_mobile']) ? $row['telephone_numbers_mobile'] : $row['mobile']), FILTER_SANITIZE_NUMBER_INT);

                        if(!$this->checkuser($mobile)){
                            $name;$email;$mobile;$experience;$salary;

                            /*Naukri Data*/                        
                            if(isset($row['total_experience'])){
                                $name = $row['name'];
                                $email = $row['e_mail_id'];                            

                                $exp = explode(' ',$row['total_experience']);
                                $sal = explode(' ',$row['annual_salary']);

                                $experience = ((isset($exp[0])) ? $exp[0] : 0).'.'.((isset($exp[2])) ? $exp[2] : 0);                            
                                $salary = (isset($sal[1])) ? $sal[1] : 0;
                            }                        
                            /*Quikr Data*/
                            else if(isset($row['role_experience_in_years'])){
                                $name = $row['name'];
                                $email = $row['email'];                            
                                $salary = ($row['current_salary_per_month'] * 12) / 100000;
                                $experience = $row['role_experience_in_years'] != '' ? $row['role_experience_in_years'] : 0;                        
                            }
                            /*Regular excel data*/
                            else{
                                $name = $row['name'];
                                $email = $row['email'];
                                $mobile = $row['mobile'];
                                $salary = $row['current_ctc'];
                                $experience = $row['experience'];
                            }                    
                            
                            $remark = isset($row['remark']) ? $row['remark'] : '';
                            
                            $insert[] = [
                            'can_name' => ucwords($name),
                            'can_email' => $email,
                            'can_mobile' => str_replace(' ', '',$mobile),
                            'can_current_ctc' => $salary,
                            'can_experience' => $experience,
                            'can_designation' => $designation,
                            'can_remark' => $remark,
                            'can_user_id' => Auth::user()->id
                            ];                     
                        }         
                    }
                }                
                
                if(!empty($insert)){
                    CandidateDetail::insert($insert);
                }
                
            });
            
        }

        return back()->with(['success'=>'Successfully inserted']);
    }
}
