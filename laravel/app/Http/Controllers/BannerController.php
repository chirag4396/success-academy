<?php

namespace App\Http\Controllers;

use App\Banner;
use Illuminate\Http\Request;
use App\Http\Traits\GetData;
use Illuminate\Database\QueryException;

class BannerController extends Controller
{
    use GetData;

    protected $res = ['msg' => 'error'];

    protected $path = 'images/banners/';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.view_banners')->with(['banners' => Banner::get()]);                
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create_banner');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        try {
            $ban = $this->changeKeys('ban_',$r->all());
            $ban['ban_title'] = ucfirst($ban['ban_title']);                            

            if ($r->hasFile('img')) {
                list($regular) = $this->uploadFiles($r, $ban['ban_title'], 'img', [$this->path], [], 1300);
                $ban['ban_img'] = $regular;
            }
            
            // return $ban;

            $this->res['msg'] = Banner::create($ban) ? 'success' : 'error';

        } catch (QueryException $e) {
            $this->res['msg'] = 'error';        
            $this->res['error'] = $e;        
        }
        return $this->res;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function show(Banner $banner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function edit(Banner $banner)
    {
        return $this->removePrefix($banner->toArray());                
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Banner $banner)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function destroy(Banner $banner)
    {
        //
    }
}
