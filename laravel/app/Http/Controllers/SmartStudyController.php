<?php

namespace App\Http\Controllers;

use App\SmartStudy;
use Illuminate\Http\Request;
use App\Http\Traits\GetData;
use Illuminate\Database\QueryException;

class SmartStudyController extends Controller
{
    use GetData;

    protected $res = ['msg' => 'error'];

    protected $path = 'pdfs/smart-studies/';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.view_smart_studies')->with(['smartStudies' => SmartStudy::get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create_smart_study');        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        try {
            $ss = $this->changeKeys('ss_',$r->all());

            if ($r->hasFile('pdf')) {
                $exe = $r->file('pdf')->getClientOriginalExtension();
                $filename = strtolower(str_replace(' ', '-', $ss['ss_title'])).'-'.$ss['ss_type'].'.'.$exe;
                $r->file('pdf')->move($this->path, $filename);
                $ss['ss_pdf'] = $this->path.$filename;
            }
            
            $ss['ss_title'] = ucfirst($ss['ss_title']);

            if ($r->hasFile('img')) {
                list($regular) = $this->uploadFiles($r, $ss['ss_title'], 'img', [$this->path]);
                $ss['ss_img'] = $regular;
            }
            
            $this->res['msg'] = SmartStudy::create($ss) ? 'success' : 'error';

        } catch (QueryException $e) {
            $this->res['msg'] = 'error';
            $this->res['exc'] = $e;

        }
        return $this->res;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SmartStudy  $smartStudy
     * @return \Illuminate\Http\Response
     */
    public function show(SmartStudy $smartStudy)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SmartStudy  $smartStudy
     * @return \Illuminate\Http\Response
     */
    public function edit(SmartStudy $smartStudy)
    {
        return $this->removePrefix($smartStudy->toArray());                
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SmartStudy  $smartStudy
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SmartStudy $smartStudy)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SmartStudy  $smartStudy
     * @return \Illuminate\Http\Response
     */
    public function destroy(SmartStudy $smartStudy)
    {
        //
    }
}
