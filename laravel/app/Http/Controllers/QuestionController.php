<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use App\Test;
use App\Subject;
use App\Option;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('question');
    }
    
    public function addQuestion($sub, $test) {
        $subject = Subject::find($sub);
        $test = Test::find($test);
        return view('admin.add-question')->with(['subject' => $subject, 'test' => $test]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->option;
        $data = ['msg' => 'error'];
        
        if($request->detail != ''){

            $qId = Question::create([
                'qus_detail' => $request->detail,
                'qus_test_id' => $request->test_id,
                'qus_score' => $request->score,
                'qus_subject' => $request->subject,
                'qus_solution' => $request->solution,
                'qus_seq' => $request->seq
            ])->qus_id;

            if($qId){                
                foreach ($request->option as $k => $v) {
                    if($v == ''){
                        Option::where('op_qus_id',$qId)->delete();
                        Question::find($qId)->delete();
                        $data['msg'] = 'empty';
                        return $data;
                    }
                    $o = Option::create([
                        'op_detail' => $v,
                        'op_qus_id' => $qId
                    ])->op_id;
                    if($k == $request->answer){
                        $oId = $o;
                    }
                }   

                $data['msg'] = Question::find($qId)->update(['qus_answer' => $oId]) ? 'success' : 'error';
            }
        }else{
            $data['msg'] = 'empty';            
        }

        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin.single-question')->with(['question' => Question::with('option')->find($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.single-question')->with(['question' => Question::with('option')->find($id), 'type' => 'edit']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // return $request->answer;
        // return $request->all();
        $oId = 0;
        foreach ($request->option as $k => $v) {

            $op = Option::find($k);
            $o = Option::where('op_id',$k)->update([
                'op_detail' => $v                
            ]);
            if($k == $request->answer){
                $oId = $op->op_id;
            }
        }   
        
        return $qId = Question::where('qus_id',$id)->update([
            'qus_detail' => $request->detail,            
            'qus_score' => $request->score,
            'qus_solution' => $request->solution,            
            'qus_answer' => $oId
        ]) ? 'OK' : 'NO';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $id;
    }
}
