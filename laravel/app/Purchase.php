<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
	public $primaryKey = 'pur_id';

	protected $fillable = ['pur_user_id', 'pur_pack_id', 'pur_status'];

	CONST UPDATED_AT = 'pur_updated_at';

	CONST CREATED_AT = 'pur_created_at';

	public function user(){
		return $this->belongsTo(\App\User::class, 'pur_user_id');
	}

	public function package(){
		return $this->belongsTo(\App\PackageDetail::class, 'pur_pack_id');
	}	
}
