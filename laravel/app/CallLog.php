<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CallLog extends Model
{
    protected $fillable = ['log_id', 'log_user_id', 'log_can_id', 'log_created_at'];

    CONST CREATED_AT = 'log_created_at';

    CONST UPDATED_AT = NULL;
    
    public $primaryKey = 'log_id';

}
