<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    protected $fillable = ['op_id', 'op_detail', 'op_qus_id'];

    public $timestamps = false;

    public $primaryKey = 'op_id';

    public function question(){
    	return $this->belongsTo(Question::class,'op_qus_id','qus_id');
    }
}
