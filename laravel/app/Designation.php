<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Designation extends Model
{
    protected $fillable = ['des_id', 'des_name'];

    public $timestamps = false;

    public $primaryKey = 'des_id';

    public function candidates(){
    	return $this->hasMany(CandidateDetail::class, 'can_designation', 'des_id');
    }
}
