<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientEmail extends Model
{
    protected $fillable = ['ce_id', 'ce_email', 'ce_client_id'];

    public $timestamps = false;

    public $primaryKey = 'ce_id';

    public function client(){
    	return $this->belongsTo(ClientDetail::class,'ce_client_id','c_id');
    }
}
