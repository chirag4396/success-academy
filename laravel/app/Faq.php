<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    public $primaryKey = 'faq_id';

    protected $fillable = ['faq_qus', 'faq_ans', 'faq_type'];

    public $timestamps = false;

    public function type()
    {
    	return $this->belongsTo(\App\PackageType::class, 'faq_type');
    }
}
