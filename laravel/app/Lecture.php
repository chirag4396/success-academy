<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lecture extends Model
{
	public $primaryKey = 'lec_id';

	protected $fillable = ['lec_title', 'lec_description', 'lec_video', 'lec_type'];

	CONST UPDATED_AT = 'lec_updated_at';

	CONST CREATED_AT = 'lec_created_at';
}
