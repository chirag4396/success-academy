<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StartTestLog extends Model
{
    protected $fillable = ['st_id', 'st_test_id', 'st_user_id', 'st_time', 'st_end'];
   
    CONST CREATED_AT = 'st_time';
    
    CONST UPDATED_AT = null;

    public $primaryKey = 'st_id';

    public function test(){
        return $this->belongsTo(Test::class, 'st_test_id', 'test_id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'st_user_id', 'id');
    }
}
