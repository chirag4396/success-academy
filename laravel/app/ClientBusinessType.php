<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientBusinessType extends Model
{
    protected $fillable = ['cbt_id', 'cbt_title'];

    public $timestamps = false;

    public $primaryKey = 'cbt_id';

    public function client(){
    	return $this->hasMany(ClientDetail::class, 'c_business_type', 'cbt_id');
    }
}
