<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Download extends Model
{
	public $primaryKey = 'd_id';

	protected $fillable = ['d_title', 'd_pdf', 'd_type'];

	public $timestamps = false;

	public function type()
	{
		return $this->belongsTo(\App\PackageType::class, 'd_type');
	}    
}
