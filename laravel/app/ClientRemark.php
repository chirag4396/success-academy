<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientRemark extends Model
{
    protected $fillable = ['cr_id', 'cr_text', 'cr_client_id'];

    public $timestamps = false;

    public $primaryKey = 'cr_id';

    public function client(){
    	return $this->belongsTo(ClientRemark::class, 'cr_client_id', 'c_id');
    }
}
