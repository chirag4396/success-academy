<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'visible_password', 'type', 'mobile', 'p_mobile', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function startLog(){
        return $this->hasMany(StartTestLog::class, 'st_user_id', 'id');
    }   

    public function package(){
        return $this->belongsTo(\Package::class, 'pack_user_id');
    }

    public function purchases(){
        return $this->hasMany(\Purchase::class, 'pack_user_id');
    }
}
