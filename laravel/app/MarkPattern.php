<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarkPattern extends Model
{
    protected $fillable = ['mark_id', 'mark_title'];

    public $timestamps = false;

    public $primaryKey = 'mark_id';
}
