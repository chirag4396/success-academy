<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Skill extends Model
{
    protected $fillable = ['skill_id', 'skill_name'];

    public $timestamps = false;
    
}
