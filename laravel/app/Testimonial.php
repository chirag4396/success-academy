<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{
    protected $fillable = ['tes_name', 'tes_designation', 'tes_img', 'tes_description'];

    CONST CREATED_AT = 'tes_created_at';
    
    CONST UPDATED_AT = 'tes_updated_at';

    public $primaryKey = 'tes_id';
}
