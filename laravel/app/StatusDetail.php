<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatusDetail extends Model
{
    protected $fillable = ['sta_id', 'sta_name', 'sta_color', 'sta_for_id'];

    public $timestamps = false;

    public $primaryKey = 'sta_id';

    public function candidate(){
    	return $this->hasMany(CandidateDetail::class,'can_status', 'sta_id');
    }

    public function userType(){
    	return $this->belongsTo(UserType::class, 'u_id', 'sta_for_id');
    }
}
