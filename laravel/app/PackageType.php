<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageType extends Model
{
	public $primaryKey = 'pt_id';

	protected $fillable = ['pt_title'];

	public $timestamps = false;

	public function packages()
	{
		return $this->hasMany(\App\PackageDetail::class, 'pack_type');
	}

	public function achievements()
	{
		return $this->hasMany(\App\Achievement::class, 'ach_type');
	}
	
	public function achievementTypes()
	{
		return $this->hasMany(\App\AchievementType::class, 'at_pack_type');
	}
	
	public function upcomingBatches()
	{
		return $this->hasMany(\App\UpcomingBatch::class, 'ub_type');
	}

	public function faqs()
	{
		return $this->hasMany(\App\Faq::class, 'faq_type');
	}

	public function smartStudies()
	{
		return $this->hasMany(\App\SmartStudy::class, 'ss_type');
	}

	public function downloads()
	{
		return $this->hasMany(\App\Download::class, 'd_type');
	}	

	public function lectures()
	{
		return $this->hasMany(\App\Lecture::class, 'lec_type');
	}	
}
