<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $primaryKey = 'order_id';
	CONST CREATED_AT = 'order_created_at';
	CONST UPDATED_AT = null;

    protected $fillable = ['order_user_id','order_d_id', 'order_package', 'order_total', 'order_created_at', 'order_status'];

    public function products(){
    	return $this->hasMany(\App\Models\OrderProduct::class, 'op_order_id');
    }

    public function user(){
    	return $this->belongsTo(\App\User::class, 'order_user_id');
    }

    public function userDetail()
    {
        return $this->belongsTo(\App\Models\UserDetail::class, 'order_user_detail');
    }

    public function status(){
    	return $this->belongsTo(\App\Models\OrderStatus::class, 'order_status');
    }
}
