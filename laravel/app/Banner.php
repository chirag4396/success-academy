<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
	public $primaryKey = 'ban_id';

	protected $fillable = ['ban_title', 'ban_img'];

	CONST UPDATED_AT = 'ban_updated_at';

	CONST CREATED_AT = 'ban_created_at';
}
