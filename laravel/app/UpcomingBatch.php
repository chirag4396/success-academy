<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UpcomingBatch extends Model
{
	public $primaryKey = 'ub_id';
	
	protected $fillable = ['ub_title', 'ub_type', 'ub_description', 'ub_img', 'ub_updated_at'];

	CONST CREATED_AT = 'ub_created_at';
	
	CONST UPDATED_AT = 'ub_updated_at';

	public function type()
	{
		return $this->belongsTo(\App\PackageType::class, 'ub_type');
	}
}
