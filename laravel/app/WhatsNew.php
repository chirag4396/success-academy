<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WhatsNew extends Model
{
    protected $primaryKey = 'wn_id';
 
    public $table = 'whats_new';
 
    protected $fillable = ['wn_description', 'wn_status'];

    CONST CREATED_AT = 'wn_created_at';
    
    CONST UPDATED_AT = null;    
}
