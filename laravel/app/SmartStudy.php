<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SmartStudy extends Model
{
    public $primaryKey = 'ss_id';

    protected $fillable = ['ss_title', 'ss_pdf', 'ss_type', 'ss_img'];

    public $timestamps = false;

    public function type()
    {
    	return $this->belongsTo(\App\PackageType::class, 'ss_type');
    }
}
