<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
	public $primaryKey = 'gal_id';

	protected $fillable = ['gal_title', 'gal_img', 'gal_description'];

	public $timestamps = false;
}
