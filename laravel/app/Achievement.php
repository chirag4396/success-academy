<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Achievement extends Model
{
    protected $fillable = ['ach_type', 'ach_a_type', 'ach_name', 'ach_sub_name', 'ach_description', 'ach_img'];

    CONST CREATED_AT = 'ach_created_at';
    
    CONST UPDATED_AT = 'ach_updated_at';

    public $primaryKey = 'ach_id';

    public function type()
    {
    	return $this->belongsTo(\App\PackageType::class, 'ach_type');
    }

    public function aType()
    {
    	return $this->belongsTo(\App\AchievementType::class, 'ach_a_type');
    }
}
