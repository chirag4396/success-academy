<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Degree extends Model
{
	protected $fillable = ['degree_id', 'degree_name'];

	public $timestamps = false;

	public $primaryKey = 'degree_id';
}
