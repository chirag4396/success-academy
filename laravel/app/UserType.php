<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserType extends Model
{    
    protected $fillable = ['u_id', 'u_title'];
	   
    public $timestamps = false;
    
    public $primaryKey = 'u_id';


    public function status(){
    	return $this->hasMany(StatusDetail::class, 'u_id', 'sta_for_id');
    }
}
