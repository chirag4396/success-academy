<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    protected $fillable = ['test_id','test_title', 'test_package', 'test_hours', 'test_score','test_pattern','test_enable_at', 'test_disable_at',' test_taken_at', 'test_subjects', 'test_updated_at'];

    CONST CREATED_AT = 'test_taken_at';

    CONST UPDATED_AT = 'test_updated_at';

    public $primaryKey ='test_id';

    public function package(){
    	return $this->belongsTo(PackageDetail::class, 'test_package', 'pack_id');
    }
    
    public function subjects(){
        // return $this->test_subjects;
        return \App\Subject::whereIn('sub_id', explode(',',$this->test_subjects));
    }

    public function question(){
    	return $this->hasMany(Question::class, 'qus_test_id', 'test_id');
    }
    
    public function startLog(){
        return $this->hasMany(StartTestLog::class, 'st_test_id', 'test_id');
    }
}
