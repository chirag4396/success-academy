<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enquiry extends Model
{
	protected $fillable = ['enq_type', 'enq_name', 'enq_email', 'enq_mobile', 'enq_query'];

	CONST CREATED_AT = 'enq_created_at';
	
	CONST UPDATED_AT = null;

	public $primaryKey = 'enq_id';
}
