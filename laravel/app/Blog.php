<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
	public $primaryKey = 'blog_id';

	protected $fillable = ['blog_title', 'blog_img', 'blog_description'];

	CONST CREATED_AT = 'blog_created_at'; 
	
	CONST UPDATED_AT = 'blog_updated_at';
}
