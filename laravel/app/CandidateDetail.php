<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CandidateDetail extends Model
{
    protected $fillable = ['can_id', 'can_name', 'can_email', 'can_mobile', 'can_alt_mobile', 'can_designation','can_remark' , 'can_highest_degree', 'can_passout_year', 'can_experience', 'can_skills', 'can_current_ctc', 'can_expected_ctc', 'can_test_time', 'can_status', 'can_called', 'can_user_id', 'can_created_at', 'can_updated_at'];

    CONST CREATED_AT = 'can_created_at';

    CONST UPDATED_AT = 'can_updated_at';

    public $primaryKey = 'can_id';
    
    public function designation(){
    	return $this->belongsTo(Designation::class, 'can_designation', 'des_id');
    }

    // public function batch(){
    //     return $this->belongsTo(Batch::class, 'can_batch', 'batch_id');
    // }
    
    public function status(){
    	return $this->belongsTo(StatusDetail::class, 'can_status', 'sta_id');
    }
}
