<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = ['qus_id', 'qus_seq', 'qus_detail', 'qus_answer', 'qus_test_id', 'qus_score', 'qus_solution', 'qus_subject'];
    
    public $timestamps = false;

    public $primaryKey = 'qus_id';

    public function option(){
    	return $this->hasMany(Option::class,'op_qus_id','qus_id');
    }

    public function test(){
    	return $this->belongsTo(Test::class, 'qus_test_id', 'test_id');
    }

    public function subject() {
    	return $this->belongsTo(\App\Subject::class, 'qus_subject');
    }
}
