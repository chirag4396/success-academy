<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageDetail extends Model
{
	public $primaryKey = 'pack_id';

    protected $fillable = ['pack_title', 'pack_user_id', 'pack_status', 'pack_type', 'pack_price', 'pack_dis_price', 'pack_description', 'pack_active_at', 'pack_deactive_at', 'pack_delete_status'];

    CONST UPDATED_AT = 'pack_updated_at';
    
    CONST CREATED_AT = 'pack_created_at';
    
    public function type()
    {
        return $this->belongsTo(\App\PackageType::class, 'pack_type');
    }

    public function user(){
    	return $this->belongsTo(\User::class, 'pack_user_id');
    }

    public function test(){
    	return $this->hasMany(Test::class, 'test_package', 'pack_id');
    }

    public function purchases(){
        return $this->hasMany(\Purchase::class, 'pack_pack_id');
    }
}
