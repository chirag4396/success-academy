<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientMobile extends Model
{    
    protected $fillable = ['cm_id', 'cm_number', 'cm_client_id'];

    public $timestamps = false;

    public $primaryKey = 'cm_id';

    public function client(){
    	return $this->belongsTo(ClientDetail::class, 'cm_client_id', 'c_id');
    }
}
