<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Batch extends Model
{
    protected $fillable = ['batch_id', 'batch_name'];

    public $timestamps = false;

    public $primaryKey = 'batch_id';

    public function user(){
    	return $this->hasMany(User::class, 'batch', 'batch_id');
    }

    public function test(){
    	return $this->hasMany(Test::class, 'test_batch', 'batch_id');
    }
}
