<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $fillable = ['sub_title', 'sub_test_id'];

    public $primaryKey = 'sub_id';

    public $timestamps = false;

    public function questions() {
    	return $this->hasMany(\App\Question::class, 'qus_subject');
    }
}
