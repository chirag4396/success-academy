<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SkipDetail extends Model
{
    protected $fillable = ['s_id', 's_user_id', 's_tst_id', 's_start_id', 's_qus_id', 's_page'];

    public $timestamps = false;

    public $primaryKey = 's_id';
}
