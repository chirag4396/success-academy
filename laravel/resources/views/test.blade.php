@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <form id = "questionForm">
                    @foreach($questions as $question)
                    <input type="hidden" name="question" value="{{ $question->qus_id }}">
                    {{-- <input type="hidden" name="selected" id = "selected" > --}}
                    <div class="panel-heading question disable-select">
                        {{ $question->qus_detail }}                
                    </div>
                    <div class="panel-body">
                        @php
                        $l = ['A','B','C','D']
                        @endphp
                        @forelse (App\Option::where('op_qus_id',$question->qus_id)->get() as $d => $option)

                        <div class="col-md-12 padding option">
                            <input type="radio" name="option" value = "{{ $option->op_id }}" /> {{ $l[$d].'. '. $option->op_detail }}
                        </div>

                        @empty

                        @endforelse
                    </div>
                    @endforeach
                </form>
                <div class="panel-footer">

                        <a href="{{ route('finish') }}" class="btn btn-danger">Finish</a>
                    
                    <a class="btn btn-primary pull-right" id = "next">Next</a>
                    {{-- href = "{{ $questions->nextPageUrl() }}" --}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('footer')

<script>

    $url = "{{ Request::ip() != '127.0.0.1' ? 'http://myfabshop.com/data/' : 'http://sungare-mcq/'}}";
</script>


<script type="text/javascript" src = "{{ asset('js/jquery.countdownTimer.min.js') }}"></script>
<script type="text/javascript" src = "{{ asset('js/test.js') }}"></script>
@endpush