@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">                
                <div class="panel-body notice">
                    <h3>Rules to be Follow :</h3>
                    <ul>
                        <li>
                            You have to complete each question in 2 minutes, if you somehow not able to manage selecting option, that question will be skipped and considered 0 as mark.
                        </li>
                        <li>
                            After pressing next button you'll not able to go back to previous question.
                        </li>
                        <li>
                            No negative marking is there for wrong selection.
                        </li>
                        <li>
                            You have to score atleast 60% in these test otherwise you will not be eligible for Machine round.
                        </li>
                        <li>
                            No Malpractices. If we found any, you'll not be eligible for next round and immediately terminated from current round also.
                        </li>
                    </ul>
                    <h2 class="text-center">Best of Luck</h2>
                </div>
                <div class="panel-footer text-center">
                    <form action="{{url('/startTest')}}">
                        <button class="btn btn-primary">Start Test</button>                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection