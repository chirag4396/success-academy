<!DOCTYPE html>
<html lang="en">
<head>
	<title>{{ config('app.name') }} | @yield('title')</title>
	<link rel="icon" type="image/png" href="{{ asset('images/fav-icon.png') }}" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	
	<meta name="keywords" content="{{ config('app.name') }}"/>
	<style type="text/css">
		#loader{			
			position: fixed;
			z-index: 1000;
			background: whitesmoke;
			height: 100%;
			width: 100%;
			text-align: center;
			padding-top: 15%;
		}
	</style>
	<link href="{{ asset('front/css/bootstrap.css') }}" rel="stylesheet" type="text/css" media="all" />
	<link href="{{ asset('front/css/style.css') }}" rel="stylesheet" type="text/css" media="all" />
	<link href="{{ asset('front/css/fontawesome-all.css') }}" rel="stylesheet">
	<script type="text/javascript" src="{{ asset('front/js/jquery.js') }}"></script>
	<script type="text/javascript" src="{{ asset('front/js/ach.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('.navbar-fostrap').click(function(){
				$('.nav-fostrap').toggleClass('visible');
				$('body').toggleClass('cover-bg');
			});
		});
	</script>

	@stack('header')
</head>	
<body @yield('body')>
	<div id="loader">
		<img src="{{ asset('images/logo.png') }}" width="150">
		<p></p>
	</div>
	@php
	function packageType($route)
	{
		echo '<li><a href="'.route($route).'" >'.str_replace('-', ' ', $route).'<span class="arrow-down"></span></a>'
		.'<ul class="dropdown">';
		foreach (\App\PackageType::get() as $pt){
			echo'<li>'
			.'<a href="'.route($route ,['type' => strtolower($pt->pt_title)]).'">'
			.$pt->pt_title
			.'</a>'
			.'</li>';
		}
		echo '</ul>'
		.'</li>';
	}
	@endphp
	<div class="clearfix"> </div>
	<div class="top_heder_agile_info header-main">
		<div class="w3ls_agile_header_inner">	
			<nav class=" navbar navbar-default text-xs-center">
				<div class="navbar-header ">

					<h1><a class="navbar-brand" href="{{ route('u_home') }}"><img src="{{ asset('front/images/logo1.png') }}"> </a></h1>

					<div class="float-xs-right">
						<img src="{{ asset('front/images/logo3.png') }}">
					</div>
					{{-- <ul class="nav nav1 navbar-nav float-xs-right col-xs-8 col-lg-2 col-md-5 res-no-padding create">

						<li>
							<a class="btn create col-sm-6  col-xs-6 col-xl-5" href="#" data-toggle="modal" data-target="#credentialModel"> <input type="text" class="form-control" placeholder="Search"> <i class="fas fa-search"></i>
							</a>
						</li>
					</ul> --}}
				</div>
			</nav>
			<div id="main">
				<div class="">

					<nav>
						<div class="nav-fostrap">
							<ul>
								<li><a href="{{ route('u_home') }}"><i class="fas fa-home"></i></a></li>
								<li><a href="{{ route('about') }}">About<span class="arrow-down"></span></a>
									<ul class="dropdown">
										<li><a href="{{ route('about') }}">Aim</a></li>
										<li><a href="{{ route('about') }}">Vision</a></li>
										<li><a href="{{ route('about') }}">Mission</a></li>
										<li><a href="{{ route('about') }}">360 APPROACH</a></li>
										<li><a href="{{ route('about') }}">From Director’s Desk</a></li>
									</ul>
								</li>
								@php
								$pages = ['achievements','upcoming-batch', 'gallery', 'faqs', 'smart-study', 'downloads'];
								@endphp
								@forelse ($pages as $page)
								{{ packageType($page) }}
								@empty

								@endforelse								
								<li><a href="{{ route('blogs') }}">Blog</a></li>
								<li ><a href="{{ route('contact') }}">Contact</a></li>
							</ul>
						</div>
						<div class="nav-bg-fostrap">
							<div class="navbar-fostrap"> <span></span> <span></span> <span></span> </div>
						</div>
					</nav>
				</div>
			</div>
		</div>
	</div>