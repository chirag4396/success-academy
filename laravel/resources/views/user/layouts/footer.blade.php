	<div class="footer">
		<div class="container">
			<div class="col-md-4 agile_footer_grid con">
				<h3>Contact Info</h3>
				<ul class="w3_address">
					<li><i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i>I. P. Sir's Success Academy,
					5th Floor, Kesari Wada,Narayan Peth,Pune 411030</li>
					<li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i><a href="mailto:ipsirssuccessacademy@gmail.com"> ipsirssuccessacademy@gmail.com</a></li>
					<li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>020 - 24432433 , 7276568803 , 7385061249</li>
				</ul>
			</div>
			<div class="col-md-4 agile_footer_grid">
				<h3>Connect with us</h3>
				<ul class="agileits_social_list">
					<li><img src="{{ asset('front/images/s1.png') }}"></li>
					<li><img src="{{ asset('front/images/s2.png') }}"></li>


					<li><img src="{{ asset('front/images/s3.png') }}"></li>

					<li><img src="{{ asset('front/images/s4.png') }}"></li>
					<li><img src="{{ asset('front/images/s5.png') }}"></li>
				</ul>
			</div>
			<div class="col-md-4 agile_footer_grid">
				<img src="{{ asset('front/images/logo1.png') }}">

			</div>
			<div class="clearfix"> </div>

		</div>
	</div>
	<div class="agileinfo_copyright">
		<p>© 2018 Sungare. All rights reserved | Design by <a href="http://Sungare.com/">Sungare Technologies</a></p>
	</div>		
	<script src="{{ asset('front/js/bootstrap.js') }}"></script>
	<!-- //for bootstrap working -->





	<script type="text/javascript" src="{{ asset('front/js/scroll.js') }}"></script>

	<script type="text/javascript">
		$(function() {
			$('#example').vTicker();
		});
	</script>
	<script type="text/javascript">
		$(function() {
			$('#example2').vTicker();
		});
	</script>
	<script src="{{ asset('front/js/jssor.slider-27.0.3.min.js') }}" type="text/javascript"></script>
	<script type="text/javascript">
		jssor_1_slider_init = function() {

			var jssor_1_SlideoTransitions = [
			[{b:-1,d:1,o:-0.7}],
			[{b:900,d:2000,x:-379,e:{x:7}}],
			[{b:900,d:2000,x:-379,e:{x:7}}],
			[{b:-1,d:1,o:-1,sX:2,sY:2},{b:0,d:900,x:-171,y:-341,o:1,sX:-2,sY:-2,e:{x:3,y:3,sX:3,sY:3}},{b:900,d:1600,x:-283,o:-1,e:{x:16}}]
			];

			var jssor_1_options = {
				$AutoPlay: 1,
				$SlideDuration: 3000,
				$SlideEasing: $Jease$.$OutQuint,
				$Cols: 1,
				$Align: 0,
				$CaptionSliderOptions: {
					$Class: $JssorCaptionSlideo$,
					$Transitions: jssor_1_SlideoTransitions
				},
				$ArrowNavigatorOptions: {
					$Class: $JssorArrowNavigator$
				},
				$BulletNavigatorOptions: {
					$Class: $JssorBulletNavigator$
				}
			};

			var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

			/*#region responsive code begin*/

			var MAX_WIDTH = 3000;

			function ScaleSlider() {
				var containerElement = jssor_1_slider.$Elmt.parentNode;
				var containerWidth = containerElement.clientWidth;

				if (containerWidth) {

					var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

					jssor_1_slider.$ScaleWidth(expectedWidth);
				}
				else {
					window.setTimeout(ScaleSlider, 30);
				}
			}

			ScaleSlider();

			$Jssor$.$AddEvent(window, "load", ScaleSlider);
			$Jssor$.$AddEvent(window, "resize", ScaleSlider);
			$Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
			/*#endregion responsive code end*/
		};

		function openModal(id, title){
			$('#'+id+'Modal').modal('toggle');
			$('#'+id+'Modal h4 span').html(title);
		}
	</script>
	<script type="text/javascript" src = "{{ asset('js/crud.js') }}"></script>
	<script type="text/javascript">jssor_1_slider_init();</script>		
	@stack('footer')
	<script type="text/javascript">
		$(window).on('load',function(){
			$('#loader').addClass('hidden');
		});
	</script>
</body>
</html>