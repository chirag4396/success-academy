@extends('user.layouts.master')

@section('title')
Home
@endsection

@section('content')
<!-- banner -->
<div class="col-md-12 banner-silder">
    <div class="col-md-8">
        <div class="col-md-12">
            <div>
                <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:1300px;height:650px;overflow:hidden;visibility:hidden;">
                    <!-- Loading Screen -->
                    <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height: 650px;;text-align:center;background-color:rgba(0,0,0,0.7);">
                        <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="{{ asset('img/spin.svg') }}" />
                    </div>
                    <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:1300px;height:650px;overflow:hidden;">
                        @forelse (\App\Banner::get() as $b)                        
                        <div data-p="225.00">
                            <img data-u="image" src="{{ asset($b->ban_img) }}" />
                        </div>
                        @empty                        
                        @endforelse                        
                    </div>
                    <!-- Bullet Navigator -->
                    <div data-u="navigator" class="jssorb032" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
                        <div data-u="prototype" class="i" style="width:16px;height:16px;">
                            <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                                <circle class="b" cx="8000" cy="8000" r="5800"></circle>
                            </svg>
                        </div>
                    </div>
                    <!-- Arrow Navigator -->
                    <div data-u="arrowleft" class="jssora051" style="width:65px;height:65px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
                        <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                            <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
                        </svg>
                    </div>
                    <div data-u="arrowright" class="jssora051" style="width:65px;height:65px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
                        <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                            <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 test2">
            <h2 class="w3l_header">What's New</h2>
            <div class="border-line"></div>
            <div id="example" class="exam">
                <ul>
                    @forelse (\App\WhatsNew::where('wn_status', 1)->get() as $wn)
                        <li>{!! $wn->wn_description !!}</li>
                    @empty                        
                    @endforelse                    
                </ul>
            </div>


        </div>
    </div>
    <div class="col-md-4 testi1" >
        <div id="example2" class="exam2" style="height:auto!important;"  >
            <ul>
                @forelse (\App\Testimonial::get() as $t)
                <li>
                    <div class="testimonial">
                        <i class="fa icon fa-quote-left testimonial_fa" aria-hidden="true"></i>
                        <p class="description">
                            {!! $t->tes_description !!}                                
                        </p>
                        <div class="testimonial-content">
                            <div class="pic"><img src="{{ asset($t->tes_img) }}" alt=""></div>
                            <h3 class="title">{{ $t->tes_name }}</h3>
                            <span class="post">{{ $t->tes_designation }}</span>
                        </div>
                    </div>
                </li>
                @empty
                @endforelse
            </ul>


        </div>   
        
        <div class="col-md-12 white no-padding">
            <div class=" col-md-12 online-test no-padding">
                <div class=" col-md-6 aa no-padding"> <p>Online Test</p></div> 
                <div class=" col-md-6 bb no-padding">
                    @forelse (\App\PackageType::get() as $e => $pte)                    
                    <a href="{{ route('course-pricing', ['type' => strtolower($pte->pt_title)]) }}">
                        <div class=" col-md-12{{ $e == 0 ? 'b-b1' : '' }} no-padding">
                            <p>{{ $pte->pt_title }}</p>
                        </div>
                    </a>
                    @empty
                    @endforelse
                </div> 
            </div>
            <div class=" clearfix"></div>
            <a style="color: #fff;" href="{{ route('lecture-videos') }}">
                <div class="lec-video">Lecture Video</div>
            </a>
            <div class=" clearfix"></div>  
        </div>


    </div>

    <div class="clearfix"></div>
</div>

<div class="clearfix"></div>




<div class="popular-section-wthree ">
    <div class="container"> 
        <div class="wthree-heading">
            <h2 class="w3l_header">About us</h2>
            <div class="border-line"></div>
        </div>
        <p class="bui">The Success Academy is sincerely committed towards the goal of an egalitarian and democratic society where the discriminatory walls between rich and poor, urban and rural, upper and lower castes, men and women, English speaking and vernacular etc don’t inhibit the opportunities for development of one’s inner potential. Moreover it aims at eradication of all such divides..The Success Academy is sincerely committed towards the goal of an egalitarian and democratic society where the discriminatory .</p>



        <div class="col-xs-12 popular-agileinfo">


            <div class=" col-md-8 col-xs-12">

                <div class=" col-md-12 ">
                    <div class="col-md-4 popular-grid">
                        <div class="new">
                            <!-- <i class="fa fa-home" aria-hidden="true"></i> -->
                            <h4>Admission</h4>
                            <p>Itaque earum rerum hic tenetur a sapiente delectus reiciendis maiores hasellus </p>
                        </div>
                    </div>
                    <div class="col-md-4 popular-grid">
                        <div class="new">
                            <!--    <i class="fa fa-bars" aria-hidden="true"></i> -->
                            <h4>Achievements</h4>
                            <p>Itaque earum rerum hic tenetur a sapiente delectus reiciendis maiores hasellus</p>
                        </div>
                    </div>
                    <div class="col-md-4 popular-grid">
                        <div class="new">
                            <!--    <i class="fa fa-bars" aria-hidden="true"></i> -->
                            <h4>FAQs</h4>
                            <p>Itaque earum rerum hic tenetur a sapiente delectus reiciendis maiores hasellus</p>
                        </div>
                    </div>

                </div>
                <div class=" col-md-12 ">

                    <div class="col-md-4 popular-grid popular-grid-bottom">
                        <div class="new">
                            <!-- <i class="fa fa-square-o" aria-hidden="true"></i> -->
                            <h4>Upcoming Batches</h4>
                            <p>Itaque earum rerum hic tenetur a sapiente delectus reiciendis maiores hasellus</p>
                        </div>
                    </div>
                    <div class="col-md-4 popular-grid">
                        <div class="new">
                            <!-- <i class="fa fa-building" aria-hidden="true"></i> -->
                            <h4>May I help You?</h4>
                            <p>Itaque earum rerum hic tenetur a sapiente delectus reiciendis maiores hasellus</p>
                        </div>
                    </div>
                    <div class="col-md-4 popular-grid">
                        <div class="new">
                            <!--    <i class="fa fa-building" aria-hidden="true"></i> -->
                            <h4>Feedback</h4>
                            <p>Itaque earum rerum hic tenetur a sapiente delectus reiciendis maiores hasellus</p>
                        </div>
                    </div>
                </div>
                <div class="clearfix"> </div>
            </div>


            <div class=" col-md-4 ">
                <div class="col-md-12">
                    <img class="img3" src="{{ asset('front/images/bg2.jpg') }}">
                </div>

            </div>
        </div>
    </div>
</div>






<div class="stats">
    <div class="container">
        <div class="wthree-heading">
            <h3 class="w3l_header">Why Success Academy</h3>
            <div class="border-line"></div>

        </div>
        <div class="stats-info-1">
            <p class="stats-info">Specialized &amp; Expert Faculty having thorough knowledge &amp; experience in guiding students for UPSC / MPSC exams, apart from having in-depth knowledge of the particular subjects that they teach. Focus is to develop conceptual clarity through teaching the basic concepts in simple language as well as developing comprehending capabilities of the students. Comprehensive coaching, full…Specialized &amp; Expert Faculty having thorough knowledge &amp; experience in guiding students for UPSC / MPSC exams, apart from having in-depth knowledge of the particular subjects that they teach. Focus is to develop conceptual clarity through teaching the basic concept rough teaching the basic concepts in simple language as well as developing comprehending capabilities of the students. Comprehensive coaching, full…Specialized &amp; Expert Faculty having thorough knowledge &amp; experience in guiding students for UPSC / MPSC exams, apart from having in-depth knowledge of the particular subjects that they teach. Focus is to develop conceptual clarity through teaching the basic concepts in simple language as well as developing comprehending capabilities of the students. Comprehensive coaching, full…s in simple language as well as developing comprehending capabilities of the students. Comprehensive coaching, full…</p>




            <div class="clearfix"></div>
            <div class="col-md-12 col-xs-12 no-pad">

                <div class=" col-md-12 popular-agileinfo ">

                    <div class="col-md-8 popular-grid popular-grid-bottom">
                        <div class="new">

                            <h4>CURRENT EVENTS</h4>
                            <p>Itaque earum rerum hic tenetur a sapiente delectus reiciendis maiores hasellus Itaque earum rerum hic tenetur a sapiente delectus reiciendis maiores hasellusItaque earum rerum hic tenetur a sapiente delectus reiciendis maiores hasellusItaque earum rerum hic tenetur a sapiente delectus reiciendis maiores hasellusItaque earum rerum hic tenetur a sapiente delectus reiciendis maiores hasellus</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <img class="img3" src="{{ asset('front/images/kalam.jpg') }}">
                    </div>

                </div>
                <div class="clearfix"></div>
            </div>

        </div>
    </div>
</div>
@endsection

@push('footer')

@endpush