@extends('user.layouts.master')

@section('title')
Gallery
@endsection
@push('header')
    <link href="{{ asset('front/css/simpleLightbox.css') }}" rel="stylesheet" type="text/css" />    
@endpush
@section('content')
<section class="main-wrapper bg6">
    <div class="gal">
        <div class="container">

            <div class="wthree-heading">
                <h2 class="w3l_header">Photo Gallery

                </h2>
                <div class="border-line"></div>
                {{-- <p class="new2 ">Lorem ipsum dolor sit amet consecter adipsicing elit sed usm tempor incididunt ut reitad dolore magna aliqua ut enim minim beniaps quis nostrual exercitationullamco laboris sed.</p>   --}}
            </div>
            <div class="w3ls_gallery_grids">
                @forelse (\App\Gallery::get() as $g)                    
                <div class="col-md-4 w3_agile_gallery_grid">
                    <div class="agile_gallery_grid">
                        <a href="{{ asset($g->gal_img) }}">
                            <div class="agile_gallery_grid1">
                                <img src="{{ asset($g->gal_img) }}" alt="{{ $g->gal_title }}" class="img-responsive" />
                            </div>
                        </a>
                        {!! $g->gal_description !!}
                    </div>
                </div>
                @empty

                @endforelse
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>

<!-- //gallery -->
<!-- myModal -->
<div class="tooltip-content">

    <div class="modal fade features-modal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">homestead</h4>
                </div>
                <div class="modal-body">
                    <img src="{{ asset('front/images/g4.jpg') }}" class="img-responsive" alt="Boat house">
                    <h5>Fishing Season Now Open</h5>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent varius lectus vitae porttitor fringilla. Donec turpis orci, elementum a nunc quis, maximus varius ipsum. Sed bibendum ex in dignissim bibendum.</p>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- //myModal -->


@endsection

@push('footer')
<script src="{{ asset('front/js/simpleLightbox.js') }}"></script>
<script>
    $('.w3_agile_gallery_grid a').simpleLightbox();
</script>
@endpush