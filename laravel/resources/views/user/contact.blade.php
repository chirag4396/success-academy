@extends('user.layouts.master')

@section('title')
Contact Us
@endsection

@section('content')
<section class="main-wrapper">
    <div class="container">

        <h2 class="w3l_header">Send Enquiry

        </h2>
        <div class="border-line"></div> 
        
    </div>
    <div class="con-top  ">
        <div class="">
            <div class="col-lg-6 col-md-6 col-sm-6 bg7  " data-aos="flip-left">

            </div>
            <div class="col-lg-4  offset-lg-2 col-md-6 col-sm-6 contact-w3-agile1" style="background: #fff;" data-aos="flip-right">
                <div class="contact-agileits contact-w3-agile2">
                    @include('user.forms.enquiry_form', ['id' => 'enq'])                    
                </div>
                <h4 class="w3l-contact" style="color: #c90000;"></h4>
                <p class="contact-agile1"><strong>Phone :</strong>020 - 24432433 , 7276568803 , 7385061249</p>
                <p class="contact-agile1"><strong>Email :</strong> <a href=""> ipsirssuccessacademy@gmail.com</a></p>
                <p class="contact-agile1"><strong>Address :</strong> I. P. Sir's Success Academy, 5th Floor, Kesari Wada,Narayan Peth,Pune 411030</p>                                           
                
                <div class=" agile_footer_grid">

                    <ul class="agileits_social_list">
                        <li><img src="{{ asset('front/images/s1.png') }}"></li>
                        <li><img src="{{ asset('front/images/s2.png') }}"></li>
                        <li><img src="{{ asset('front/images/s3.png') }}"></li>
                        <li><img src="{{ asset('front/images/s4.png') }}"></li>
                        <li><img src="{{ asset('front/images/s5.png') }}"></li>
                    </ul>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
</section>
<!-- map -->
<div class="w3l-map">

    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d242117.68101783327!2d73.72287860996963!3d18.524890422025432!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bc2bf2e67461101%3A0x828d43bf9d9ee343!2sPune%2C+Maharashtra!5e0!3m2!1sen!2sin!4v1522846623962" allowfullscreen></iframe>
</div>
@endsection

@push('footer')

@endpush