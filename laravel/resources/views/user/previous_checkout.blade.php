@extends('user.layouts.master')
@push('header')
	{{-- expr --}}
@endpush

@section('title')
Payment Details
@endsection
@push('header')
@isset ($MERCHANT_KEY)

<script>
	var hash = '{{ $hash }}';	
	function submitPayuForm() {
		if(hash == '') {
			return
		}else{
			$('#loader').show();		
		}
		var payuForm = document.forms.payuForm
		payuForm.submit()
	}
</script>
@endisset

@endpush

@section('content')
<section class="main-wrapper">
	<div class="container">
		<div class="wthree-heading">
			<h2 class="w3l_header">Payment Status</h2>
			<div class="border-line"></div>
		</div>
		<div class="w3ls_banner_bottom_grids">
			<div class="col-md-6 col-md-offset-3 w3layouts_mail_grid_left">
				<div class="agileits_mail_grid_left">
					@if ($status)
					<div class="alert">								 
						<strong>Thank you for your purchase!</strong>								
						<p>We recieved your payment successfully, we'll deliver your order to given address soon, for any query please contact us at {{ Config::get('app.landline') }} or {{ Config::get('app.support_email') }}</p>
					</div>
					@else
					<div class="alert fail">								  
						<strong>Order Failure!</strong> Please Try again or we'll contact to you shortly or please feel free to contact us at <b>{{ config('app.landline') }}</b> or <b>{{ config('app.support_email') }}</b>
					</div>
					@endif
				</div>
			</div>

		</div>
	</div>
	<div class="clearfix"></div>
</section>
@endsection

@push('footer')
@endpush