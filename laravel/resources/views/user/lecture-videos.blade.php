@extends('user.layouts.master')

@section('title')
Lectures Videos
@endsection

@section('content')

<section class="main-wrapper">
	<div class="container">
		<div class="wthree-heading">
			<h2 class="w3l_header"> Our Videos</h2>
			<div class="border-line"></div>	
		</div>

		<section  class="ach smart">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bhoechie-tab-container">
						<div class="col-lg-12 col-md-12 col-sm-3 col-xs-3 bhoechie-tab-menu">
							<div class="list-group">
								@forelse (\App\PackageType::get() as $k => $pt)
								<a href="#" class="list-group-item text-center col-md-6 {{ !is_null($type) ? ($type == strtolower($pt->pt_title) ? 'active' : '') : ($k == 0 ? 'active' : '') }}">
									{{ $pt->pt_title }}
								</a>
								@empty
								@endforelse 
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bhoechie-tab">

							@forelse (\App\PackageType::get() as $k => $ptI)
							<div class="bhoechie-tab-content {{ !is_null($type) ? ($type == strtolower($ptI->pt_title) ? 'active' : '') : ($k == 0 ? 'active' : '') }}">
								<center>
									<div>
										<div>
											@forelse ($ptI->lectures()->get() as $lec)
											<div class="col-md-4 w3_agile_gallery_grid">
												<div class="agile_gallery_grid">
													<div class="agile_gallery_grid1">
														<iframe src="{{ $lec->lec_video }}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
													</div>													
													{!! $lec->lec_description !!}
												</div>

											</div> 
											@empty
											<h1>No Lectures found for {{ $ptI->pt_title }}</h1>
											@endforelse
										</div>
									</div>
								</center>
							</div>
							@empty
							@endforelse
						</div>
						{{-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bhoechie-tab">
							<!-- flight section -->
							<div class="bhoechie-tab-content active">
								<center>
									<div class="w3ls_gallery_grids">
										<div class="col-md-4 w3_agile_gallery_grid">
											<div class="agile_gallery_grid">
												
												<div class="agile_gallery_grid1">
													<iframe src="https://www.youtube.com/embed/sgyQNygopmQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

													
												</div>
												<p>Lorem ipsum dolor sit amet consecter adipsicing elit sed usm tempor incididunt ut reitad dolore magna aliqua ut enim minim beniaps quis nostrual exercitationullamco laboris sed</p>	
												
											</div>
											
										</div>
										<div class="col-md-4 w3_agile_gallery_grid">
											<div class="agile_gallery_grid">
												<div class="agile_gallery_grid1">
													<iframe src="https://www.youtube.com/embed/sgyQNygopmQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

													
												</div>
												<p>Lorem ipsum dolor sit amet consecter adipsicing elit sed usm tempor incididunt ut reitad dolore magna aliqua ut enim minim beniaps quis nostrual exercitationullamco laboris sed</p>
											</div>
											
											
										</div>
										
										<div class="col-md-4 w3_agile_gallery_grid">
											<div class="agile_gallery_grid">
												<div class="agile_gallery_grid1">
													<iframe src="https://www.youtube.com/embed/sgyQNygopmQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

													
												</div>
												<p>Lorem ipsum dolor sit amet consecter adipsicing elit sed usm tempor incididunt ut reitad dolore magna aliqua ut enim minim beniaps quis nostrual exercitationullamco laboris sed</p>
											</div>
											
											
										</div>


										<div class="col-md-4 w3_agile_gallery_grid">
											<div class="agile_gallery_grid">
												<div class="agile_gallery_grid1">
													<iframe src="https://www.youtube.com/embed/sgyQNygopmQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

													
												</div>
												<p>Lorem ipsum dolor sit amet consecter adipsicing elit sed usm tempor incididunt ut reitad dolore magna aliqua ut enim minim beniaps quis nostrual exercitationullamco laboris sed</p>
											</div>
											
											
										</div>
										
									</div>


								</center>
							</div>
							<!-- train section -->
							<div class="bhoechie-tab-content">
								
								
								<center>
									<div class="w3ls_gallery_grids">
										<div class="col-md-4 w3_agile_gallery_grid">
											<div class="agile_gallery_grid">
												
												<div class="agile_gallery_grid1">
													<iframe src="https://www.youtube.com/embed/sgyQNygopmQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

													
												</div>
												<p>Lorem ipsum dolor sit amet consecter adipsicing elit sed usm tempor incididunt ut reitad dolore magna aliqua ut enim minim beniaps quis nostrual exercitationullamco laboris sed</p>	
												
											</div>
											
										</div>
										<div class="col-md-4 w3_agile_gallery_grid">
											<div class="agile_gallery_grid">
												<div class="agile_gallery_grid1">
													<iframe src="https://www.youtube.com/embed/sgyQNygopmQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

													
												</div>
												<p>Lorem ipsum dolor sit amet consecter adipsicing elit sed usm tempor incididunt ut reitad dolore magna aliqua ut enim minim beniaps quis nostrual exercitationullamco laboris sed</p>
											</div>
											
											
										</div>
										
										<div class="col-md-4 w3_agile_gallery_grid">
											<div class="agile_gallery_grid">
												<div class="agile_gallery_grid1">
													<iframe src="https://www.youtube.com/embed/sgyQNygopmQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

													
												</div>
												<p>Lorem ipsum dolor sit amet consecter adipsicing elit sed usm tempor incididunt ut reitad dolore magna aliqua ut enim minim beniaps quis nostrual exercitationullamco laboris sed</p>
											</div>
											
											
										</div>


										<div class="col-md-4 w3_agile_gallery_grid">
											<div class="agile_gallery_grid">
												<div class="agile_gallery_grid1">
													<iframe src="https://www.youtube.com/embed/sgyQNygopmQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

													
												</div>
												<p>Lorem ipsum dolor sit amet consecter adipsicing elit sed usm tempor incididunt ut reitad dolore magna aliqua ut enim minim beniaps quis nostrual exercitationullamco laboris sed</p>
											</div>
											
											
										</div>
										

										
										<div class="col-md-4 w3_agile_gallery_grid">
											<div class="agile_gallery_grid">
												<div class="agile_gallery_grid1">
													<iframe src="https://www.youtube.com/embed/sgyQNygopmQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

													
												</div>
												<p>Lorem ipsum dolor sit amet consecter adipsicing elit sed usm tempor incididunt ut reitad dolore magna aliqua ut enim minim beniaps quis nostrual exercitationullamco laboris sed</p>
											</div>
											
											
										</div>
									</div>


								</center>  
								
							</div>
						</div> --}}
					</div>
				</div>	
			</section>
			<div class="clearfix"></div>

		</div>
	</section>
	@endsection

	@push('footer')

	@endpush