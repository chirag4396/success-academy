@extends('user.layouts.master')

@section('title')
Course Pricing
@endsection

@section('content')

<section class="main-wrapper">
    <div class="container">
        <div class="wthree-heading">
            <h2 class="w3l_header">Coaching Programmes</h2>
            <div class="border-line"></div>
        </div>
        {{-- <p class="new2 ">Lorem ipsum dolor sit amet consecter adipsicing elit sed usm tempor incididunt ut reitad dolore magna aliqua ut enim minim beniaps quis nostrual exercitationullamco laboris sed.</p> --}}
    </div>
    <section  class="ach smart">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bhoechie-tab-container">
                    <div class="col-lg-12 col-md-12 col-sm-3 col-xs-3 bhoechie-tab-menu">
                        <div class="list-group">
                            @forelse (\App\PackageType::get() as $k => $pt)
                            <a href="#" class="list-group-item text-center col-md-6 {{ !is_null($type) ? ($type == strtolower($pt->pt_title) ? 'active' : '') : ($k == 0 ? 'active' : '') }}">
                                {{ $pt->pt_title }}
                            </a>
                            @empty
                            @endforelse 
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bhoechie-tab">

                        @forelse (\App\PackageType::get() as $k => $ptI)
                        <div class="bhoechie-tab-content {{ !is_null($type) ? ($type == strtolower($ptI->pt_title) ? 'active' : '') : ($k == 0 ? 'active' : '') }}">
                            <center>
                                <div>
                                    <div>
                                        @forelse ($ptI->packages()->where('pack_deactive_at', '>=', \Carbon\Carbon::now())->get() as $pack)
                                        <div class="col-md-4  ">
                                            <div class="single-price text-center no">
                                                <div class="price-title">
                                                    <h4>{{ $pack->pack_title }}</h4>
                                                    {{-- <img src="{{ asset('front/images/a1.png') }}"> --}}
                                                    @if ($pack->pack_dis_price)
                                                    <span class="text-large">₹{{ number_format($pack->pack_dis_price) }}/-</span>
                                                    @endif
                                                    <span class="price-dollar" style="text-decoration: line-through;">₹{{ number_format($pack->pack_price) }}/-</span>
                                                </div>
                                                <div class="price-list">
                                                    {!! $pack->pack_description !!}
                                                    <div class="price-btn">
                                                        <a href="{{ route('checkout',['id' => $pack->pack_id]) }}" class="button">Join Now</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                                        @empty
                                        <h1>No Test found for {{ $ptI->pt_title }}</h1>
                                        @endforelse
                                    </div>
                                </div>
                            </center>
                        </div>
                        @empty
                        @endforelse
                    </div>
                </div>
            </div>
        </div>  
    </section>
</section>      
@endsection

@push('footer')

@endpush