@extends('user.layouts.master')

@section('title')
Downloads
@endsection

@section('content')

<section class="main-wrapper">
    <div class="container">
        <div class="wthree-heading">
            <h2 class="w3l_header"> Downloads
            </h2>
            <div class="border-line"></div>
        </div>

        <section  class="ach smart">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bhoechie-tab-container">
                        <div class="col-lg-12 col-md-12 col-sm-3 col-xs-3 bhoechie-tab-menu">
                            <div class="list-group">
                               @forelse (\App\PackageType::get() as $k => $pt)
                               <a href="javascript:;" class="list-group-item text-center col-md-6 {{ !is_null($type) ? ($type == strtolower($pt->pt_title) ? 'active' : '') : ($k == 0 ? 'active' : '') }}">
                                {{ $pt->pt_title }}
                            </a>
                            @empty
                            @endforelse 
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bhoechie-tab">
                        @forelse (\App\PackageType::get() as $k => $ptI)
                        <div class="bhoechie-tab-content {{ !is_null($type) ? ($type == strtolower($ptI->pt_title) ? 'active' : '') : ($k == 0 ? 'active' : '') }}">
                            <center>
                                @forelse ($ptI->downloads()->get() as $d)
                                <div class="success-view">        
                                    <span class="success-field-content">
                                        <ul class="arrows">
                                            <li>{{ $d->d_title }}
                                                <a href="{{ asset($d->d_pdf) }}" target="_BLANK" title="{{ $d->d_title }}"> {{ number_format((filesize($d->d_pdf)/ 1024),2). ' KB' }}
                                                </a>
                                            </li>
                                        </ul>
                                    </span> 
                                </div>  
                                @empty
                                <div class="success-view">        
                                    <span class="success-field-content text-center">
                                        No files found for {{ $ptI->pt_title }}
                                    </span> 
                                </div> 
                                @endforelse
                            </center>
                        </div>
                        @empty
                        @endforelse
                    </div>
                </div>
            </div>
        </div>  
    </section>
    <div class="clearfix"></div>
</div>
</section>



@endsection

@push('footer')

@endpush