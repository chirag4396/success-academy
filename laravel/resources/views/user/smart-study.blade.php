@extends('user.layouts.master')

@section('title')
Smart Study
@endsection

@section('content')
<section class="main-wrapper">
    <div class="container">
        <div class="wthree-heading">
            <h2 class="w3l_header">Smart Study</h2>
            <div class="border-line"></div>    
        </div>
        <section  class="ach smart">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bhoechie-tab-container">
                        <div class="col-lg-12 col-md-12 col-sm-3 col-xs-3 bhoechie-tab-menu">
                            <div class="list-group">
                                @forelse (\App\PackageType::get() as $k => $pt)
                                <a href="#" class="list-group-item text-center col-md-6 {{ !is_null($type) ? ($type == strtolower($pt->pt_title) ? 'active' : '') : ($k == 0 ? 'active' : '') }}">
                                    {{ $pt->pt_title }}
                                </a>
                                @empty
                                @endforelse
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bhoechie-tab">
                            <!-- flight section -->
                            @forelse (\App\PackageType::get() as $k => $ptI)
                            <div class="bhoechie-tab-content {{ !is_null($type) ? ($type == strtolower($ptI->pt_title) ? 'active' : '') : ($k == 0 ? 'active' : '') }}">
                                <center>
                                    <div class="row achv">
                                        @forelse ($ptI->smartStudies()->get() as $ss)
                                            
                                        <div class="col-lg-3 col-md-6 mb-5 mb-lg-0  br ac2">
                                            <img src="{{ asset($ss->ss_img) }}">
                                            <div class="smart-study">
                                                <a href="{{ asset($ss->ss_pdf) }}" target="_BLANK" title="{{ $ss->ss_title }}" class="col-md-6 col-xs-12 bg11">View</a>
                                                <button class=" col-md-6 col-xs-12 bg22" onclick="openModal('smart', '{{ $ss->ss_title }}');">Enquire</button>
                                            </div>
                                        </div>
                                        @empty
                                            <h1>No Smart Study data found for {{ $ptI->pt_title }}</h1>
                                        @endforelse
                                    </div>                                    
                                </center>
                            </div>
                            @empty
                            @endforelse                            
                        </div>
                    </div>
                </div>
            </div> 
        </section>
        <div class="clearfix"></div>
    </div>
</section>
<div id="smartModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Smart Studies Enquiry for <span></span></h4>
            </div>
            <div class="modal-body">
                @include('user.forms.enquiry_form', ['id' => 'smart', 'type' => 2])
            </div>      
        </div>

    </div>
</div>
@endsection

@push('footer')

@endpush