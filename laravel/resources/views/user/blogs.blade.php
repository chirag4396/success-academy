@extends('user.layouts.master')

@section('title')
Blogs
@endsection

@section('content')
<section class="main-wrapper bg6">
    <div class="gal">
        <div class="container">
            <div class="wthree-heading">
                <h2 class="w3l_header">Blog</h2>
                <div class="border-line"></div>
            </div>
            <div class="container">
                <div class="row" style="background: #fff;">
                    <div class="col-md-8">
                        @forelse ($blogs as $blog)                        
                        <div class="card mb-4">
                            <img class="card-img-top" src="{{ asset($blog->blog_img) }}" alt="{{ $blog->blog_title }}">
                            <div class="card-body">
                                <h2 class="card-title">{{ $blog->blog_title }}</h2>
                                <div class="card-text">
                                    {!! $blog->blog_description !!}
                                </div>
                            </div>
                            <div class="card-footer blog1 col-xs-12 text-muted">
                                <p class="col-lg-8">Posted on {{ $blog->blog_created_at->diffForHumans() }}</p>
                            </div>
                        </div>
                        @empty

                        @endforelse                    
                        <ul class="pagination justify-content-center mb-4">
                            @if ($blogs->previousPageUrl())                                
                            <li class="page-item">
                                <a class="page-link" href="{{ $blogs->previousPageUrl() }}">← Older</a>
                            </li>
                            @endif
                            @if ($blogs->nextPageUrl())

                            <li class="page-item">
                                <a class="page-link" href="{{ $blogs->nextPageUrl() }}">Newer →</a>
                            </li>
                            @endif
                        </ul>
                    </div>                    
                </div>
            </div>
        </div>
    </div>
</section>
@endsection