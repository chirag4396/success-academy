@extends('user.layouts.master')

@section('title')
FAQ's
@endsection

@section('content')
<section class="main-wrapper">
    <div class="container">
        <div class="wthree-heading">
            <h2 class="w3l_header">UPSC/MPSC FAQs

            </h2>
            <div class="border-line"></div>
        </div>
        

    </div>


    <div class="container">
        <div class="col-md-3">
            <ul class="list-group help-group">
                <div class="faq-list list-group nav nav-tabs">
                    @forelse (\App\PackageType::get() as $k => $pt)
                    <a href="#{{ strtolower(str_replace(' ', '-', $pt->pt_title)) }}" class="list-group-item {{ !is_null($type) ? ($type == strtolower($pt->pt_title) ? 'active' : '') : ($k == 0 ? 'active' : '') }}" role="tab" data-toggle="tab"><i class="mdi mdi-account"></i> {{ $pt->pt_title }}</a>                    
                    @empty
                    @endforelse
                </div>
            </ul>
        </div>
        <div class="col-md-9">
            <div class="tab-content panels-faq">
                @forelse (\App\PackageType::get() as $k => $ptI)
                <div class="tab-pane {{ !is_null($type) ? ($type == strtolower($ptI->pt_title) ? 'active' : '') : ($k == 0 ? 'active' : '') }}" id="{{ strtolower(str_replace(' ', '-', $ptI->pt_title)) }}">
                    <div class="panel-group">
                        @forelse ($ptI->faqs()->get() as $ki => $f)
                        <div class="panel panel-default panel-help">
                            <a href="#faq{{ $f->faq_id }}" data-toggle="collapse" data-parent="#help-accordion-1">
                                <div class="panel-heading">
                                    <h2>{!! $f->faq_qus !!}</h2>
                                </div>
                            </a>
                            <div id="faq{{ $f->faq_id }}" class="collapse{{ $k == 0 ? ' in' : '' }}">
                                <div class="panel-body">
                                    {!! $f->faq_ans !!}
                                </div>
                            </div>
                        </div>
                        @empty

                        @endforelse                        
                    </div>
                </div>
                @empty
                @endforelse
            </div>    
        </div>
    </div>






</section>      

@endsection

@push('footer')
<script type="text/javascript">
    $(function() {
        // Since there's no list-group/tab integration in Bootstrap
        $('.list-group-item').on('click',function(e){
          var previous = $(this).closest(".list-group").children(".active");
              previous.removeClass('active'); // previous list-item
              $(e.target).addClass('active'); // activated list-item
          });
    });
</script>
@endpush