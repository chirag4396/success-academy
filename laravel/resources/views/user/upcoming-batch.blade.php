@extends('user.layouts.master')

@section('title')
Upcoming Batch
@endsection
@push('header')
@php
	$ID = "up";
@endphp
@endpush
@section('content')
<section class="main-wrapper">
	<div class="container">
		<div class="wthree-heading">
			<h2 class="w3l_header">Upcoming Batches</h2>
			<div class="border-line"></div>	
		</div>
		<section class="ach smart">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bhoechie-tab-container">
						<div class="col-lg-12 col-md-12 col-sm-3 col-xs-3 bhoechie-tab-menu">
							<div class="list-group">
								@forelse (\App\PackageType::get() as $k => $pt)
								<a href="javascript:;" class="list-group-item text-center col-md-6 {{ !is_null($type) ? ($type == strtolower($pt->pt_title) ? 'active' : '') : ($k == 0 ? 'active' : '') }}">
									{{ $pt->pt_title }}
								</a>
								@empty
								@endforelse  
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bhoechie-tab no-padding">
							<!-- flight section -->
							@forelse (\App\PackageType::get() as $k => $ptI)
							<div class="bhoechie-tab-content {{ !is_null($type) ? ($type == strtolower($ptI->pt_title) ? 'active' : '') : ($k == 0 ? 'active' : '') }} no-padding">
								<div class="col-md-12 w3_agile_gallery_grid ">
									@forelse ($ptI->upcomingBatches()->get() as $up)
									<div class=" col-md-6 blue no-padding">
										<div class="col-md-5 no-padding">
											<div class="agile_gallery_grid1">
												<img src="{{ asset($up->ub_img) }}" alt="{{ $up->ub_title }}" class="img-responsive">
												<button onclick="openModal('up', '{{ $up->ub_title }}');">Enquiry</button>
											</div>
										</div>
										<div class="col-md-7 no-padding">
											<h1>{{ str_limit($up->ub_title,20) }}</h1> 
											<div class="up-des">{!! $up->ub_description !!}</div>
										</div>
									</div>

									@empty
									<h1>No Upcoming batches found for {{ $ptI->pt_title }}</h1>
									@endforelse
								</div>									
							</div>
							@empty
							@endforelse
						</div>
					</div>
				</div>
			</div>	
		</section>
		<div class="clearfix"></div>
	</div>
</section>
<div class="clearfix"></div>

<div id="upModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Upcoming Batch Enquiry for <span></span></h4>
			</div>
			<div class="modal-body">
				@include('user.forms.enquiry_form', ['id' => $ID, 'type' => 1])
			</div>      
		</div>

	</div>
</div>
@endsection

@push('footer')
{{-- <script type="text/javascript">
	$('#upModal').modal('toggle');
</script> --}}
@endpush