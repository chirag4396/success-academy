@extends('user.layouts.master')
@section('title')

@endsection

@push('header')
@isset ($MERCHANT_KEY)

<script>
	var hash = '{{ $hash }}';	
	function submitPayuForm() {
		if(hash == '') {
			return
		}else{
			$('#loader').removeClass('hidden');		
			$('#loader p').html('Taking you to PayUMoney, Please Wait!!');
		}
		var payuForm = document.forms.payuForm
		payuForm.submit()
	}
</script>
@endisset

@endpush
@php
$Log = 'login';
@endphp
@section('body')
onload="submitPayuForm();"
@endsection
@section('content')
<section class="main-wrapper">
	<div class="container">
		<div class="wthree-heading">
			<h2 class="w3l_header">Payment</h2>
			<div class="border-line"></div>
		</div>
		<div class="w3ls_banner_bottom_grids">
			<div class="col-md-6 col-md-offset-3 w3layouts_mail_grid_left">
				<div class="agileits_mail_grid_left">					
					<form action="{{ $action }}" method="post" name="payuForm">
						{{ csrf_field() }}
						<input type="hidden" name="key" value="{{ $MERCHANT_KEY }}" />
						<input type="hidden" name="hash" value="{{ $hash }}"/>
						<input type="hidden" name="txnid" value="{{ $txnid }}" />
						<input type="hidden" name="service_provider" value="payu_paisa" size="64" />

						<input type = "hidden" name="productinfo" value="{{ (empty($posted['productinfo'])) ? 'MyFabShop' : $posted['productinfo'] }}" size="64" />

						<input type = "hidden" name="surl" value="{{ (empty($posted['surl'])) ? route('orderStatus') : $posted['surl'] }}" size="64" />

						<input type = "hidden" name="furl" value="{{  (empty($posted['furl'])) ? route('orderStatus') : $posted['furl']  }}" size="64" />

						<input type="hidden" name="package" value = "1">

						<div class="control-group form-group">
							<div class="controls">
								<label>Amount</label>
								<input required class="form-control"  name="amount" type="number"  value="{{ (empty($posted['amount'])) ? (is_null($package->pack_dis_price) ? $package->pack_price : $package->pack_dis_price) : $posted['amount'] }}" readonly />
							</div>
						</div>
						<div class="control-group form-group">
							<div class="controls">
								<label>Full Name</label>								
								<input required class="form-control" name="firstname" type="text" id="firstname" value="{{ (empty($posted['firstname'])) ? (Auth::user() ? Auth::user()->name : '') : $posted['firstname'] }}" />
							</div>
						</div>						
						<div class="control-group form-group">
							<div class="controls">
								<label>Email Address</label>
								<input required class="form-control" name="email" type="email" autocomplete="off" id="email" value="{{ (empty($posted['email'])) ? (Auth::user() ? Auth::user()->email : '') : $posted['email'] }}" />
							</div>
						</div>
						<div class="control-group form-group">
							<div class="controls">
								<label>Phone</label>
								<input required id = "phone" class="form-control" type="number" name="phone" value="{{ (empty($posted['phone'])) ? '' : $posted['phone'] }}" />

							</div>
						</div>										
						<div class="control-group form-group">
							<div class="controls">
								<label>Address</label>
								<textarea class="form-control" name="address" required>{{ (empty($posted['address']) ? '' : $posted['address']) }}</textarea>												
							</div>
						</div>											
						<input type="hidden" name="udf1" value="{{ (empty($posted['udf1'])) ? '' : $posted['udf1'] }}" />

						<input type="hidden" name="udf2" value="{{ (empty($posted['udf2'])) ? '' : $posted['udf2'] }}" />

						<input type="hidden" name="udf3" value="{{ (empty($posted['udf3'])) ? '' : $posted['udf3'] }}" />

						<input type="hidden" name="udf4" value="{{  (empty($posted['udf4'])) ? '' : $posted['udf4'] }}" />

						<input type="hidden" name="udf5" value="{{ (empty($posted['udf5'])) ? '' : $posted['udf5'] }}" />

						<input type="hidden" name="pg" value="{{(empty($posted['pg'])) ? '' : $posted['pg'] }}" type="hidden" />

						<div class="clearfix"></div>

						<div class="text-center" style="margin-bottom: 20px;">
							<input id = "proceedBtn" class="btn btn-primary" type="submit" value="Pay Now">
						</div>

					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
</section>
@endsection