<form id = "{{ $id }}Form">
	@isset ($type)
		<input type="hidden" name="type" value="{{ $type }}">
	@endisset
	<div class="control-group form-group">
		<div class="controls">
			<input type="text" placeholder="Enter Name" class="form-control" name="name">
			<p class="help-block"></p>
		</div>
	</div>  
	<div class="control-group form-group">
		<div class="controls">
			<input type="number" class="form-control" placeholder="Phone Number" name="mobile">
			<p class="help-block"></p>
		</div>
	</div>
	<div class="control-group form-group">
		<div class="controls">
			<input type="email" class="form-control" placeholder="Email Address" name="email">
			<p class="help-block"></p>
		</div>
	</div>
	<div class="control-group form-group">
		<div class="controls">
			<textarea class="form-control" name = "query" placeholder="Enter Message here"></textarea>
			<p class="help-block"></p>
		</div>
	</div>
	<button type="submit" class="btn btn-primary">Send</button> 
</form>   

@push('footer')
	<script type="text/javascript">
		$('#{{ $id }}Form').CRUD({
		    url : '{{ route('admin.enquiry.store') }}',
		    processResponse : function(data){
		     	console.log(data);
		    }
		});
	</script>
@endpush