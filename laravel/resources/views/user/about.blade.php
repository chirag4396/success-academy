@extends('user.layouts.master')

@section('title')
About Us
@endsection

@section('content')
<!-- banner -->

<section class="main-wrapper">
	<div class="container">
		<div class="wthree-heading">
			<h2 class="w3l_header">About us</h2>
			<div class="border-line"></div>	

			<p class="new2 ">Lorem ipsum dolor sit amet consecter adipsicing elit sed usm tempor incididunt ut reitad dolore magna aliqua ut enim minim beniaps quis nostrual exercitationullamco laboris spsum dolor sit amet consecter adipsicing elit sed usm tempor incididunt ut reitad dolore magna aliqua ut enim minim beniaps quis nostrual exercitationullamco laboris spsum dolor sit amet consecter adipsicing elit sed usm tempor incididunt ut reitad dolore magna aliqua ut enim minim beniaps quis nostrual exercitationullamco laboris sed.</p>	
		</div>

		<div class="about1">


			<div class="col-md-6 col-xs-12">
				<img class="img3" src="{{ asset('front/images/aim.png') }}">
			</div>
			<div class="col-md-6 col-xs-12">
				<h3 class="inner"> Aim</h3>
				<div class="border-line"></div>
				<p>Lorem ipsum dolor sit amet consecter adipsicing elit sed usm tempor incididunt ut reitad dolore magna aliqua ut enim minim beniaps quis nostrual exercitationullamco laboris sed.Lorem ipsum dolor sit amet consecter adipsicing elit sed usm tempor incididunt ut reitad dolore magna aliqua ut enim minim beniaps quis nostrual exercitationullamco laboris sed.Lorem ipsum dolor sit amet consecter adipsicing elit sed usm tempor incididunt ut reitad dolore magna aliqua ut enim minim beniaps quis nostrual exercitationullamco laboris sed.
				</p>
			</div>

			<div class=" clearfix"></div>
		</div>	
	</div>
	<section class="vision">
		<div class="container">
			
			<div class="col-md-4 col-xs-12 a1 q1">							
				<h3 class="inner" style="color: #fff;">Vision</h3>
				<div class="border-line"></div>
				<p >Our vision is to impart our students with high standard tutoring and for nurturing young minds to act as advanced leaders for the advancement of society and mankind which in turn can contribute towards the development of our country.
				</p>
			</div>
			


			<div class="col-md-4 col-xs-12">
				<img class="img3" src="{{ asset('front/images/vision.png') }}">
			</div>
			<div class="col-md-4 col-xs-12 a1 q2">
				<h3 class="inner">Mission</h3>
				<div class="border-line"></div>
				<p>Lorem ipsum dolor sit amet consecter adipsicing elit sed usm tempor incididunt ut reitad dolore magna aliqua ut enim minim beniaps quis nostrual exercitationullamco laboris sed.Lorem ipsum dolor sit amet tationullamco laboris sed.
				</p>
			</div>
			<div class=" clearfix"></div>
		</div>
	</section>
	<div class="container">

		<div class="col-md-6 col-xs-12">
			<img class="img3" src="{{ asset('front/images/333.png') }}">
		</div>
		<div class="col-md-6 col-xs-12">
			<h3 class="inner"> 360 APPROACH</h3>
			<div class="border-line"></div>
			<p>Lorem ipsum dolor sit amet consecter adipsicing elit sed usm tempor incididunt ut reitad dolore magna aliqua ut enim minim beniaps quis nostrual exercitationullamco laboris sed.Lorem ipsum dolor sit amet consecter adipsicing elit sed usm tempor incididunt ut reitad dolore magna aliqua ut enim minim beniaps quis nostrual exercitationullamco laboris sed.Lorem ipsum dolor siolore magna aliqua ut enim minim beniaps quis nostrual exercitationullamco laboris sed.Lorem ipsum dolor sit amet consecter adipsicing elit sed usm tempor incididunt ut reitad dolore magna aliqua ut enim minim beniaps quis nostrual e
			</p>
		</div>

		<div class=" clearfix"></div>

	</div>
</section>		

<section class="directr pt">
	<div class="container">
		<div class="col-md-5 col-xs-12">
			<img class="img3" src="{{ asset('front/images/dd.jpg') }}">
		</br>
		<p class="pp"><b>Ponny Chacko</b> (Director)</p>
	</div>
	<div class="col-md-7 col-xs-12 " style="background:#ffffffbd;">
		<h3 class="inner"> From Director’s Desk</h3>
		
		<p>To significantly enhance the self-confidence level for developing creative skills of staff and students.To significantly enhance the self-confidence level for developing creative skills of staff and students. To create an excellent teaching and learning environment for our staff and students to realize their full potential thus enabling them to contribute positively to the community. To significantly enhance the self-confidence level for developing creative skills of staff and students.</p>
	</div>
	<div class=" clearfix"></div>
</section>


@endsection

@push('footer')

@endpush