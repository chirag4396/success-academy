@extends('user.layouts.master')

@section('title')
Achievements
@endsection

@section('content')

<section class="main-wrapper">
    <div class="container">
        <div class="wthree-heading">
            <h2 class="w3l_header">ACHIEVEMENTS
            </h2>
            <div class="border-line"></div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bhoechie-tab-container">
            <div class="col-lg-12 col-md-12 col-sm-3 col-xs-3 bhoechie-tab-menu">
                <div class="list-group">
                    @forelse (\App\PackageType::get() as $k => $pt)
                    <a href="#" class="list-group-item text-center col-md-6 {{ !is_null($type) ? ($type == strtolower($pt->pt_title) ? 'active' : '') : ($k == 0 ? 'active' : '') }}">
                        {{ $pt->pt_title }}
                    </a>
                    @empty
                    @endforelse                    
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bhoechie-tab">
                @forelse (\App\PackageType::get() as $k => $ptI)
                <div class="bhoechie-tab-content {{ !is_null($type) ? ($type == strtolower($ptI->pt_title) ? 'active' : '') : ($k == 0 ? 'active' : '') }}">
                    @php
                    $ats = $ptI->achievementTypes();
                    @endphp
                    @if ($ats->count()>1)
                    <div class="succ-tab">
                        @forelse ($ats->get() as $ak => $atI)
                        <input type="radio" name="succ-tab" id="{{ strtolower(str_replace('', '-', $atI->at_title)) }}" {{ $ak == 0 ? 'checked="checked"' : '' }}>
                        <label for="{{ strtolower(str_replace('', '-', $atI->at_title)) }}">{{ $atI->at_title }}</label>

                        <div class="tab">
                            <center>
                                <div class="row achv"> 
                                    @forelse ($atI->achievements()->get() as $att)
                                    <div class="col-lg-4 col-md-6 mb-5 mb-lg-0  br ac1">
                                        <img src="{{ asset($att->ach_img) }}">
                                        <h4>
                                            <strong>{{ $att->ach_name }}</strong>
                                            <p style="text-align: center;color: #d26a00;"> ( {{ $att->ach_sub_name }} )</p>
                                        </h4>
                                        <div class="text-faded mb-0  ach-des"> {!! $att->ach_description !!}</div>
                                    </div>
                                    @empty
                                    @endforelse
                                </div>
                            </center>
                        </div>
                        @empty

                        @endforelse
                    </div>
                    @else                    
                    <h2 class="w3l_header">
                        {{ $ats->first()->at_title }}
                    </h2>
                    <div class="border-line"></div>

                    <center>
                        <div class="row achv">
                            @forelse ($ats->first()->achievements()->get() as $aa)
                                <div class="col-lg-4 col-md-6 mb-5 mb-lg-0  br ac1">
                                    <img src="{{ asset($aa->ach_img) }}">
                                    <h4>
                                        <strong>{{ $aa->ach_name }}</strong>
                                        <p style="text-align: center;color: #d26a00;"> ( {{ $aa->ach_sub_name }} )</p>
                                    </h4>
                                    <div class="text-faded mb-0 ach-des"> {!! $aa->ach_description !!}</div>
                                </div>
                            @empty
                            <h1></h1>
                            @endforelse
                        </div>
                    </center>
                    @endif
                </div>
                @empty
                @endforelse               
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</section>
@endsection

@push('footer')

@endpush