@extends('layouts.simple.master')

@section('content')

<div class="container-fluid">
    <div class="row">                   

        <div class="col-md-offset-4 col-md-4">
            <div class="card">
                <div class="card-header" data-background-color="blue">
                    <h4 class="title">Registeration</h4>
                    <p class="category">All fields are Mandatory</p>
                </div>
                <div class="card-content">                                
                    <div class="col-md-12">

                        <form id = "candidateForm">

                            <div class="form-group label-floating">
                                <label for="name" class="control-label left">Name</label>

                                <input id="name" type="text" class="form-control" name="name" required autofocus>

                            </div>

                            <div class="form-group label-floating">
                                <label for="email" class="control-label left">E-Mail Address</label>


                                <input id="email" type="email" class="form-control" name="email" required>

                            </div>

                            <div class="form-group label-floating">
                                <label for="mobile" class="control-label left">Mobile</label>


                                <input id="mobile" type="text" class="form-control" name="mobile" required>

                                {{-- <div id = "mobileMsg"></div> --}}
                            </div>

                            
                            <div class="form-group label-floating">
                                <label for="batch" class="control-label left">Batch</label>
                                
                                <select class="form-control" id = "batchSelection">
                                    <option>-Batch-</option>
                                    @forelse (App\Batch::get() as $batch)
                                    <option value="{{ $batch->batch_id }}">{{ $batch->batch_name }}</option>
                                    @empty

                                    @endforelse
                                    <option value = "other">Others</option>
                                </select>
                                
                            </div>
                            <input type="hidden" name="batch" id = "batch">
                            <div class="form-group hidden" id="batchOther">

                                <div class="input-group">
                                    <input type="text" class="form-control">
                                    <div class="input-group-btn">
                                        <button id = "addBatch" class="btn btn-default">
                                            Add
                                        </button>
                                    </div>
                                </div>
                                
                            </div>  
                            <div class="form-group">
                                <label for="password" class="control-label left">Password</label>

                                
                                <input id="password" type="password" class="form-control" name="password" required>
                                
                            </div>
                            <div class="form-group">
                                <label for="confirm-password" class="control-label left">Confirm Password</label>

                                
                                <input id="confirm-password" type="password" class="form-control" required>
                                
                            </div> 
                            <input type="hidden" name="type" value="102">

                            <div class="form-group label-floating">
                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary btn-round">
                                        Register
                                    </button>
                                </div>
                            </div>
                        </form>
                        <div class="alert alert-success zero-padding text-center hidden" id = "candidateMessage"></div>



                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>


@endsection

@push('footer')
<script type="text/javascript" src = "{{ asset('js/candidate.js') }}"></script>

@endpush
