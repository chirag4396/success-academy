@extends('layouts.app')

@section('title')
All Candidates
@endsection
@push('header')
<link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.dataTables.min.css') }}">
@endpush
@section('content')
{{--<div class="container">--}}
    {{--<div class="row">--}}
        {{--<div class="col-md-12">--}}
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="col-md-2">
                        <h5>All Candidates</h5>
                    </div>
                    <div class="col-md-2">
                        <button type="button" class="btn btn-primary" id = "addCandidateModal">Add New</button>
                    </div>
                    <div class="col-md-2 pull-right">
                        <select id = "sDesignation" class="form-control">
                            <option value="-1">-Designation-</option>
                            @forelse (App\Designation::get() as $designation)
                            <option value="{{ $designation->des_id }}">{{ $designation->des_name }}</option>
                            @empty

                            @endforelse
                        </select>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">

                    <table id = "user_role_table">
                        <thead>
                            <tr>
                                <th><input type="checkbox" name="select_all" value="1" id="example-select-all"></th>
                                <th>Name</th>
                                <th>Experience</th>
                                <th>Mobile</th>                                
                                <th>Remark</th>
                                <th>Status</th>
                                <th>Action</th>
                                <th>Designation</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div id="addCanModal" class="modal fade" role="dialog">
                <div class="modal-dialog">              
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Add new Candidate Details</h4>
                        </div>
                        <div class="modal-body">
                            <form class="form-horizontal" id = "acandidateForm" >
                                <input type="hidden" id="id" name = "id">
                                <div class="form-group">
                                    <label for="highest" class="col-md-4 control-label">Selecte Designation</label>
                                    <div class="col-md-6">
                                        <select class="form-control" id = "designationSelection">
                                            <option value="-1">-Designation-</option>
                                            @forelse (App\Designation::get() as $des)
                                            <option value = "{{ $des->des_id }}">{{ $des->des_name }}</option>

                                            @empty

                                            @endforelse
                                            <option value = "other">Others</option>
                                        </select>
                                    </div>
                                </div>      
                                <input type="hidden" name = "designation" id = "designation">

                                <div class="form-group hidden" id="desOther">
                                    <div class="col-md-offset-4 col-md-6">
                                        <div class="input-group">
                                            <input type="text" class="form-control">
                                            <div class="input-group-btn">
                                                <button type="button" id = "addDes" class="btn btn-default" >Add</button>
                                            </div>
                                        </div>                            
                                    </div>
                                </div>  
                                <div class="form-group">
                                    <label for="name" class="col-md-4 control-label">Name</label>

                                    <div class="col-md-6">
                                        <input id="name" type="text" class="form-control" name="name" required autofocus>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                    <div class="col-md-6">
                                        <input id="email" type="email" class="form-control" name="email" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="mobile" class="col-md-4 control-label">Mobile</label>

                                    <div class="col-md-6">
                                        <input id="mobile" type="text" class="form-control" name="mobile" required>
                                        <div id = "mobileMsg"></div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="alt-mobile" class="col-md-4 control-label">Alternate Mobile</label>

                                    <div class="col-md-6">
                                        <input id="alt-mobile" type="text" class="form-control" name="alt_mobile">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label for="highest" class="col-md-4 control-label">Highest Degree</label>
                                    <div class="col-md-6">
                                        <select class="form-control" id = "highestSelection">
                                            <option value="-1">-Degree-</option>
                                            @forelse (App\Degree::get() as $degree)
                                            <option value="{{ $degree->degree_id }}">{{ $degree->degree_name }}</option>
                                            @empty

                                            @endforelse
                                            <option value = "other">Others</option>
                                        </select>
                                    </div>
                                </div>
                                <input type="hidden" name="highest_degree" id = "highest">
                                <div class="form-group hidden" id="highestOther">
                                    <div class="col-md-offset-4 col-md-6">
                                        <div class="input-group">
                                            <input type="text" class="form-control">
                                            <div class="input-group-btn">
                                                <button type = "button" id = "addDegree" class="btn btn-default">Add</button>
                                            </div>
                                        </div>                            
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="highest" class="col-md-4 control-label">Passout Year</label>
                                    <div class="col-md-6">
                                        <select class="form-control" name = "passout_year" id = "passout_yearSelection">
                                            <option value="-1">-Year-</option>
                                            @for ($i = 2010; $i <= 2018; $i++)
                                            <option value="{{ $i }}">{{ $i }}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">                            
                                    <div class="col-md-offset-4 col-md-3">
                                        <input type="radio" name="type" id = "fresher" value="0"> Fresher
                                    </div>
                                    <div class="col-md-3">
                                        <input type="radio" name="type" id = "exprienced"> Experienced

                                    </div>
                                </div>
                                <div class="panel panel-default panel-body hidden" id = "typeSection">
                                    <div id = "exprienceSection" class="hidden">
                                        <div class="form-group">
                                            <label for="alt-mobile" class="col-md-4 control-label">Experience</label>
                                            <div class="col-md-3">
                                                <select class="form-control" name = "exp_year">
                                                    <option value="-1">-Year-</option>
                                                    @for ($i = 0; $i <= 10; $i++)
                                                    <option value="{{ $i }}">{{ $i }}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <select class="form-control" name = "exp_month">
                                                    <option value="-1">-Month-</option>
                                                    @for ($i = 1; $i <= 12; $i++)
                                                    <option value="{{ $i }}">{{ $i }}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="alt-mobile" class="col-md-4 control-label">Current CTC</label>

                                            <div class="col-md-6">
                                                <div class="input-group">                                    
                                                    <input id="alt-mobile" type="text" class="form-control" name="current_ctc">
                                                    <span class="input-group-addon">lacs</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id = "fresherSection">
                                        <div class="form-group">
                                            <label for="alt-mobile" class="col-md-4 control-label">Expected CTC</label>

                                            <div class="col-md-6">
                                                <div class="input-group">                                    
                                                    <input id="alt-mobile" type="text" class="form-control" name="expected_ctc">
                                                    <span class="input-group-addon">lacs</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="alt-mobile" class="col-md-4 control-label">Skills you know</label>
                                            <div class="col-md-8">                                
                                                @forelse (App\Skill::get() as $skill)
                                                <div class="col-md-4">
                                                    <input type="checkbox" name = "skill[]" value = "{{ $skill->skill_id }}"> {{ $skill->skill_name }}
                                                </div>                                
                                                @empty
                                                {{-- empty expr --}}
                                                @endforelse                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="text-center" id = "formButton">
                                        <input type="submit" class="btn btn-primary" id = "addCandidate" value="Register">

                                    </div>
                                </div>
                            </form>
                            <div class="alert alert-success text-center hidden" id = "acandidateMessage"></div>
                        </div>                        
                    </div>
                </div>
            </div>
            <div id="remarkModal" class="modal fade" role="dialog">
                <div class="modal-dialog">              
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Remark for <span id = "remarkPName"> </span> Details</h4>
                        </div>
                        <div class="modal-body text-center" id = "remarkBody">                        
                        </div>                        
                    </div>
                </div>
            </div>
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}
@endsection

@push('footer')
<script type="text/javascript" src = "{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src = "{{ asset('js/view-candidate.js') }}"></script>
 <script type="text/javascript" src = "{{ asset('js/candidate.js') }}"></script>
<script type="text/javascript" src = "{{ asset('js/excel.js') }}"></script>
@endpush