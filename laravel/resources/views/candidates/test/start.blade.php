@extends('layouts.topbar')

@section('title')
	Start Test
@endsection

@section('content')
<div class="container-fluid">
	<div class="row">                   

		<div class="col-md-offset-2 col-md-8">
			{{-- @if (!$exists) --}}
			<div class="card">
				<div class="card-header" data-background-color="blue">
					<h4 class="title">Rules</h4>	                    
				</div>
				<div class="card-content">                                
					<div class="col-md-12">	                        
						<ul class="ul">
							<li>
								You have to complete each question in 2 minutes, if you somehow not able to manage selecting option, that question will be skipped and considered 0 as mark.
							</li>
							<li>
								After pressing next button you'll not able to go back to previous question.
							</li>
							<li>
								No negative marking is there for wrong selection.
							</li>
							<li>
								You have to score atleast 60% in these test otherwise you will not be eligible for Machine round.
							</li>
							<li>
								No Malpractices. If we found any, you'll not be eligible for next round and immediately terminated from current round also.
							</li>
						</ul>
						<h2 class="text-center">Best of Luck</h2>
						<div class="text-center">
							<form action="{{ route('candidate.test.startTest') }}">
								<input type="hidden" name="test_id" value="{{ $id }}">
								<button class="btn btn-primary">Start Test</button>                        
							</form>
							<br>
							<a href="{{route('candidate.test.c_home')}}" class="btn btn-danger">Go Back to Dashboard</a>
							
						</div>

					</div>
				</div>

			</div>
			{{-- @else
			<div class="panel-body notice text-center">
				These test is already given by you!! <a href="{{route('home')}}">Click for Dashboard</a>
			</div>
			@endif  --}}
			<div class="clearfix"></div>
		</div>
	</div>
</div>

@endsection