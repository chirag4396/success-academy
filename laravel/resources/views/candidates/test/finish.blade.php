@extends('layouts.topbar')

@section('title')
Test completed
@endsection
@push('header')
<style>
canvas {
    -moz-user-select: none;
    -webkit-user-select: none;
    -ms-user-select: none;
    padding: 20px;
}
</style>
<script type="text/javascript" src = "https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.js"></script>
<script>
    history.pushState({ page: 1 }, "Title 1", "#no-back");
    window.onhashchange = function (event) {
        window.location.hash = "no-back";
    };
</script>
@endpush

@section('content')
<div class="container-fluid">
    <div class="row">                   

        <div class="col-md-3">

            <div class="card">            
                <div class="card-header" data-background-color="blue">
                    <h4>Results</h4>
                </div>
                <div class="card-content res">                            
                    <div class="col-md-12">                         
                        <h3>Attempted: {{ $attemptedQus .'/'. $totalQus }}</h3>
                        <h3>Skipped: {{ $skipped }}</h3>
                        <h3>True Attempts: {{ $attemptedTrue }}</h3>
                        <h3>Score: {{ $trueMark - ($falseMark * 0.33) .'/'. $testMarks }}</h3>
                        <h3>Percent: {{ round(($trueMark * 100 )/ $testMarks) }} %</h3>
                        <div class="text-center">
                            <a href="{{route('candidate.test.c_home')}}" class="btn btn-lg btn-warning">Go back to Dashboard</a>
                        </div>
                    </div>
                </div>

            </div>
            <div class="content-footer">
                You must accquired more than 60% for machine round.
            </div>                
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-header" data-background-color="blue">
                    <h4 class="text-center">Analysis</h4>
                </div>
                <div class="card-content sol">
                    <div id="canvas-holder" style="width:auto">
                        <canvas id="chart-area"></canvas>
                    </div>
                </div>
                <div class="card-header" data-background-color="blue">
                    <h4>Solutions<a href="{{ route('candidate.test.solution', ['id' => $test_id]) }}" class="btn btn-danger pull-right">Download</a></h4>
                </div>
                <div class="card-content sol">

                    @forelse ($qus as $k => $q)                    
                    <div class="qus">
                        <div>
                            <span class = "digit">{{ ++$k }}.</span>{!! $q->qus_detail !!}
                        </div>
                    </div>
                    <div class="clear-fix"></div>
                    <div class="ans">                        
                        <div>                       
                            {!! $q->qus_solution == '' ? '<p>No Solution Found</p>' : $q->qus_solution !!}                            
                        </div>
                    </div>
                    @empty
                    No Answers Found for these test
                    @endforelse
                </div>
            </div>
        </div>            
    </div>
</div>

@endsection

@push('footer')
<script>        
    $(document).ready(function(){
        $.ajax({
            url: "{{ route('get-chart') }}",
            type: "GET",
            success: function(data) {
                var labels = [];
                var score = [];
                var colors = [];

                for(var i in data) {
                    labels.push(data[i]['label']);
                    score.push(data[i]['score']);
                    colors.push('rgba(128, '+(i*20)+', '+(i*50)+', 0.75)');
                }

                window.myPolarArea = Chart.PolarArea($("#chart-area"), {
                    data: {
                        labels: labels,
                        datasets : [
                        {
                            backgroundColor: colors,
                            data: score
                        }
                        ]
                    },
                    options: {
                        responsive: true,
                        legend: {
                            position: 'right',
                        },
                        scale: {
                            ticks: {
                                beginAtZero: true
                            },
                            reverse: false
                        },
                        animation: {
                            animateRotate: false,
                            animateScale: true
                        }
                    }
                });
            },
            error: function(data) {
            }
        });
    });
</script>
@endpush