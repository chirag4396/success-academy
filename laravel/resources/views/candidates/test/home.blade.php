@extends('layouts.topbar')
@section('title')
Dashboard
@endsection
@push('header')    
@php
$ID = 'package';
$ID2 = 'test';
@endphp
@endpush
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="blue">
                    <h4 class="title" style="color: black;">Your Test's</h4>                    
                </div>
                <div class="card-content">
                    @if (count($packs) > 0)

                    <table class="table table-hover">
                        <thead>
                            <th>Sr. No.</th>
                            <th style="width: 100px;">Title</th>
                            <th>Start at</th>
                            <th>Ends at</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            @forelse ($packs as $k => $pack)
                            @php
                            $current = Carbon\Carbon::now('Asia/Kolkata');
                            
                            $active = Carbon\Carbon::parse($pack->package->pack_active_at,'Asia/Kolkata');
                            $deactive = Carbon\Carbon::parse($pack->package->pack_deactive_at,'Asia/Kolkata');
                            $d1 = $current->diffInDays($active,false);
                            $d2 = $current->diffInDays($deactive,false);                            

                            $day = $current->diffInDays($deactive);

                            @endphp
                            <tr>
                                <td>{{ ++$k }}</td>
                                <td>{{ $pack->package->pack_title}}</td>
                                <td>{{ $active->toDayDateTimeString() }}</td>
                                <td>{{ $deactive->toDayDateTimeString() }}</td>
                                <td> 
                                    @if($d1 <= 0)
                                        @if($d2 <= 0)
                                        <p class="tmsg-g"><b>Expired</b></p>
                                        @else
                                        
                                        <a href = "javascript:;" onclick = "fetch('{{ route('candidate.pack-can-test',['id' => $pack->package->pack_id]) }}');" title = "Click to view all tests of {{ $pack->package->pack_title }} Package" class="btn btn-primary" >{{ $pack->package->test->count() }}</a>
                                        @endif
                                    @endif
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td class="text-center" colspan="3">
                                    You Don't have any purchased package, please buy one now
                                </td> 
                            </tr>
                            @endforelse

                        </tbody>
                    </table>
                    @else
                    <div class="text-center">                    
                        You Don't have any purchased package, please buy one now
                    </div>
                    @endif    
                </div>
            </div>
        </div>
    </div>
    <!-- Test Modal -->
    <div id="{{ $ID2 }}Modal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id = "{{ $ID2 }}Title"></h4>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">

                        <table id = "{{ $ID2 }}Table" class="table table-hover">
                            <thead>
                                <th style="width: 100px;">Title</th>
                                <th>Duration</th>
                                <th>Total Questions</th>
                                <th>Disable At</th>
                                <th>Action</th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <!-- End Test Modal -->
</div>
@endsection
@push('footer')
<script type="text/javascript">

    function getUrl(id) {   
        return "{{ route('admin.test.create') }}?pack="+id;
        
    }   
    {{-- route = "{{ route('candidate.'.$ID.'.store') }}"; --}}

    function fetch(route) {
        $('#{{ $ID2 }}Table').CRUD({
            url : route,
            processResponse: function(data){                
                $('#{{ $ID2 }}Title').html(data.pack.pack_title+ ' Package Tests');
                $('#{{ $ID2 }}Modal').modal('toggle');
                $('#{{ $ID2 }}Table tbody').html(data.tr);          
            }
        }, 'get');
    }
</script>
@endpush