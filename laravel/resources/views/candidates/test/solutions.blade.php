<!Doctype html>
<html>
<head>
    <meta charset="utf-8" />    
    {{-- <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" /> --}}
    
    {{-- <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' /> --}}

    {{-- <style type="text/css">
        @font-face {
            font-family: SHREE-DEV7-0708E;
            src: url('{{ asset('fonts/S0708890.TTF') }}');
        }
    </style> --}}

    {{-- <link href="{{ asset('css/style.css') }}" rel="stylesheet"> --}}
    {{-- <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" /> --}}
    {{-- <link href="{{ asset('assets/css/material-dashboard.css?v=1.2.0') }}" rel="stylesheet" />     --}}
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-md-12">            
                <div class="panel panel-default">
                    <div class="panel-body">

                        @forelse ($qus as $k => $q)                    
                        <div class="qus">
                            <div>
                                <span>{{ ++$k }}.</span>{!! $q->qus_detail !!}
                            </div>
                        </div>
                        <div class="clear-fix"></div>
                        <div class="ans">                        
                            <div>                       
                                {!! $q->qus_solution == '' ? '<p>No Solution Found</p>' : $q->qus_solution !!}                            
                            </div>
                        </div>
                        @empty
                        No Answers Found for these test
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>