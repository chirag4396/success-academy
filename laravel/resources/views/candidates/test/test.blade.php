@extends('layouts.topbar')
@section('title')
{{ $test->test_title }}
@endsection
@push('header')
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('ckeditor/samples/js/sample.js') }}"></script>
<style type="text/css">
.cke_top, .cke_bottom{
	display:none!important;
}
.cke_editable {
	margin: 0!important;
	overflow-y: hidden!important;
}
.cke_contents{
	/*height: auto!important;*/
}
.cke_chrome{
	border: none!important;
}
.cke_reset{
	/*height: auto!important;*/
}
</style>

@endpush
@section('content')
<div class="container-fluid">
	<div class="row">                   

		<div class="col-md-9">
			<div class="card">
				<form id = "questionForm">
					@foreach($questions as $question)
					<input type="hidden" name="question" value="{{ $question->qus_id }}">
					<input type="hidden" name="start" value="{{ \Request::session()->get('start') }}">

					<div class="card-header" data-background-color="blue">
						<h4 class="test-title"><span class="qus-span">{{$question->qus_seq }}.</span>
							{{-- <textarea readonly id = "que"> --}}
								{!! $question->qus_detail !!}
							{{-- </textarea>							 --}}
							{{-- <script type="text/javascript">
								CKEDITOR.replace('que');
							</script> --}}
						</h4>
					</div>
					<div class="card-content" id = "options">
						@php
						$l = ['A','B','C','D'];
						$c = App\TestLog::where(['tlog_qus_id' => $question->qus_id, 'tlog_start_id' => \Request::session()->get('start'), 'tlog_test_id' => $test->test_id, 'tlog_user_id' => Auth::id()])->first();
						@endphp
						@forelse (App\Option::where('op_qus_id',$question->qus_id)->get() as $d => $option)

						<div class="option">
							<div class="option-l">
								@if ($c)
								<input type="radio" {{ $c->tlog_answer == $option->op_id ? 'checked' : '' }} name="option" value = "{{ $option->op_id }}" /> {{ $l[$d].'. '}}
								@else									
								<input type="radio" name="option" value = "{{ $option->op_id }}" /> {{ chr(65+$d).'. '}}
								@endif

							</div>
							<div class="option-a">
								{{-- <textarea {{ isset($type) ? '' :'readonly' }} id = "option-{{ $option->op_id }}"> --}}
									{!! $option->op_detail !!}
								{{-- </textarea> --}}
								{{-- {!! $option->op_detail !!} --}}
							</div>
						</div>

						@empty

						@endforelse

					</div>
					@endforeach
				</form>
			</div>
			<div class="card-footer" id = "test-footer">
				<div class="row">
					<div class="col-md-6">
						<a href="#" id = "finish" class="btn btn-danger">Finish</a>
					</div>
					{{-- <div class="col-md-10 text-center" id = "skiped">
						@if (count($skipped) > 0)
						@php						
						foreach ($skipped as $key => $value) {
							$skippedLink[] = $value->s_qus_id;
						}
						@endphp
						<span>Skiped</span>
						@forelse ($skipped as $skip)
						@php
						if($skip->s_page == 0){
							$link = Request::url();
						}else{

							$link = Request::url().'?page='.$skip->s_page;
						}
						@endphp
						<a href="{{ $link }}" class="btn btn-success">{{ $skip->s_page == 0 ? 1 : $skip->s_page }}</a>
						@empty
						
						@endforelse
						@endif
					</div> --}}

					<div class="col-md-6">
						<a class="btn btn-primary pull-right" id = "next">Next</a>
					</div>  
					{{--@endif--}}
					<div class="clear-fix"></div>                  
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="col-md-3">
			<div class="card">
				<div class="card-header" data-background-color="blue">
					Question Palettes
				</div>
				<div class="card-content" id = "palettes">
					@foreach($test->question()->get() as $que)					
					@php
					$class = 'blue';
					if(in_array($que->qus_id, $skippedLink)){
						$class = 'red';
					}
					if(in_array($que->qus_id, $attempted)){
						$class = 'green';
					}
					@endphp
					<div data-background-color="{{ $class }}" class="qus-count">
						<a href = "javascript:;" data-link="{{ Request::url().'?page='.$que->qus_id }}">
							<h4>{{ $que->qus_seq }}</h4>
						</a>
					</div>
					@endforeach					
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>

@endsection

@push('footer')

<script>
	var $min = {{ $min }};
	var $sec = {{ $sec }};
	var t = {{ $total }};
	var tlog = {{ $tlog }};
	@if ($skipped)        
	@php
	$sk = count($skipped);
	@endphp
	var skip = {{ $sk }};
	@if ($sk != 0)

	@if ($sk == 1)
	var snext = {{ $skipped->first()->s_page }};        
	@else
	var snext = {{ $skipped[1]->s_page }};
	@endif
	@endif
	@endif
	{{-- var $urll = "{{ Request::ip() != '127.0.0.1' ? 'http://mcq.myfabshop.com/student/' : 'http://sungare-erp/'}}candidate/test/"; --}}
	var $urll = "{{ route('candidate.test.send-test') }}"
	var $urlf = "{{ route('candidate.test.finish') }}"

	// $('#options textarea').each(function (k,v) {            
	// 	CKEDITOR.replace(this.id);	
	// });

		// $(document).ready(function(){
		// 	$('.ckeditor').each(function(){
		// 		if($(this).attr('height')) {
		// 			CKEDITOR.replace($(this).attr('name'),
		// 			{
		// 				height: $(this).attr('height')
		// 			});
		// 		}
		// 	});
		// });
	</script>

	<script type="text/javascript" src = "{{ asset('js/jquery.session.js') }}"></script>
	
	<script type="text/javascript" src = "{{ asset('js/jquery.countdownTimer.min.js') }}"></script>
	<script type="text/javascript" src = "{{ asset('js/test.js') }}"></script>
	@endpush