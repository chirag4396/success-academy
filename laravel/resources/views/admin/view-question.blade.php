@extends('layouts.sidebar')
@section('title')
{{ $subject->sub_title }} Questions
@endsection
@push('header')
<link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.dataTables.min.css') }}">
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('ckeditor/samples/js/sample.js') }}"></script>
<style type="text/css">
.cke_top, .cke_bottom{
	display:none!important;
}
.cke_editable {
	margin: 0!important;
	overflow-y: hidden!important;
}
.cke_chrome{
	border: none!important;
}
</style>
@endpush
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header" data-background-color="blue">
					<h4 class="title">
						All Questions from {{ $subject->sub_title.' of '.$test->test_title.' Test - '. $test->package->pack_title }}
						<a href="{{ route('admin.add-question', ['sub' => $subject->sub_id, 'test' => $test->test_id]) }}" class="btn btn-danger pull-right">Add more Questions</a>
					</h4>					
				</div>
				<div class="card-content table-responsive">

					<table id = "questions" class="table table-hover">
						<thead>
							<th style="width: 10px;">Sr. NO.</th>
							<th style="width: 200px;">Questions</th>
							<th style="width: 100px;">Correct Answer</th>
							<th style="width: 10px;">Marks</th>
							<th style="width: 120px;">Action</th>
						</thead>
						<tbody>
							{{--{{$test->question->first()}}--}}
							@forelse ($subject->questions()->get() as $k => $qus)
							<tr id = "tr-{{ ++$k }}">
								<td>{{ $k }}</td>
								<td><textarea readonly id = "que{{ $k }}">{!! $qus->qus_detail !!}</textarea></td>
								<td><textarea readonly id = "ans{{ $k }}">{!! App\Option::find($qus->qus_answer)->op_detail !!}</textarea></td>
								<td>{{ $qus->qus_score }}</td>
								<td>
									<a href="{{ url('/admin/question/'.$qus->qus_id) }}" class="btn btn-success">View</a> |
									<a href="{{ url('/admin/question/'.$qus->qus_id.'/edit') }}" class="btn btn-warning">Edit</a>
								</td>
							</tr>							
							@empty
							<tr>
								<td colspan="5" class="text-center">
									No Questions found, <a href="{{ route('admin.add-question', ['sub' => $subject->sub_id, 'test' => $test->test_id]) }}" class="btn btn-success">Add some NOW!!</a>
								</td>
							</tr>
							@endforelse
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@push('footer')
<script type="text/javascript">
	function assignCK() {
		$('#questions tr').each(function(k,v) {
			if(v.id){
				var c = v.id.split('-');
				CKEDITOR.replace( 'que'+c[1]);
				CKEDITOR.replace( 'ans'+c[1]);			
			}
		});
	}

	$(document).ready(function() {
		$('#questions').DataTable();
		assignCK();
		$('.paginate_button').on({
			'click' : function(){

				assignCK();
			}
		});
	});
</script>
<script type="text/javascript" src = "{{ asset('js/jquery.dataTables.min.js') }}"></script>
@endpush
