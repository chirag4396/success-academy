@extends('layouts.sidebar')
@section('title')
Add New Question
@endsection
@push('header')
@php
$ID = 'question';
@endphp
<script>
    ID = '{{ $ID }}';
</script>
{{-- <link rel="stylesheet" href="{{ asset('ckeditor/contents.css') }}"> --}}
{{-- <link rel="stylesheet" href="{{ asset('ckeditor/samples/css/samples.css') }}"> --}}
{{-- <link rel="stylesheet" href="{{ asset('ckeditor/samples/toolbarconfigurator/lib/codemirror/neo.css') }}"> --}}
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('ckeditor/samples/js/sample.js') }}"></script>
{{--<link rel="stylesheet" href="{{ asset('ckeditor/samples/toolbarconfigurator/lib/codemirror/neo.css') }}">--}}
<style type="text/css">
.control-label{
    font-size: 16px!important;
}    
</style>
@endpush
@php
$co = App\Question::where('qus_test_id', $test->test_id)->count();
@endphp
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="blue">
                    <h4 class="title">
                        Add Question - (<span id = "qusId">{{++$co}}</span>) <p class="pull-right">Test Package - {{ $test->package->pack_title }}</p>
                    </h4>                  
                </div>
                <div class="card-content">
                    <div class="col-md-12">

                        <form class="form-horizontal" id = "questionForm">
                            <input type="hidden" name="test_id" value="{{ $test->test_id }}">
                            <input type="hidden" name="subject" value="{{ $subject->sub_id }}">
                            <input type="hidden" name="seq" value="{{$co}}" id = "inQus">
                            <div class="form-group">
                                <label for="name" class="col-md-2 control-label">Mark</label>
                                <div class="col-md-9">
                                    <input type="text" name="score" class="form-control" value="{{ $test->test_score }}" {{ $test->test_pattern == 1 ? 'readonly' : ''}} >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name" class="col-md-2 control-label">Question</label>
                                <div class="col-md-9">
                                    <textarea name = "detail" id="question" rows = "10" class="form-control"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-2">
                                    <label class="col-md-12 control-label">Option A</label>                    
                                    <input type="radio" class="col-md-4  myradio pull-right" name = "answer" value="0">
                                </div>
                                <div class="col-md-9">
                                    <textarea name = "" id = "editor1" rows="5" class="form-control"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-2">

                                    <label class="col-md-12 control-label">Option B</label>
                                    <input class="col-md-4 myradio pull-right" type="radio" name = "answer" value="1">
                                </div>

                                <div class="col-md-9">                                

                                    <textarea name = "" id = "editor2" rows="5" class="form-control"></textarea>

                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-2">
                                    <label class="col-md-12 control-label">Option C</label>
                                    <input class="col-md-4 myradio pull-right" type="radio" name = "answer" value="2">
                                </div>

                                <div class="col-md-9">
                                    <textarea name = "" id = "editor3" rows="5" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-2">

                                    <label class="col-md-12 control-label">Option D</label>
                                    <input class="col-md-4 myradio pull-right" type="radio" name = "answer" value="3">

                                </div>

                                <div class="col-md-9">                                

                                    <textarea name = "" id = "editor4" rows="5" class="form-control" required></textarea>

                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name" class="col-md-2 control-label">Solution</label>

                                <div class="col-md-9">
                                    <textarea name = "solution" id="solution" rows = "10" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary">
                                        Add Question
                                    </button>
                                </div>
                            </div>
                        </form>

                        <div class="complete">
                            <a href="{{ route('admin.subjects', ['id' => $test->test_id]) }}" type="submit" class="btn btn-danger">Complete Test</a>
                        </div>

                        <div class="alert alert-success text-center hidden" id = "questionMessage"></div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('footer')
<script>
    CKEDITOR.replace( 'question' );
    CKEDITOR.replace( 'solution' );
    CKEDITOR.replace( 'editor1' );
    CKEDITOR.replace( 'editor2' );
    CKEDITOR.replace( 'editor3' );
    CKEDITOR.replace( 'editor4' );

    var $path = "{{ route('question.store') }}";

    $('#{{ $ID }}Form').CRUD({
        url : "{{ route('admin.'.$ID.'.store') }}",
        validation : false,
        extraVariables : function(){
            var $op = [];
            for(var i=1;i<5;i++){
                $op.push(CKEDITOR.instances['editor'+i].getData());
            }
            return {
                detail : CKEDITOR.instances['question'].getData(),
                solution : CKEDITOR.instances['solution'].getData(),
                'option' : $op
            }
        },
        processResponse: function(data) {            
            if(data.msg == 'success') {
                CKEDITOR.instances['question'].setData('');
                CKEDITOR.instances['solution'].setData('');            
                for(var i=1;i<5;i++) {
                    CKEDITOR.instances['editor'+i].setData('');
                }
                var q = parseInt($('#qusId').html()) + 1;
                $('#qusId').html(q);
                $('#inQus').val(q);
            }
        }
    });
</script>
{{-- <script type="text/javascript" src = "{{ asset('js/questions.js') }}"></script> --}}
@endpush