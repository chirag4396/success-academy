@extends('layouts.sidebar')
@section('title')
All Results
@endsection
@push('header')
<link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.dataTables.min.css') }}">
@endpush
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header" data-background-color="blue">
					<h4 class="title">All Tests <a href = "{{ url('/admin') }}" class="btn btn-danger pull-right">Back</a></h4>                  
				</div>
				<div class="card-content">
					<table id = "table" class="table table-hover">
						<thead>
							<th style="width: 100px;">Title</th>
							<th>Package</th>
							<th>Duration</th>
							<th>Total Questions</th>
							<th>Created At</th>
							<th>Action</th>
						</thead>
						<tbody>
							@forelse ($tests as $test)				
							<tr>
								<td>{{ $test->test_title }}</td>
								<td>{{ $test->package->pack_title }}</td>
								<td>{{ $test->test_hours }} mins</td>
								<td>{{ \App\Question::where('qus_test_id',$test->test_id)->get()->count() }}</td>
								<td>{{ \Carbon\Carbon::parse($test->test_taken_at)->toFormattedDateString() }}</td>
								<td>
									<a href="{{ url('/admin/all-questions/'.$test->test_id) }}" class="btn btn-success">View Questions</a> | 
									{{-- <a href="#" class="btn btn-success">View Questions</a> |  --}}
									<a href="{{ url('/admin/test-result/'.$test->test_id) }}" class="btn btn-danger">See Result</a>
								</td>
							</tr>
							@empty
							{{-- empty expr --}}
							@endforelse
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@push('footer')
<script type="text/javascript">
	$(document).ready(function() {
		$('#table').DataTable();
	});
</script>
<script type="text/javascript" src = "{{ asset('js/jquery.dataTables.min.js') }}"></script>
@endpush