@extends('layouts.sidebar')
@section('title')
All Candidates
@endsection
@push('header')
<link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.dataTables.min.css') }}">
<script type="text/javascript">
	function changeStatus(id) {		
		$(id).html('Loading..');		
	}
</script>
@endpush
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header" data-background-color="blue">
					<h4 class="title">All Candidates</h4>					
				</div>
				<div class="card-content table-responsive">
					<table id = "table" class="table table-hover">
						<thead>
							<th>Sr. No.</th>
							<th>Name</th>
							<th>Mobile</th>
							<th>Package</th>
							<th>Date</th>
							{{-- <th>Status</th> --}}
						</thead>
						<tbody>
							@forelse ($purchases as $k => $pur)
							<tr>
								<td>{{ ++$k }}</td>
								<td>{{ $pur->user->name }}</td>
								<td>{{ $pur->user->mobile }}</td>
								<td>{{ $pur->package->pack_title }}</td>
								<td>{{ \Carbon\Carbon::parse($pur->pur_created_at)->toFormattedDateString() }}</td>
								{{-- <td>
									<button id = "s-{{ $pur->pur_id }}" onclick = "changeStatus(this);" class="btn btn-{{ $pur->pur_status ? 'success' : 'danger' }}">{{ $pur->pur_status ? 'Enable' : 'Disable' }}</button>
								</td> --}}
							</tr>
							@empty
							<tr>
								<td colspan="5" class="text-center">
									No Purchases yet!!
								</td>
							</tr>
							@endforelse
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@push('footer')
<script type="text/javascript">
	$(document).ready(function() {
		$('#table').DataTable();
	});
</script>
<script type="text/javascript" src = "{{ asset('js/jquery.dataTables.min.js') }}"></script>
@endpush