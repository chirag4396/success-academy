@extends('layouts.sidebar')
@section('title')
	Dashboard
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header" data-background-color="orange">
                    <i class="material-icons">content_copy</i>
                </div>
                <div class="card-content">
                    <p class="category">Pacakges</p>
                    <h3 class="title">
                        {{ \App\PackageDetail::get()->count() }}
                    </h3>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        <i class="material-icons">create</i>
                        <a href="{{ route('admin.package.create') }}">Add More</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header" data-background-color="blue">
                    <i class="material-icons">store</i>
                </div>
                <div class="card-content">
                    <p class="category">Test</p>
                    <h3 class="title">
                        {{ \App\Test::get()->count() }}                        
                    </h3>

                </div>
                <div class="card-footer">
                    <div class="stats">
                        <i class="material-icons">create</i>
                        <a href="{{ route('admin.test.create') }}">Add More</a>
                        {{-- <i class="material-icons">date_range</i> Last 24 Hours --}}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header" data-background-color="red">
                    <i class="material-icons">info_outline</i>
                </div>
                <div class="card-content">
                    <p class="category">Questions</p>
                    <h3 class="title">
                        {{ \App\Question::get()->count() }}                        
                    </h3>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        <i class="material-icons">create</i>
                        <a href="{{ route('admin.question.create') }}">Add More</a>
                        {{-- <i class="material-icons">local_offer</i> Tracked from Github --}}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header" data-background-color="blue">
                    <i class="fa fa-twitter"></i>
                </div>
                <div class="card-content">
                    <p class="category">Followers</p>
                    <h3 class="title">+245</h3>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        <i class="material-icons">update</i> Just Updated
                    </div>
                </div>
            </div>
        </div>
    </div>    
</div>
{{-- <div class="col-md-10 zero-padding">
	<h3>Dashboard</h3>
	<div class="profile-content zero-padding">
		<div class="col-md-4 zero-padding">
			<a href="{{ url('admin/test/create') }}">
				<div class="dashboard-div">
					<p>
						Create Test
					</p>
					<div class="icon pull-right">
						<i class="fa fa-eye"></i>
					</div>
				</div>
			</a>
		</div>
		<div class="col-md-4 zero-padding">
			<a href="{{ url('/admin/results') }}">
				<div class="dashboard-div">
					<p>
						Results
					</p>
					<div class="icon pull-right">
						<i class="fa fa-eye"></i>
					</div>
				</div>
			</a>
		</div>
		<div class="col-md-4 zero-padding">
			<a href="{{ url('/admin/all-test') }}">						
				<div class="dashboard-div">
					<p>
						View All Test
					</p>
					<div class="icon pull-right">
						<i class="fa fa-eye"></i>
					</div>
				</div>
			</a>
		</div>
	</div>
</div> --}}	
@endsection

@push('footer')
    <script type="text/javascript">
        var current = '{{ \Request::fullUrl() }}';
    </script>
@endpush