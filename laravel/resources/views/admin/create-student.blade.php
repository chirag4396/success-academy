@extends('layouts.sidebar')
@section('title')
Create New Student
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="blue">
                    <h4 class="title">Create Student</h4>                  
                </div>
                <div class="card-content">
                    <div class="col-md-offset-2 col-md-8">

                        <form id = "candidateForm">
                            <div class="form-group label-floating ">
                                <label for="name" class="control-label">Name</label>

                             
                                    <input id="name" type="text" class="form-control" name="name" required autofocus>
                             
                            </div>

                            <div class="form-group label-floating ">
                                <label for="email" class="control-label">E-Mail Address</label>

                                
                                    <input id="email" type="email" class="form-control" name="email" required>
                                
                            </div>

                            <div class="form-group label-floating ">
                                <label for="mobile" class="control-label">Mobile</label>

                                
                                    <input id="mobile" type="text" class="form-control" name="mobile" required>

                                
                            </div>

                            <div class="form-group label-floating ">
                                <label for="alt-mobile" class="control-label">Parent Mobile</label>

                                
                                    <input id="alt-mobile" type="text" class="form-control" name="p_mobile" required>
                                
                            </div>

                            <div class="form-group label-floating ">
                                <label for="batch" class="control-label">Batch</label>
                                
                                    <select class="form-control" id = "batchSelection">
                                        <option>-Batch-</option>
                                        @forelse (App\Batch::get() as $batch)
                                        <option value="{{ $batch->batch_id }}">{{ $batch->batch_name }}</option>
                                        @empty

                                        @endforelse
                                        <option value = "other">Others</option>
                                    </select>
                                
                            </div>
                            <input type="hidden" name="batch" id = "batch">
                            <div class="form-group label-floating  hidden" id="batchOther">
                                
                                    <div class="input-group">
                                        <input type="text" class="form-control" id = "batchName">
                                        <div class="input-group-btn">
                                            <button id = "addBatch" class="btn btn-default" type="submit">
                                                Add
                                            </button>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="alert-warning batch-msg hidden" id = "batchMsg"></div>
                                
                            </div>

                            <input id="password" type="hidden" class="form-control" name="password" value = "{{ 'Can@'.(App\User::max('id')+1) }}">

                            <input type="hidden" name="type" value="102">
                            <div class="form-group label-floating ">
                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary">
                                        Register
                                    </button>
                                </div>
                            </div>
                        </form>
                        <div class="alert alert-success text-center hidden" id = "candidateMessage"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('footer')

<script type="text/javascript" src = "{{ asset('js/candidate.js') }}"></script>
@endpush