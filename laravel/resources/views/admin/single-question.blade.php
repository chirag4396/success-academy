@extends('layouts.sidebar')
@section('title')
Question
@endsection
@push('header')
{{--<link rel="stylesheet" href="{{ asset('ckeditor/contents.css') }}">--}}
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('ckeditor/samples/js/sample.js') }}"></script>
{{--<link rel="stylesheet" href="{{ asset('ckeditor/samples/toolbarconfigurator/lib/codemirror/neo.css') }}">--}}
<style type="text/css">
    .control-label{
        font-size: 16px!important;
    }
</style>
@if (!isset($type))

<style type="text/css">
.cke_top, .cke_bottom{
    display:none!important;
}
</style>
@endif
@endpush
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="blue">
                    <h4 class="title">
                        @if (isset($type))
                        Edit Question of {{ $question->test->package->pack_title }} <a href = "{{ url('/admin/all-questions/'.$question->test->test_id) }}" class="btn btn-danger pull-right">Back</a>
                        @else
                        Question of {{ $question->test->test_title.' - '.$question->test->package->pack_title }}<a href = "{{ url('/admin/question/'.$question->qus_id.'/edit') }}" class="btn btn-danger pull-right">Edit</a>
                        @endif    
                    </h4>                  
                </div>
                <div class="card-content">
                    <div class="col-md-12">


                        <div class="panel-body">
                            <form id = "editForm">
                                <input type="hidden" name="qus_id" value="{{ $question->qus_id }}">
                                <div class="form-group label-floating">
                                    <label for="name" class="control-label">Mark</label>
                                    <input type="text" name="score" class="form-control" value="{{ $question->qus_score }}" {{ isset($type) ? '' :'readonly' }}>
                                </div>
                                <div class="form-group label-floating">
                                    <label for="name" class="big-label">Qustion</label>
                                    
                                    <textarea name = "detail" id = "question" rows = "10" class="form-control" {{ isset($type) ? '' :'readonly' }}>{{ $question->qus_detail }}</textarea>
                                    
                                </div>
                                <div id = "options">
                                    @forelse ($question->option as $k => $option)
                                    <div class="form-group label-floating">
                                        <label class="big-label">Option {{ chr(65+$k) }}</label>

                                        <div class="input-group">
                                            <textarea {{ isset($type) ? '' :'readonly' }} id = "option-{{ $option->op_id }}" name = "option[{{ $option->op_id }}]" rows="5" class="form-control" >{{ $option->op_detail }}</textarea>
                                            <div class="input-group-btn">
                                                <input type="radio" name = "answer" value="{{ $option->op_id }}" {{ $question->qus_answer == $option->op_id ? 'checked' : ''}}>
                                            </div>
                                        </div>        

                                    </div>                
                                    @empty
                                    @endforelse
                                </div>
                                <div class="form-group label-floating">
                                    <label for="name" class="big-label">Solution</label>

                                    
                                    <textarea name = "solution" id="solution" rows = "10" class="form-control">{{ $question->qus_solution }}</textarea>
                                    
                                </div>
                                @if (isset($type))    
                                <div class="form-group label-floating">
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-primary">
                                            Edit
                                        </button>
                                    </div>
                                </div>
                                @endif
                            </form>
                            <div class="alert alert-success text-center hidden" id = "editMessage"></div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('footer')
<script type="text/javascript" src = "{{ asset('js/questions.js') }}"></script>
<script>
    CKEDITOR.replace( 'question');
    CKEDITOR.replace( 'solution');

    $('#options textarea').each(function (k,v) {            
        CKEDITOR.replace(this.id);
    }); 
</script>
@endpush