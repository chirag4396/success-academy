<form id = "{{ $ID }}Form">    
    <div class="form-group label-floating">
        <div id = "description"></div>
    </div>
    <div class="clearfix"></div>
    <div class="form-group label-floating">
        <div class="text-center">
            <button type="submit" class="btn btn-primary">
                Create
            </button>
        </div>
    </div>
</form>