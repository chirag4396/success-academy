<form id = "{{ $ID }}Form">    
    <div class="form-group label-floating">
        <label for="name" class="control-label">Type</label>
        <select class="form-control" name="type" id = "packType">
            <option>--Select--</option>
            @forelse (\App\PackageType::get() as $pt)
            <option value="{{ $pt->pt_id }}">{{ $pt->pt_title }}</option>
            @empty
            @endforelse
        </select>
    </div>
    <div class="form-group label-floating">
        <label for="name" class="control-label">Title</label>
        <input type="text" class="form-control" name="title" required>
    </div>    
    <div class="form-group label-floating">
        <label for="name" class="control-label">Video URL</label>
        <input type="text" class="form-control" name="video" required>
    </div>
    <div class="form-group label-floating">
        <div id = "description"></div>
    </div>
    <div class="clearfix"></div>
    <div class="form-group label-floating">
        <div class="text-center">
            <button type="submit" class="btn btn-primary">
                Create
            </button>
        </div>
    </div>
</form>