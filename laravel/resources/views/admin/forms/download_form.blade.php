<form id = "{{ $ID }}Form">
    <div class="form-group label-floating">
        <label for="name" class="control-label">Type</label>
        <select class="form-control" name="type">
            <option>--Select--</option>
            @forelse (\App\PackageType::get() as $pt)
            <option value="{{ $pt->pt_id }}">{{ $pt->pt_title }}</option>
            @empty
            @endforelse
        </select>
    </div>
    <div class="form-group label-floating">
        <label for="name" class="control-label">Title/Name</label>
        <input type="text" class="form-control" name="title" required>
    </div>    
    <div class="form-group label-floating">
        <div class="text-center">
            <input type = "file" id ="pdf" accept="application/pdf" class="hidden form-control1" name="pdf">
            <label class="btn btn-success" for = "pdf">Choose PDF</label>
        </div>
    </div>  
    <div class="clearfix"></div>
    <div class="form-group label-floating">
        <div class="text-center">
            <button type="submit" class="btn btn-primary">
                Create
            </button>
        </div>
    </div>
</form>

@push('footer')
<script type="text/javascript">
    $('#{{ $ID }}Form').CRUD({
        url : "{{ route('admin.'.$ID.'.store') }}",
        processResponse : function(data){
            console.log(data);
        },
        validation : false
    });
</script>
@endpush