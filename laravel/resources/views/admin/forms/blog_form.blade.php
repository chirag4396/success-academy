@push('header')
<link rel="stylesheet" type="text/css" href="{{ asset('css/quill.snow.css') }}">
@endpush

<form id = "{{ $ID }}Form">        
    <div class="form-group label-floating">
        <div class="text-center">
            <img src="{{ asset('images/no-image.png') }}" id = "imgPreview">
            <div class="clearfix"></div>
            <input type = "file" id ="img" accept="image/png, image/jpeg, image/jpg" class="hidden form-control1" name="img">
            <label class="btn btn-success" for = "img">Choose Image</label>
        </div>
    </div>
    <div class="form-group label-floating">
        <label for="name" class="control-label">Title</label>
        <input type="text" class="form-control" name="title" required>
    </div>  
    <div class="form-group label-floating">
        <div id = "description"></div>
    </div>
    <div class="clearfix"></div>
    <div class="form-group label-floating">
        <div class="text-center">
            <button type="submit" class="btn btn-primary">
                Create
            </button>
        </div>
    </div>
</form>

@push('footer')
<script type="text/javascript" src="{{ asset('js/quill.js') }}"></script>
<script type="text/javascript">
    var options = {      
        placeholder: 'Blog Description...',    
        theme: 'snow'
    };
    var description = new Quill('#description', options);

    route = "{{ route('admin.'.$ID.'.store') }}";

    $('#{{ $ID }}Form').CRUD({
        url : route,
        extraVariables : function(){
            return {'description' : document.querySelector(".ql-editor").innerHTML};
        },
        processResponse : function(data){
            
        },
        validation : false
    });
    imageUpload('img');
</script>
@endpush