<form id = "{{ $ID }}Form">
    <div class="form-group label-floating">
        <label for="name" class="control-label">Type</label>
        <select class="form-control" name="type" id = "packType">
            <option>--Select--</option>
            @forelse (\App\PackageType::get() as $pt)
            <option value="{{ $pt->pt_id }}">{{ $pt->pt_title }}</option>
            @empty
            @endforelse
        </select>
    </div>
    {{-- <div class="form-group col-md-12 hidden" id = "subCourse">
        <label class="control-label">Course Sub Type:</label>
        <select class="form-control1 subCourseDrop" name="ud[course_sub_type]" id = "course" data-validate = "select">
        </select>
    </div> --}}
    <div class="form-group label-floating hidden" id = "achType">
        <label for="name" class="control-label">Achievement Type</label>
        <select class="form-control" name="a_type" disabled>
            {{-- <option>--Select--</option>
            @forelse (\App\AchievementType::get() as $at)
            <option value="{{ $at->at_id }}">{{ $at->at_title }}</option>
            @empty
            @endforelse --}}
        </select>
    </div>
    <div class="form-group label-floating">
        <label for="name" class="control-label">Title/Name</label>
        <input type="text" class="form-control" name="name" required>
    </div>
    <div class="form-group label-floating">
        <label for="name" class="control-label">Sub Title</label>
        <input type="text" name = "sub_name" class="form-control">
    </div>
    <div class="form-group label-floating">
        <div id = "description"></div>
    </div>
    <div class="form-group label-floating">
        <div class="text-center">
            <img src="{{ asset('images/no-image.png') }}" id = "imgPreview">
            <div class="clearfix"></div>
            <input type = "file" id ="img" accept="image/png, image/jpeg, image/jpg" class="hidden form-control1" name="img">
            <label class="btn btn-success" for = "img">Choose Image</label>
        </div>
    </div>  

    <div class="clearfix"></div>
    <div class="form-group label-floating">
        <div class="text-center">
            <button type="submit" class="btn btn-primary">
                Create
            </button>
        </div>
    </div>
</form>