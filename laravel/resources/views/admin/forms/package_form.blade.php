@push('header')
    <style type="text/css">
    #description{
        height: 200px;
    }
    </style>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/quill.snow.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-datetimepicker.min.css') }}">
@endpush
<form id = "{{ $ID }}Form">
    @isset ($pack)
        <input type="hidden" name="id" value="{{ $pack->pack_id }}">
    @endisset
    <div class="form-group label-floating">
        <label for="name" class="control-label">Type</label>
        <select class="form-control" name="type">
            <option>--Select--</option>
            @forelse (\App\PackageType::get() as $pt)
                @php                
                    $s = isset($pack) ? ($pack->pack_type == $pt->pt_id ? 'selected' : '' ): '';
                @endphp
                <option value="{{ $pt->pt_id }}" {{ $s }}>{{ $pt->pt_title }}</option>
            @empty
            @endforelse
        </select>
    </div>
    <div class="form-group label-floating">
        <label for="name" class="control-label">Title/Name</label>
        <input type="text" class="form-control" name="title" required value="{{ $pack->pack_title or ''}}">
    </div>                            
    <div class="form-group label-floating">
        <label for="name" class="control-label">Price</label>
        <input type="text" name = "price" class="form-control" value="{{ $pack->pack_price or ''}}">
    </div>
    <div class="form-group label-floating">
        <label for="name" class="control-label">Discount (Optional)</label>
        <input type="text" name = "dis_price" class="form-control" value="{{ $pack->pack_dis_price or ''}}">
    </div>
    <div class="form-group label-floating">
        <div id = "description">{!! $pack->pack_description or '' !!}</div>
    </div>                            
    <div class="form-group label-floating">
        <label for="name" class="control-label">Enable at </label>
        <div class="input-group date form_datetime" id = "enableSelect" data-date="2017-01-1T05:25:07Z" data-date-format="dd MM yyyy - HH:ii p" data-link-field="enable">
            <input class="form-control" size="16" type="text" value="{{ isset($pack) ? \Carbon\Carbon::parse($pack->pack_enable) : '' }}" readonly>
            <span class="input-group-addon">
                <span class="fa fa-calendar">
                </span>
            </span>
        </div>
        <input type="hidden" name="active_at" id="enable" value="{{ isset($pack) ? \Carbon\Carbon::parse($pack->pack_enable) : '' }}" />
    </div>
    <div class="form-group label-floating">
        <label for="name" class="control-label">Disable at </label>
        <div class="input-group date form_datetime" id = "disableSelect" data-date-format="dd MM yyyy - HH:ii p" data-link-field="disable">
            <input class="form-control" size="16" type="text" value="{{ isset($pack) ? \Carbon\Carbon::parse($pack->pack_disable) : '' }}" readonly>
            <span class="input-group-addon">
                <span class="fa fa-calendar">
                </span>
            </span>
        </div>
        <input type="hidden" name="deactive_at" id="disable" value="{{ isset($pack) ? \Carbon\Carbon::parse($pack->pack_disable) : '' }}" />
    </div>
    <div class="form-group label-floating">
        <div class="text-center">
            <button type="submit" class="btn btn-primary">
                {{ isset($pack) ? 'Update' : 'Create' }}
            </button>
        </div>
    </div>
</form>

@push('footer')
<script type="text/javascript" src="{{ asset('js/quill.js') }}"></script>
<script type="text/javascript" src = "{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
<script type="text/javascript">
    var options = {      
        placeholder: 'Add Package description...',    
        theme: 'snow'
    };
    var description = new Quill('#description', options);

    route = "{{ route('admin.'.$ID.'.index') }}";

    $('#{{ $ID }}Form').CRUD({
        url : route,
        @isset ($pack)
            type : 2,
        @endisset
        extraVariables : function(){
            return {'description' : document.querySelector(".ql-editor").innerHTML};
        },
        processResponse : function(data){
            if(data.msg == 'success'){
                $('.ql-editor').html();
            }
        },
        validation : false
    });

    var d = '{{ Carbon\Carbon::now('Asia/Kolkata') }}';
    $('#enableSelect').datetimepicker({
        //language:  'fr',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        forceParse: 0,
        showMeridian: 1,
        startDate : d
    });
    $('#disableSelect').datetimepicker({
        //language:  'fr',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        forceParse: 0,
        showMeridian: 1,
        startDate : d
    });
    
</script>
@endpush