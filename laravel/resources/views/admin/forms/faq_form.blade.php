@push('header')
<link rel="stylesheet" type="text/css" href="{{ asset('css/quill.snow.css') }}">
<style type="text/css">
#qus, #ans {
    height: 200px;
}
</style>
@endpush

<form id = "{{ $ID }}Form">
    <div class="form-group label-floating">
        <label for="name" class="control-label">Type</label>
        <select class="form-control" name="type">
            <option>--Select--</option>
            @forelse (\App\PackageType::get() as $pt)
            <option value="{{ $pt->pt_id }}">{{ $pt->pt_title }}</option>
            @empty
            @endforelse
        </select>
    </div>
    <div class="form-group label-floating">
        <div id = "qus"></div>
    </div>
    <div class="form-group label-floating">
        <div id = "ans"></div>
    </div>
    <div class="clearfix"></div>
    <div class="form-group label-floating">
        <div class="text-center">
            <button type="submit" class="btn btn-primary">
                Create
            </button>
        </div>
    </div>
</form>

@push('footer')
<script type="text/javascript" src="{{ asset('js/quill.js') }}"></script>
<script type="text/javascript">

    var qus = new Quill('#qus', {      
        placeholder: 'Question...',    
        theme: 'snow'
    });

    var ans = new Quill('#ans', {
        placeholder: 'Answer...',    
        theme: 'snow'
    });

    $('#{{ $ID }}Form').CRUD({
        url : "{{ route('admin.'.$ID.'.store') }}",
        extraVariables : function(){
            return {
                'qus' : document.querySelector("#qus .ql-editor").innerHTML,
                'ans' : document.querySelector("#ans .ql-editor").innerHTML
            };
        },
        processResponse : function(data){
            console.log(data);
        },
        validation : false
    });
</script>
@endpush