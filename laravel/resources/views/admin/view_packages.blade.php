@extends('layouts.sidebar')
@section('title')
All Packages
@endsection
@push('header')
@php
$ID = 'package';
$ID2 = 'test';
@endphp
<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-datetimepicker.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.dataTables.min.css') }}">
<link href="{{ asset('admin-assets/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('admin-assets/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('admin-assets/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('admin-assets/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('admin-assets/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') }}" rel="stylesheet">
@endpush
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header" data-background-color="blue">
					<h4 class="title">All Packages<a href="{{ route('admin.package.create') }}" class="btn btn-danger pull-right">Add more Package</a></h4>					
				</div>
				<div class="card-content table-responsive">

					<table id = "{{ $ID }}Table" class="table table-hover">
						<thead>
							<th style="width: 100px;">Title</th>
							<th>Type</th>
							<th>Price</th>
							<th>Active On</th>
							<th>Deactive On</th>
							<th>Created</th>
							<th>Total Test</th>
							<th>Action</th>
						</thead>
						<tbody>
							@forelse ($packs as $pack)				
							<tr>
								<td>{{ $pack->pack_title }}</td>
								<td>{{ $pack->type->pt_title }}</td>
								<td align="right">{{ '&#8377;'.number_format($pack->pack_price) }}</td>
								<td>{{ Carbon\Carbon::parse($pack->pack_active_at)->format('jS M, Y g:i a') }}</td>
								<td>{{ Carbon\Carbon::parse($pack->pack_deactive_at)->format('jS M, Y g:i a') }}</td>
								<td>{{ Carbon\Carbon::parse($pack->pack_created_at)->format('jS M, Y g:i a') }}</td>
								<td><a href = "#" onclick = "fetch('{{ route('admin.pack-test',['id' => $pack->pack_id]) }}');" title = "Click to view all tests of {{ $pack->pack_title }} Package" class="btn btn-primary" >{{ $pack->test->count() }}</a></td>
								<td>
									<a href="{{ route('admin.package.edit', ['id' => $pack->pack_id]) }}" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit</a>
									<a onclick = "$(this).CRUD({id : '{{ $ID }}', url : '{{ route('admin.package.index') }}', pId : '{{ $pack->pack_id }}', name : '{{ $pack->pack_title }}'}, 'deleteForm');" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>									
								</td>
							</tr>
							@empty
							<tr>
								<td colspan="8" class="text-center">
									No pack found, <a href="{{ route('admin.package.create') }}" class="btn btn-success">Create one NOW!!</a>
								</td>
							</tr>
							@endforelse
						</tbody>
					</table>
				</div>

				<!-- Test Modal -->
				<div id="{{ $ID2 }}Modal" class="modal fade" role="dialog">
					<div class="modal-dialog modal-lg">
						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title" id = "{{ $ID2 }}Title"></h4>
							</div>
							<div class="modal-body">
								<div class="table-responsive">

									<table id = "{{ $ID2 }}Table" class="table table-hover">
										<thead>
											<th style="width: 100px;">Title</th>
											<th>Duration</th>
											<th>Total Questions</th>
											<th>Created At</th>
											<th>Action</th>
										</thead>
										<tbody>
											
										</tbody>
									</table>

								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="{{ $ID }}DModal" tabindex="-1" role="dialog" aria-labelledby="Modal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header text-center">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Are You Sure you want to delete <span id = "delName"></span> Pacakge ? It will delete all test and data realted to it</h4>
				<br>
				<div>
					<form id = "{{ $ID }}DForm">             
						<button type = "submit" class="btn btn-danger">Yes</button>
						<button type = "button" class="btn btn-success" data-dismiss="modal">No</button>
					</form>
				</div>            
			</div>
		</div>
	</div>
</div>
@endsection
@push('footer')
<script src="{{ asset('admin-assets/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
<script type="text/javascript" src = "{{ asset('admin-assets/dataTables.buttons.min/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('admin-assets/dataTables.buttons.min/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-scroller/js/dataTables.scroller.min.js') }}"></script>
<script src="{{ asset('admin-assets/jszip/dist/jszip.min.js') }}"></script>
<script src="{{ asset('admin-assets/pdfmake/build/pdfmake.min.js') }}"></script>
<script src="{{ asset('admin-assets/pdfmake/build/vfs_fonts.js') }}"></script>
<script type="text/javascript" src = "{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>

<script type="text/javascript">	
	Table.init('{{ $ID }}');	
	Table.init('{{ $ID2 }}');
	function getUrl(id) {	
		return "{{ route('admin.test.create') }}?pack="+id;
	
	}	
	function fetch(route) {
		$('#{{ $ID2 }}Table').CRUD({
			url : route,
			processResponse: function(data){

				$('#{{ $ID2 }}Title').html(data.pack.pack_title+ ' Package Tests<a href="'+getUrl(data.pack.pack_id)+'" class="btn btn-danger pull-right">Add more Test</a>');
				$('#{{ $ID2 }}Modal').modal('toggle');
				$('#{{ $ID2 }}Table tbody').html(data.tr);			
			}
		}, 'get');
	}

	route = "{{ route('admin.'.$ID.'.store') }}";

	$('#{{ $ID }}Form').CRUD({
	    url : route,
	    validation : false
	});
	var d = '{{ Carbon\Carbon::now('Asia/Kolkata') }}';
	$('#enableSelect').datetimepicker({
	    //language:  'fr',
	    weekStart: 1,
	    todayBtn:  1,
	    autoclose: 1,
	    todayHighlight: 1,
	    startView: 2,
	    forceParse: 0,
	    showMeridian: 1,
	    startDate : d
	});
	$('#disableSelect').datetimepicker({
	    //language:  'fr',
	    weekStart: 1,
	    todayBtn:  1,
	    autoclose: 1,
	    todayHighlight: 1,
	    startView: 2,
	    forceParse: 0,
	    showMeridian: 1,
	    startDate : d
	});
</script>
@endpush