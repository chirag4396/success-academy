            </div>
        </div>
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/quill.js') }}"></script>
    <script src="{{ asset('js/common.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/crud.js') }}"></script>

    {{-- <script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script> --}}
    <script type="text/javascript">
        // $.ajaxSetup({
        //     headers: {
        //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //     }
        // });
        type = {{Auth::guest() ? '0' : Auth::user()->type}}

        var $url = "{{ Request::root()}}/admin";
        
    </script>
    @stack('footer')

    @stack('last')
</body>
</html>
