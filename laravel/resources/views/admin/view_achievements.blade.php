@extends('layouts.sidebar')
@section('title')
All Achievements
@endsection
@push('header')
<link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.dataTables.min.css') }}">
<link href="{{ asset('admin-assets/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('admin-assets/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('admin-assets/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('admin-assets/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('admin-assets/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') }}" rel="stylesheet">
@php
$ID = 'achievement';
@endphp
<script>
	var ID = '{{ $ID }}';
</script>
@endpush
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header" data-background-color="blue">
					<h4 class="title">All Achievements</h4>					
				</div>
				<div class="card-content table-responsive">
					<table id = "{{ $ID }}Table" class="table table-hover">
						<thead>
							<th>Sr. No.</th>
							<th>Image</th>
							<th>Name</th>
							<th>Type</th>
							<th>Achievement Type</th>
							<th>Action</th>
						</thead>
						<tbody>
							@forelse ($achievements as $k => $ach)
							<tr>
								<td>{{ ++$k }}</td>
								<td><img src="{{ asset($ach->ach_img) }}" width="80" class="table-img"></td>
								<td>{{ $ach->ach_name }}</td>
								<td>{{ $ach->type->pt_title }}</td>
								<td>{{ $ach->aType->at_title }}</td>
								<td>
									<a href="javascript:;" class="btn btn-info btn-xs" onclick = "$(this).CRUD({url : '{{ route('admin.'.$ID.'.index') }}',fetchId:'{{ $ach->ach_id }}',id : '{{ $ID }}'}, 'edit')"><i class="fa fa-pencil"></i> Edit</a>
									<a onclick = "CRUD.delete('{{ $ID }}',{{ $ach->ach_id }},'{{ $ach->ach_name }}');" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>									
								</td>
							</tr>
							@empty
							<tr>
								<td colspan = "6" class="text-center">No {{ $ID }} Found <a class = "btn btn-primary" href="{{ route('admin.'.$ID.'.create') }}">Add some now</a></td>
							</tr>
							@endforelse
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="{{ $ID }}Modal" tabindex="-1" role="dialog" aria-labelledby="Modal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="{{ $ID }}ModalLabel">Edit {{ $ID }}</h4>
			</div>
			<div class="modal-body">
				@include('admin.forms.achievement_form')
			</div>          
		</div>
	</div>
</div>

<div class="modal fade" id="{{ $ID }}DModal" tabindex="-1" role="dialog" aria-labelledby="Modal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header text-center">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Are You Sure you want to delete <span id = "delName"></span> Achievement ?</h4>
				<br>
				<div>
					<form id = "{{ $ID }}DForm">                
						<button type = "button" class="btn btn-danger" id = "yes">Yes</button>
						<button type = "button" class="btn btn-success" data-dismiss="modal">No</button>
					</form>
				</div>            
			</div>
		</div>
	</div>
</div>
@endsection

@push('footer')
<script src="{{ asset('admin-assets/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
{{-- <script src="{{ asset('admin-assets/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script> --}}
<script type="text/javascript" src = "{{ asset('admin-assets/dataTables.buttons.min/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('admin-assets/dataTables.buttons.min/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-scroller/js/dataTables.scroller.min.js') }}"></script>
<script src="{{ asset('admin-assets/jszip/dist/jszip.min.js') }}"></script>
<script src="{{ asset('admin-assets/pdfmake/build/pdfmake.min.js') }}"></script>
<script src="{{ asset('admin-assets/pdfmake/build/vfs_fonts.js') }}"></script>
<script type="text/javascript">
	var update = "{{ url('admin/'.$ID) }}",
	deleteU = "{{ url('admin/'.$ID) }}",
	store = "{{ route('admin.'.$ID.'.store') }}";

	Table.init(ID);
</script>
@endpush