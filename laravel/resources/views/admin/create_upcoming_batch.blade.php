@extends('layouts.sidebar')
@section('title')
Create Upcoming Batch
@endsection
@push('header')
@php
$ID = 'upcoming-batch';
@endphp
<script>
    ID = '{{ $ID }}';
</script>
<style type="text/css">
#description{
    height: 200px;
}

</style>
{{-- <link href="{{ asset('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" media="screen"> --}}
<link rel="stylesheet" type="text/css" href="{{ asset('css/quill.snow.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-datetimepicker.min.css') }}">
@endpush
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="blue">
                    <h4 class="title">Create {{ ucwords($ID) }}</h4>                  
                </div>
                <div class="card-content">
                    <div class="col-md-offset-2 col-md-8">
                        @include('admin.forms.upcoming_batch_form')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('footer')
<script type="text/javascript" src="{{ asset('js/quill.js') }}"></script>
{{--    <script type="text/javascript" src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>--}}
<script type="text/javascript" src = "{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
{{-- <script type="text/javascript" src = "{{ asset('js/create-test.js') }}"></script> --}}
<script type="text/javascript" src="{{ asset('js/crud.js') }}"></script>
<script type="text/javascript">
    var options = {      
        placeholder: 'Upcoming Batch description...',    
        theme: 'snow'
    };
    var description = new Quill('#description', options);

    route = "{{ route('admin.'.$ID.'.store') }}";

    $('#{{ $ID }}Form').CRUD({
        url : route,
        extraVariables : function(){
            return {'description' : document.querySelector(".ql-editor").innerHTML};
        },
        processResponse : function(data){
            // console.log(data);
            // if(data.msg == 'success'){
            //     $('.ql-editor').html();
            // }
        },
        validation : false
    });
    imageUpload('img');
</script>
@endpush