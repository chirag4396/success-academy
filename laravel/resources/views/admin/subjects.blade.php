@extends('layouts.sidebar')
@section('title')
All {{ $test->test_title }} Subjects
@endsection
@push('header')
<link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.dataTables.min.css') }}">

@endpush
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header" data-background-color="blue">
					<h4 class="title">All {{ $test->test_title }} Subjects</h4>					
				</div>
				<div class="card-content table-responsive">

					<table id = "table" class="table table-hover">
						<thead>
							<th style="width: 100px;">Title</th>
							<th>Total Questions</th>
							<th>Action</th>
						</thead>
						<tbody>
							@forelse ($test->subjects()->get() as $sub)
							<tr>
								<td>{{ $sub->sub_title }}</td>
								<td>{{ $sub->questions()->count() }}</td>
								<td>
									<a href="{{ route('admin.all-questions', ['sub' => $sub->sub_id, 'test' => $test->test_id]) }}" class="btn btn-success">View Questions</a> 									
								</td>
							</tr>
							@empty
							<tr>
								<td colspan="5" class="text-center">
									No Test found, <a href="{{ url('/admin/test/create') }}" class="btn btn-success">Create one NOW!!</a>
								</td>
							</tr>
							@endforelse
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@push('footer')
<script type="text/javascript">
	$(document).ready(function() {
		$('#table').DataTable({
			responsive: true
		});
	});

</script>
<script type="text/javascript" src = "{{ asset('js/jquery.dataTables.min.js') }}"></script>
@endpush