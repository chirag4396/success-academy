@extends('layouts.sidebar')
@section('title')
All Tests
@endsection
@push('header')
<link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.dataTables.min.css') }}">

@endpush
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header" data-background-color="blue">
					<h4 class="title">All Tests</h4>					
				</div>
				<div class="card-content table-responsive">

					<table id = "table" class="table table-hover">
						<thead>
							<th style="width: 100px;">Title</th>
							<th>Package</th>
							<th>Duration</th>
							<th>Total Questions</th>
							<th>Created At</th>
							<th>Action</th>
						</thead>
						<tbody>
							@forelse ($tests as $test)				
							<tr>
								<td>{{ $test->test_title }}</td>
								<td>{{ $test->package->pack_title }}</td>
								<td>{{ $test->test_hours }} mins</td>
								<td>{{ \App\Question::where('qus_test_id',$test->test_id)->get()->count() }}</td>
								<td>{{ \Carbon\Carbon::parse($test->test_taken_at)->toFormattedDateString() }}</td>
								<td>
									{{-- <a href="{{ url('/admin/all-questions/'.$test->test_id) }}" class="btn btn-success">View Questions</a>--}}
									<a href="{{ route('admin.subjects', ['id' => $test->test_id]) }}" class="btn btn-success">Subjects ({{ $test->subjects()->count() }})</a> 									
								</td>
							</tr>
							@empty
							<tr>
								<td colspan="5" class="text-center">
									No Test found, <a href="{{ url('/admin/test/create') }}" class="btn btn-success">Create one NOW!!</a>
								</td>
							</tr>
							@endforelse
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@push('footer')
<script type="text/javascript">
	$(document).ready(function() {
		$('#table').DataTable({
			responsive: true
		});
	});

</script>
<script type="text/javascript" src = "{{ asset('js/jquery.dataTables.min.js') }}"></script>
@endpush