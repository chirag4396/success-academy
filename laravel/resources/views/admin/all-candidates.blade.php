@extends('layouts.sidebar')
@section('title')
All Candidates
@endsection
@push('header')
<link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.dataTables.min.css') }}">
@endpush
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header" data-background-color="blue">
					<h4 class="title">All Candidates</h4>					
				</div>
				<div class="card-content table-responsive">
					<table id = "table" class="table table-hover">
						<thead>
							<th>Sr. No.</th>
							<th>Name</th>
							{{-- <th>Batch</th> --}}
							<th>Mobile</th>
							<th>Parent Mobile</th>
							<th>Email</th>
							<th>Password</th>
							<th>Action</th>
						</thead>
						<tbody>
							@forelse ($candidates as $k => $can)
							<tr>
								<td>{{ ++$k }}</td>
								<td>{{ $can->name }}</td>
								{{-- <td>{{ $can->batch()->first()->batch_name }}</td> --}}
								<td>{{ $can->mobile }}</td>
								<td>{{ $can->p_mobile }}</td>
								<td>{{ $can->email }}</td>
								<td>{{ $can->visible_password }}</td>
								<td>
									<a href="{{url('admin/student/edit/'.$can->id	)}}" class="btn btn-danger"> Edit </a>
								</td>
							</tr>
							@empty
							<tr>
								<td colspan="5" class="text-center">
									No Questions found, <a href="{{ url('admin/add-question/'.$test->test_id) }}" class="btn btn-success">Add some NOW!!</a>
								</td>
							</tr>
							@endforelse
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@push('footer')
<script type="text/javascript">
	$(document).ready(function() {
		$('#table').DataTable();
	});
</script>
<script type="text/javascript" src = "{{ asset('js/jquery.dataTables.min.js') }}"></script>
@endpush