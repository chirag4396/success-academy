@extends('layouts.sidebar')
@section('title')
Create Test
@endsection
@push('header')
{{-- <link href="{{ asset('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" media="screen"> --}}
<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-datetimepicker.min.css') }}">
<link href="{{ asset('css/select2.css') }}" rel="stylesheet" />
@endpush
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header" data-background-color="blue">
					<h4 class="title">Create Test</h4>					
				</div>
				<div class="card-content">
					<div class="col-md-offset-2 col-md-8">
						<form id = "testForm">
							<div class="form-group label-floating">
								<label for="name" class="control-label">Test Title/Name</label>
								<input type="text" class="form-control" name="title" required>
							</div>
							<div class="form-group label-floating">
								<label for="pack" class="control-label">Package</label>
								@if ($pack)
								<input type="text" value="{{ $pack->pack_title }}" class="form-control" readonly disabled>
								<input type="hidden" value="{{ $pack->pack_id }}" class="form-control" name="package">
								@else
								<select name = "package" class="form-control" id = "batchSelection">
									<option value="-1">-Package-</option>
									@forelse (App\PackageDetail::get() as $pack)
									<option value="{{ $pack->pack_id }}" >{{ $pack->pack_title }}</option>
									@empty

									@endforelse
									<option value = "other">Others</option>
								</select>
								@endif
							</div>							
							<div class="form-group label-floating">
								<label for="name" class="control-label">Test Minutes</label>
								
								<input type="text" class="form-control" name="hours" required>								
							</div>
							<div class="form-group label-floating">
								<label for="name" class="control-label">Marks Pattern</label>
								
								<select class="form-control" name="pattern" id = "patternSelection">
									<option value="-1">Select</option>
									@forelse (App\MarkPattern::get() as $mark)
									<option value="{{ $mark->mark_id }}">{{ $mark->mark_title }}</option>								
									@empty
									@endforelse							
								</select>
								
							</div>
							<div class="form-group">
							    <label>Subject</label>
							    <select name = "subjects[]" class="form-control subjectDrop" multiple>
							        <option value = "-1">--select--</option>
							        @forelse (\App\Subject::get() as $sub)
							            <option value = "{{ $sub->sub_id }}">{{ $sub->sub_title }}</option>
							        @empty
							        @endforelse
							    </select>
							</div>
							<div class="form-group label-floating hidden" id="patternMark">
								<label for="name" class="control-label">Marks per Question</label>
								<input type="text" name = "score" id = "markBox" class="form-control">
							</div>
							<div class="form-group label-floating">
								<label for="name" class="control-label">Enable at </label>
								
								{{-- <input type="datetime" class="form-control" name="enable_at"> --}}
								<div class="input-group date form_datetime" id = "enableSelect" data-date="2017-01-1T05:25:07Z" data-date-format="dd MM yyyy - HH:ii p" data-link-field="enable">
									<input class="form-control" size="16" type="text" value="" readonly>
									<span class="input-group-addon">
										<span class="fa fa-calendar">
										</span>
									</span>
								</div>
								<input type="hidden" name="enable_at" id="enable" value="" />
								
							</div>
							<div class="form-group label-floating">
								<label for="name" class="control-label">Disable at </label>
								
								<div class="input-group date form_datetime" id = "disableSelect" data-date-format="dd MM yyyy - HH:ii p" data-link-field="disable">
									<input class="form-control" size="16" type="text" value="" readonly>
									<span class="input-group-addon">
										<span class="fa fa-calendar">
										</span>
									</span>
								</div>
								<input type="hidden" name="disable_at" id="disable" value="" />
								{{-- <input type="datetime" class="form-control" name="disable_at"> --}}
								
							</div>
							<div class="form-group label-floating">
								<div class="text-center">
									<button type="submit" name = "create-test" class="btn btn-primary">
										Create
									</button>
								</div>
							</div>
						</form>
						<div class="alert alert-success text-center hidden" id = "testMessage"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@push('footer')

{{--	<script type="text/javascript" src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>--}}
<script src="{{ asset('js/select2.min.js') }}"></script>
<script type="text/javascript" src = "{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
{{-- <script type="text/javascript" src = "{{ asset('js/create-test.js') }}"></script> --}}
<script type="text/javascript">
	$('#patternSelection').on({
		'change' : function(){
			if(this.value == 1){
				$('#patternMark').removeClass('hidden');
				$('#markBox').attr('required', true);
			}else{
				$('#patternMark').addClass('hidden');
				$('#markBox').attr('required', false);
			}
		}
	});
	$('#testForm').CRUD({
		url : '{{ route('admin.test.store') }}',
		validation : false
	});
	var d = '{{ Carbon\Carbon::now('Asia/Kolkata') }}';
	$('#enableSelect').datetimepicker({
        //language:  'fr',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        forceParse: 0,
        showMeridian: 1,
        startDate : d
    });
	$('#disableSelect').datetimepicker({
        //language:  'fr',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        forceParse: 0,
        showMeridian: 1,
        startDate : d
    });
    function geturl(name) {
		var link = '{{ route('admin.home') }}/'+name;
		return link;
	}
	multiSelect('.subjectDrop', 'subject');
</script>
@endpush