@extends('layouts.sidebar')
@section('title')
Create Download
@endsection
@push('header')
@php
$ID = 'download';
@endphp
<script>
    ID = '{{ $ID }}';
</script>
@endpush
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="blue">
                    <h4 class="title">Create {{ ucwords($ID) }}</h4>                  
                </div>
                <div class="card-content">
                    <div class="col-md-offset-2 col-md-8">
                        @include('admin.forms.download_form')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
