<!DOCTYPE html>
<html>
<head>
	<title>Success Academy</title>
</head>
<body>
	Thanks for Registering with us, please goto {{ route('admin.login') }} and login with following details
	<table>		
		<tr>
			<td><b>Username:</b></td>
			<td>{{ $email }}</td>
		</tr>
		<tr>
			<td><b>Password:</b></td>
			<td>{{ $pass }}</td>
		</tr>
	</table>
</body>
</html>