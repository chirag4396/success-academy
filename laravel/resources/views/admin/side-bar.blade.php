<div class="col-md-2 zero-padding">
	<div class="profile-sidebar">
		<!-- SIDEBAR USERPIC -->
		<div class="profile-userpic">
			<img src="{{ asset('images/logo.png') }}" class="img-responsive">
		</div>
		<!-- END SIDEBAR USERPIC -->
		<!-- SIDEBAR USER TITLE -->
		<div class="profile-usertitle">
			<div class="profile-usertitle-name">
				{{ Auth::user()->name }}
			</div>
			<div class="profile-usertitle-job">
				Joined on - {{ Carbon\Carbon::parse(Auth::user()->created_at)->toFormattedDateString() }}
			</div>
		</div>
		
		<div class="profile-userbuttons">

		</div>
		<!-- END SIDEBAR BUTTONS -->
		<!-- SIDEBAR MENU -->
		<div class="profile-usermenu">
			<ul class="nav">
				<li class="active">
					<a href="{{ url('admin/') }}">
						<i class="fa fa-home"></i>
						Dashboard
					</a>
				</li>
				<li class="li-section">Test</li>
				<li>
					<a href="{{ url('/admin/test/create') }}">
						<i class="fa fa-edit"></i>
						Create Test
					</a>
				</li>
				<li>
					<a href="{{ url('/admin/all-test') }}">
						<i class="fa fa-list"></i>
						All Test
					</a>
				</li>
				<li class="li-section">Student</li>
				<li>
					<a href="{{ url('/admin/student/create') }}">
						<i class="fa fa-edit"></i>
						Create Student
					</a>
				</li>
				<li>
					<a href="{{ url('/admin/all-students') }}">
						<i class="fa fa-list"></i>
						All Students
					</a>
				</li>

				<li class="li-section">Report</li>
				<li>
					<a href="{{ url('/admin/results') }}">
						<i class="fa fa-flag"></i>
						View Results
					</a>
				</li>
			</ul>
		</div>
		<!-- END MENU -->
	</div>
</div>