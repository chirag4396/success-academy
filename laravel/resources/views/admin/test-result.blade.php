@extends('layouts.sidebar')
@section('title')
Result
@endsection
@push('header')
<link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.dataTables.min.css') }}">
@endpush
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header" data-background-color="blue">
					<h4 class="title">Result for {{ $test->package->pack_title }}
						<a href = "{{ url('/admin/results') }}" class="btn btn-danger pull-right">Back</a></h4>                  
					</div>
					<div class="card-content">
						<table id = "questions" class="table table-hover">
							<thead>
								<th>Name</th>
								<th>Mobile</th>
								<th>Parent Mobile</th>
								<th>Attempeted</th>
								<th>Correct</th>
								<th>Marks</th>
							</thead>
							<tbody>
								@forelse ($students as $k => $student)	
								@php
								$t = $student->st_test_id;
								$id = $student->id;
								$attemptedQus = App\TestLog::where('tlog_test_id',$t)->where('tlog_user_id',$id)->count();


								$attemptedTrue = App\TestLog::where('tlog_test_id',$t)->where('tlog_user_id',$id)->where('tlog_correct', 1)->count();

								$trueMark = App\Question::whereIn('qus_id',App\TestLog::select('tlog_qus_id')->where('tlog_test_id',$t)->where('tlog_user_id',$id)->where('tlog_correct', 1)->get())->get()->sum(['qus_score']);

								$testMarks = App\Question::where('qus_test_id',$t)->get()->sum(['qus_score']);

								@endphp
								<tr>
									<td>{{ $student->name }}</td>
									<td>{{ $student->mobile }}</td>
									<td>{{ $student->p_mobile }}</td>
									<td>{{ $attemptedQus }}</td>
									<td>{{ $attemptedTrue }}</td>
									<td>{{ $trueMark.'/'.$testMarks }}</td>
									
								</tr>
								@empty
								<tr>
									<td colspan="5" class="text-center">
										No Record found
									</td>
								</tr>
								@endforelse
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@push('footer')
<script type="text/javascript">
	$(document).ready(function() {
		$('#questions').DataTable();
	});
</script>
<script type="text/javascript" src = "{{ asset('js/jquery.dataTables.min.js') }}"></script>
@endpush