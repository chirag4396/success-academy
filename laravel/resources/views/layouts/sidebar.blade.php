@include('layouts.header')
<div class="wrapper">
	<div id = "hide">
		<i class="material-icons">reorder</i>
	</div>
	<div id = "show">
		<i class="material-icons">reorder</i>
	</div>
	<div class="sidebar" data-color="blue" data-image="{{ asset('images/logo.png') }}">
		<div class="logo">
		{{-- 	<a href="http://www.creative-tim.com" class="simple-text">
				Creative Tim
			</a> --}}
			<a href="{{ url('/admin') }}">
				<img src="{{ asset('images/logo.png') }}">
			</a>
		</div>
		<div class="sidebar-wrapper">
			<ul class="nav">
				<li>
					<a href="{{ route('admin.home') }}">
						<i class="material-icons">dashboard</i>
						<p>Dashboard</p>
					</a>
				</li>				
				<li>
					<a href="#">
						<i class="material-icons">library_books</i>
						<p>Package</p>
					</a>
					<ul class="submenu">
						<li><a href="{{ route('admin.package.index') }}">Package List</a></li>
						<li><a href="{{ route('admin.package.create') }}">Create Package</a></li>
					</ul>
				</li>
				<li>
					<a href="#">
						<i class="material-icons">library_books</i>
						<p>Test</p>
					</a>
					<ul class="submenu">
						<li><a href="{{ url('/admin/all-test') }}">View Test</a></li>
						<li><a href="{{ route('admin.test.create') }}">Create Test</a></li>
					</ul>
				</li>
				<li>
					<a href="#">
						<i class="material-icons">library_books</i>
						<p>Candidates</p>
					</a>
					<ul class="submenu">
						<li><a href="{{ route('admin.all-candidates') }}">View Candidates</a></li>
						<li><a href="{{ url('/admin/results') }}">View Results</a></li>
					</ul>
				</li>
				<li>
					<a href="{{ route('admin.purchase.index') }}">
						<i class="material-icons">bubble_chart</i>
						<p>Purchases</p>
					</a>
				</li>
				<li>
					<a href="#">
						<i class="material-icons">library_books</i>
						<p>Achievement</p>
					</a>
					<ul class="submenu">
						<li><a href="{{ route('admin.achievement.create') }}">Create Achievement</a></li>
						<li><a href="{{ route('admin.achievement.index') }}">View Achievements</a></li>
					</ul>
				</li>
				<li>
					<a href="#">
						<i class="material-icons">library_books</i>
						<p>Upcoming Batches</p>
					</a>
					<ul class="submenu">
						<li><a href="{{ route('admin.upcoming-batch.create') }}">Create Upcoming Batch</a></li>
						<li><a href="{{ route('admin.upcoming-batch.index') }}">View Upcoming Batches</a></li>
					</ul>
				</li>
				<li>
					<a href="#">
						<i class="material-icons">library_books</i>
						<p>Galleries</p>
					</a>
					<ul class="submenu">
						<li><a href="{{ route('admin.gallery.create') }}">Create Gallery</a></li>
						<li><a href="{{ route('admin.gallery.index') }}">View Galleries</a></li>
					</ul>
				</li>
				<li>
					<a href="#">
						<i class="material-icons">library_books</i>
						<p>FAQ</p>
					</a>
					<ul class="submenu">
						<li><a href="{{ route('admin.faq.create') }}">Create FAQ</a></li>
						<li><a href="{{ route('admin.faq.index') }}">View FAQ</a></li>
					</ul>
				</li>
				<li>
					<a href="#">
						<i class="material-icons">library_books</i>
						<p>Smart Study</p>
					</a>
					<ul class="submenu">
						<li><a href="{{ route('admin.smart-study.create') }}">Create Smart Study</a></li>
						<li><a href="{{ route('admin.smart-study.index') }}">View Smart Study</a></li>
					</ul>
				</li>
				<li>
					<a href="#">
						<i class="material-icons">library_books</i>
						<p>Downloads</p>
					</a>
					<ul class="submenu">
						<li><a href="{{ route('admin.download.create') }}">Create Download</a></li>
						<li><a href="{{ route('admin.download.index') }}">View Downloads</a></li>
					</ul>
				</li>
				<li>
					<a href="#">
						<i class="material-icons">library_books</i>
						<p>Blogs</p>
					</a>
					<ul class="submenu">
						<li><a href="{{ route('admin.blog.create') }}">Create Blog</a></li>
						<li><a href="{{ route('admin.blog.index') }}">View Blogs</a></li>
					</ul>
				</li>		
				<li>
					<a href="#">
						<i class="material-icons">library_books</i>
						<p>Testimonial</p>
					</a>
					<ul class="submenu">
						<li><a href="{{ route('admin.testimonial.create') }}">Create Testimonial</a></li>
						<li><a href="{{ route('admin.testimonial.index') }}">View Testimonials</a></li>
					</ul>
				</li>		
				<li>
					<a href="#">
						<i class="material-icons">library_books</i>
						<p>Banners</p>
					</a>
					<ul class="submenu">
						<li><a href="{{ route('admin.banner.create') }}">Create Banner</a></li>
						<li><a href="{{ route('admin.banner.index') }}">View Banners</a></li>
					</ul>
				</li>
				<li>
					<a href="#">
						<i class="material-icons">library_books</i>
						<p>Lectures</p>
					</a>
					<ul class="submenu">
						<li><a href="{{ route('admin.lecture.create') }}">Create Lecture</a></li>
						<li><a href="{{ route('admin.lecture.index') }}">View Lectures</a></li>
					</ul>
				</li>
				<li>
					<a href="#">
						<i class="material-icons">library_books</i>
						<p>Whats New</p>
					</a>
					<ul class="submenu">
						<li><a href="{{ route('admin.whats-new.create') }}">Create Whats New</a></li>
						<li><a href="{{ route('admin.whats-new.index') }}">View Whats New</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
	<div class="main-panel">
		<nav class="navbar navbar-transparent navbar-absolute">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					{{-- <a class="navbar-brand" href="#"> Material Dashboard </a> --}}
				</div>
				<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav navbar-right">
						{{-- <li>
							<a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
								<i class="material-icons">dashboard</i>
								<p class="hidden-lg hidden-md">Dashboard</p>
							</a>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<i class="material-icons">notifications</i>
								<span class="notification">5</span>
								<p class="hidden-lg hidden-md">Notifications</p>
							</a>
							<ul class="dropdown-menu">
								<li>
									<a href="#">Mike John responded to your email</a>
								</li>
								<li>
									<a href="#">You have 5 new tasks</a>
								</li>
								<li>
									<a href="#">You're now friend with Andrew</a>
								</li>
								<li>
									<a href="#">Another Notification</a>
								</li>
								<li>
									<a href="#">Another One</a>
								</li>
							</ul>
						</li> --}}
						<li>
							<a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
								<i class="material-icons">person</i>
								{{ Auth::user()->name }}
							</a>
							<ul class="dropdown-menu">
								<li>
									<a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">

										Logout
									</a>

									<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
										{{ csrf_field() }}
									</form>
								</li>

							</ul>
						</li>
					</ul>
					{{-- <form class="navbar-form navbar-right" role="search">
						<div class="form-group  is-empty">
							<input type="text" class="form-control" placeholder="Search">
							<span class="material-input"></span>
						</div>
						<button type="submit" class="btn btn-white btn-round btn-just-icon">
							<i class="material-icons">search</i>
							<div class="ripple-container"></div>
						</button>
					</form> --}}
				</div>
			</div>
		</nav>
		<div class="content">
			@yield('content')
		</div>
		@include('layouts.footer')