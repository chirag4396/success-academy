@include('layouts.header')
<div class="wrapper">        
    <div class="main-panel" style="width: 100%!important;">
        <nav class="navbar navbar-transparent navbar-absolute">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class = "pad-left" href="{{ url('/candidate/test') }}"> 
                        <img src="{{ asset('images/fav-icon.png') }}" width = "70">
                    </a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><div class="timer-div" id = "ms_timer"></div></li>
                        <li>
                            <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="material-icons">person</i>
                                {{ Auth::user()->name }}

                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">

                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                                
                            </ul>
                        </li>
                    </ul>                        
                </div>
            </div>
        </nav>
        <div class="content" style="margin-top: 50px!important;">

            @yield('content')

        </div>
        @include('layouts.footer')