<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/png" href="{{ asset('images/fav-icon.png') }}" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
	<meta name="viewport" content="width=device-width" />
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<style type="text/css">
		.loader{		
			position: fixed;
			z-index: 111111;
			background: #eee;
			width: 100%;
			text-align: center;
			height: 1000px;
			padding-top: 15%;
		}		
	</style>
	<title>{{ config('app.name') }} | @yield('title')</title>

	<link href="{{ asset('css/style.css') }}" rel="stylesheet">
	<!-- Bootstrap core CSS     -->
	<link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" />
	<!--  Material Dashboard CSS    -->
	<link href="{{ asset('assets/css/material-dashboard.css?v=1.2.0') }}" rel="stylesheet" />
	<!--  CSS for Demo Purpose, don't include it in your project     -->
	{{-- <link href="{{ asset('assets/css/demo.css') }}" rel="stylesheet" /> --}}

	<link href="{{ asset('font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">	
    <link rel="stylesheet" type="text/css" href="{{ asset('css/quill.snow.css') }}">
	@stack('header')
	<!--     Fonts and icons     -->
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
	<link href='https://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>
	
</head>

<body>

	<div class="loader" id = "loader">
		<img src="{{ asset('images/logo.png') }}" width = "120">
		<p style="margin-top:10px;">Loading...</p>
	</div>