@include('layouts.header')
<div class="wrapper">
	<div class="logo">
		<a href="http://www.sungare.com" class="simple-text">
			<img src="{{ asset('images/logo.png') }}">
		</a>
	</div>
	<div class="content">
		@yield('content')
	</div>
	@include('layouts.footer')