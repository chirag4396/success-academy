<footer class="footer">
	<div class="container-fluid">
		{{-- <nav class="pull-left">
			<ul>
				<li>
					<a href="#">
						Home
					</a>
				</li>
				<li>
					<a href="#">
						Company
					</a>
				</li>
				<li>
					<a href="#">
						Portfolio
					</a>
				</li>
				<li>
					<a href="#">
						Blog
					</a>
				</li>
			</ul>
		</nav> --}}
		<p class="copyright text-center">
			&copy;
			<script>
				document.write(new Date().getFullYear())
			</script>
			, Designed and Developed by <a href="http://www.sungare.com">Sungare Technologies Pvt. Ltd.</a>
		</p>
	</div>
</footer>
</div>
</div>
</body>
<!--   Core JS Files   -->
<script src="{{ asset('assets/js/jquery-3.2.1.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/material.min.js') }}" type="text/javascript"></script>
<!--  PerfectScrollbar Library -->
<script src="{{ asset('assets/js/perfect-scrollbar.jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/quill.js') }}"></script>

<!-- Material Dashboard javascript methods -->
<script src="{{ asset('assets/js/material-dashboard.js?v=1.2.0') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery-ui.min.js') }}"></script>
<script type="text/javascript">
	var mainUrl = '{{ route('u_home') }}/';
</script>
<script type="text/javascript" src="{{ asset('js/crud.js') }}"></script>

<script type="text/javascript">
	// $.ajaxSetup({
	// 	headers: {
	// 		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	// 	}
	// });
	type = {{Auth::guest() ? '0' : Auth::user()->type}};

	$url = "{{Request::ip() != '127.0.0.1' ? '/admin':'/'}}";
	var rootPath = "{{ route('admin.home') }}/";
</script>
@stack('footer')
{{-- <script type="text/javascript" src = "{{ asset('js/footer.js') }}"></script> --}}
<script type="text/javascript">
	$('#hide').on({
		'click': function () {
			$('.sidebar').toggle('slide');
			$('.main-panel').css('width','100%');
			$(this).toggle('slide');
			$('#show').toggle('slide');
			// $('html').addClass('nav-open');
		}
	});
	$('#show').on({
		'click': function () {
			$('.sidebar').toggle('slide');			
			$('.main-panel').css('width','calc(100% - 260px)');
			
			$(this).toggle('slide');
			$('#hide').toggle('slide');			
			// $('html').addClass('nav-open');
		}
	});
	// var all = $('.sidebar-wrapper li a').attr('href');
	// console.log(all);
	@php		
	$check = [route('admin.home'),route('admin.package.create'), route('admin.test.create'), route('admin.all-test'), route('admin.result')];
	if(in_array(\Request::fullUrl(), $check)){
		\Request::session()->put('page', \Request::fullUrl());
	}
	@endphp
	var current = '{{ \Request::session()->get('page') }}';	
	$('.sidebar-wrapper li').each(function(k,v){
		var curl = $(v).find('a').attr('href');
		$(v).addClass((curl == current) ? 'active' : '');
	});

	// $('.sidebar-wrapper ul.nav > li').not("ul li ul li").each(function(k,v){
	// 	var curl = $(v).find('a').attr('href');
	// 	console.log(v);
	// 	$(v).addClass((curl == current) ? 'active' : '');
	// });
	$(window).on('load',function(){
		$('#loader').addClass('hidden');
	});
</script>
@stack('last')
</html>

