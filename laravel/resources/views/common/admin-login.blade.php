@extends('layouts.simple')

@section('title')
    Admin Login
@endsection

@section('content')

<div class="content">
    <div class="container-fluid">
        <div class="row">                   

            <div class="col-md-offset-4 col-md-4">
                <div class="card card-profile">
                    <div class="card-avatar">
                        <a href="#pablo">
                            <img class="img" src="{{ asset('assets/img/user-icon.png') }}" />
                        </a>
                    </div>
                    <div class="content">
                        <h6 class="category text-gray">Candidate Login</h6>                        
                        <div class="col-md-12">
                            <form method="POST" action="{{ route('login') }}">
                                {{ csrf_field() }}
                                <div class="form-group label-floating{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label class="control-label"  style="left: 0;">Email address</label>
                                    <input type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                    @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group label-floating{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label class="control-label" style="left: 0;">Password</label>
                                    <input type="password" class="form-control" name="password" required>

                                    @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <input type="submit" class="btn btn-primary btn-round" value="Login">

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

