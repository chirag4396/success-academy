@extends('layouts.topbar')

@section('content')
<div class="container-fluid">
    <div class="row">                   

        <div class="col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="blue">
                    Results
                </div>
                <div class="card-content text-center">
                    <h2>Attempted : {{ $count }}/30</h2> 
                    <h2>Score : {{ $true }}</h2>
                    <h2>Percent : {{ round($true * 100 / 30) }} %</h2>
                </div>
                <div class="panel-footer">
                    You must accquired morethan 60% for machine round.
                </div>                
            </div>
        </div>
    </div>
</div>
@endsection