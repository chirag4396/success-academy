@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Add Questions</div>
                <div class="panel-body">
                    <form class="form-horizontal" id = "questionForm">
                        <div class="form-group">
                        <label for="name" class="col-md-2 control-label">Skill</label>

                            <div class="col-md-9">
                                <select class="form-control" name = "skill">
                                    <option>-select-</option>
                                    @forelse (App\Skill::get() as $skill)
                                        <option value="{{ $skill->skill_id }}">{{ $skill->skill_name }}</option>
                                    @empty
                                        {{-- empty expr --}}
                                    @endforelse
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-md-2 control-label">Qustion</label>

                            <div class="col-md-9">
                                <textarea name = "detail" rows = "10" class="form-control"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">Option A</label>

                            <div class="col-md-9">                                
                                <div class="input-group">
                                    <input type="text" name = "option[]" class="form-control" required>
                                    <div class="input-group-btn">
                                        <input type="radio" name = "answer" value="0">
                                    </div>
                                </div>        
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Option B</label>

                            <div class="col-md-9">                                
                                <div class="input-group">
                                    <input type="text" name = "option[]" class="form-control" required>
                                    <div class="input-group-btn">
                                        <input type="radio" name = "answer" value="1">
                                    </div>
                                </div>        
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Option C</label>

                            <div class="col-md-9">                                
                                <div class="input-group">
                                    <input type="text" name = "option[]" class="form-control" required>
                                    <div class="input-group-btn">
                                        <input type="radio" name = "answer" value="2">
                                    </div>
                                </div>        
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Option D</label>

                            <div class="col-md-9">                                
                                <div class="input-group">
                                    <input type="text" name = "option[]" class="form-control" required>
                                    <div class="input-group-btn">
                                        <input type="radio" name = "answer" value="3">
                                    </div>
                                </div>        
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary">
                                    Add Question
                                </button>
                            </div>
                        </div>
                    </form>
                    <div class="alert alert-success text-center hidden" id = "questionMessage"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('footer')
<script type="text/javascript" src = "{{ asset('js/questions.js') }}"></script>
@endpush