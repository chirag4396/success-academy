@extends('layouts.app')
@section('title')
    Upload Excel
@endsection
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Candidate Bulk Registeration</div>
				<div class="panel-body">
					<form class="form-horizontal" action="/ExcelUpload" class="form-horizontal" method="post" enctype="multipart/form-data">
						<input type="hidden" name="MAX_FILE_SIZE" value="2000000" />

						<div class="form-group">
							<label for="highest" class="col-md-4 control-label">Selecte Designation</label>
							<div class="col-md-6">
								<select class="form-control" id = "desSelection">
									<option value="-1">-Designation-</option>
									@forelse (App\Designation::get() as $des)
									<option value = "{{ $des->des_id }}">{{ $des->des_name }}</option>

									@empty

									@endforelse
									<option value = "other">Others</option>
								</select>
							</div>
						</div>		
						<input type="hidden" name = "designation" id = "designation">

						<div class="form-group hidden" id="desOther">
							<div class="col-md-offset-4 col-md-6">
								<div class="input-group">
									<input type="text" class="form-control">
									<div class="input-group-btn">
										<button id = "addDes" class="btn btn-default" type="submit">
											Add
										</button>
									</div>
								</div>                            
							</div>
						</div>	
						<div class="form-group">
							<label for="highest" class="col-md-4 control-label">Excel:</label>
							<div class="col-md-6">
								<input type="file" class="form-control" name="import_file" />
								{{ csrf_field() }}
							</div>
						</div>
						<div>
							<p><a href = "{{ url('others/demo.xlsx') }}">Click here</a> to Download Example File, which you need to upload here.</p>
						</div>
						<div class="form-group">
							<div class="text-center">
								<button type="submit" class="btn btn-primary"  value="Upload" name = "up" id = "subBtn"> Insert Records </button>
								
							</div>
						</div>
					</form>

					@if (Session::get('success'))						
						<div class="alert alert-success text-center" id = "candidateMessage">{{ Session::get('success') }}</div>
					@endif
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@push('footer')
<script type="text/javascript" src = "{{ asset('js/excel.js') }}"></script>
@endpush