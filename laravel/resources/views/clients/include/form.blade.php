<form class="form-horizontal" id = "clientForm" onsubmit="event.preventDefault();sendData('add', 'Successfully Added');">                        
    <input type="hidden" id = "id" name="id">
    <div class="form-group">
        <label for="name" class="col-md-4 control-label">Name</label>

        <div class="col-md-6">
            <input id="name" type="text" class="form-control" name="name"  autofocus>
        </div>
    </div>
    <div class="form-group">
        <label for="name" class="col-md-4 control-label">Business Name</label>

        <div class="col-md-6">
            <input id="bname" type="text" class="form-control" name="business" required autofocus>
        </div>
    </div>
    <div class="form-group">
        <label for="email" class="col-md-4 control-label">E-Mail</label>

        <div class="col-md-6" id = "emailBox">
            <input id="email" type="email" class="form-control" name="email[]">
            <span></span>
        </div>
        <div class="col-md-2">
            <button class="btn btn-add" id = "addEmail" onclick="addInput('email');" type="button"><i class="fa fa-plus"></i></button>
        </div>
    </div>
    <div class="form-group">
        <label for="mobile" class="col-md-4 control-label">Mobile</label>

        <div class="col-md-6" id = "mobileBox">
            <input id="mobile" type="phone" class="form-control" name="mobile[]" required = "required">
        </div>
        <div class="col-md-2">
            <button class="btn btn-add" id = "addEmail" onclick="addInput('mobile');" type="button"><i class="fa fa-plus"></i></button>

        </div>
    </div>
    <div class="form-group">
        <label for="remark" class="col-md-4 control-label">Remark (if any)</label>

        <div class="col-md-6" id = "remarkBox">
            <input id="remark" type="text" class="form-control" name="remark[]">
        </div>
        <div class="col-md-2">
            <button class="btn btn-add" onclick="addInput('remark');" type="button"><i class="fa fa-plus"></i></button>
        </div>
    </div>                        
    <div class="form-group">
        <label for="highest" class="col-md-4 control-label">Business Type</label>
        <div class="col-md-6">
            <select class="form-control" id = "cbtSelection">
                <option value="-1">-Type-</option>
                @forelse (App\ClientBusinessType::get() as $cbt)
                <option value = "{{ $cbt->cbt_id }}">{{ $cbt->cbt_title }}</option>

                @empty

                @endforelse
                <option value = "other">Others</option>
            </select>
        </div>
    </div>      
    <input type="hidden" name = "cbt" id = "cbt">

    <div class="form-group hidden" id="cbtOther">
        <div class="col-md-offset-4 col-md-6">
            <div class="input-group">
                <input type="text" class="form-control">
                <div class="input-group-btn">
                    <button type = "button" id = "addCbt" class="btn btn-default">
                        Add
                    </button>
                </div>
            </div>                            
        </div>
    </div>  
    <div class="form-group">
        <label class="col-md-4 control-label">Address</label>
        <div class="col-md-6">
            <textarea id = "address" class="form-control" name="address"></textarea>
        </div>                            
    </div>                        
    <div class="form-group">        
        <div class="text-center" id = "formButton">
            <button type="submit" id = "addClient" class="btn btn-primary">
                Register
            </button>
        </div>
    </div>
</form>
<div class="alert alert-success text-center hidden" id = "clientMessage"></div>

@push('last')
<script type="text/javascript" src = "{{ asset('js/client.js') }}"></script>
@endpush