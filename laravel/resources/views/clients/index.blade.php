@extends('layouts.app')

@section('title')
All Clients
@endsection
@push('header')
<link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.dataTables.min.css') }}">
@endpush
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        <div class="col-md-2">
            <h5>All Candidates</h5>
        </div>
        <div class="col-md-2">
        <button type="button" class="btn btn-primary" id = "addClientBtn">Add New</button>
        </div>
        <div class="col-md-2 pull-right">
            <select id = "sDesignation" class="form-control">
                <option value="-1">-Designation-</option>
                @forelse (App\Designation::get() as $designation)
                <option value="{{ $designation->des_id }}">{{ $designation->des_name }}</option>
                @empty

                @endforelse
            </select>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="panel-body">

        <table id = "user_role_table" class="table">
            <thead>
                <tr>
                    {{-- <th><input type="checkbox" name="select_all" value="1" id="example-select-all"></th> --}}
                    <th>Business</th>
                    <th>Name</th>
                    <th>Mobile</th>                                
                    <th>Remark</th>
                    <th>Status</th>
                    <th>Date</th>
                    <th>Action</th>                    
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>

        <div id="addClientModal" class="modal fade" role="dialog">
            <div class="modal-dialog">              
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Add new Candidate Details</h4>
                    </div>
                    <div class="modal-body">
                        @include('clients.include.form')                
                    </div>                        
                </div>
            </div>
        </div>
        <div id="deleteModal" class="modal fade" role="dialog">
            <div class="modal-dialog">              
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Are you sure you want to delete <span id = "deleteData"></span></h4>
                    </div>
                    <div class="modal-body text-center">
                        <button class = "btn btn-danger" id = "delete" onclick="deleteClient($(this).attr('data-value'));">Yes</button>
                        <button class = "btn btn-warning" id = "delete" data-dismiss="modal">No</button>
                    </div>                        
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('footer')
<script type="text/javascript" src = "{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src = "{{ asset('js/view-client.js') }}"></script>
{{-- <script type="text/javascript" src = "{{ asset('js/candidate.js') }}"></script> --}}
{{-- <script type="text/javascript" src = "{{ asset('js/excel.js') }}"></script> --}}
@endpush