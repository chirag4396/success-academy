@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Registeration</div>
                <div class="panel-body">
                    <form class="form-horizontal" id = "candidateForm">
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" required autofocus>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="mobile" class="col-md-4 control-label">Mobile</label>

                            <div class="col-md-6">
                                <input id="mobile" type="text" class="form-control" name="mobile" required>
                                
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="alt-mobile" class="col-md-4 control-label">Alternate Mobile</label>

                            <div class="col-md-6">
                                <input id="alt-mobile" type="text" class="form-control" name="alt_mobile" required>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="highest" class="col-md-4 control-label">Highest Degree</label>
                            <div class="col-md-6">
                                <select class="form-control" id = "highestSelection">
                                    <option>-Degree-</option>
                                    @forelse (App\Degree::get() as $degree)
                                    <option value="{{ $degree->degree_id }}">{{ $degree->degree_name }}</option>
                                    @empty

                                    @endforelse
                                    <option value = "other">Others</option>
                                </select>
                            </div>
                        </div>
                        <input type="hidden" name="highest_degree" id = "highest">
                        <div class="form-group" id="highestOther">
                            <div class="col-md-offset-4 col-md-6">
                                <div class="input-group">
                                    <input type="text" class="form-control" required>
                                    <div class="input-group-btn">
                                        <button id = "addDegree" class="btn btn-default" type="submit">
                                            Add
                                        </button>
                                    </div>
                                </div>                            
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="highest" class="col-md-4 control-label">Passout Year</label>
                            <div class="col-md-6">
                                <select class="form-control" name = "passout_year">
                                    <option>-Year-</option>
                                    @for ($i = 2010; $i <= 2018; $i++)
                                    <option value="{{ $i }}">{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>
                        <div class="form-group">                            
                            <div class="col-md-offset-4 col-md-3">
                                <input type="radio" name="type" id = "fresher"> Fresher
                            </div>
                            <div class="col-md-3">
                                <input type="radio" name="type" id = "exprienced"> Experienced

                            </div>
                        </div>
                        <div class="panel panel-default panel-body hidden" id = "typeSection">
                            <div id = "exprienceSection" class="hidden">
                                <div class="form-group">
                                    <label for="alt-mobile" class="col-md-4 control-label">Experience</label>
                                    <div class="col-md-3">
                                        <select class="form-control" name = "exp_year">
                                            <option>-Year-</option>
                                            @for ($i = 0; $i <= 10; $i++)
                                            <option value="{{ $i }}">{{ $i }}</option>
                                            @endfor
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <select class="form-control" name = "exp_month">
                                            <option>-Month-</option>
                                            @for ($i = 1; $i <= 12; $i++)
                                            <option value="{{ $i }}">{{ $i }}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="alt-mobile" class="col-md-4 control-label">Current CTC</label>

                                    <div class="col-md-6">
                                        <div class="input-group">                                    
                                            <input id="alt-mobile" type="text" class="form-control" name="current_ctc">
                                            <span class="input-group-addon">lacs</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id = "fresherSection">
                                <div class="form-group">
                                    <label for="alt-mobile" class="col-md-4 control-label">Expected CTC</label>

                                    <div class="col-md-6">
                                        <div class="input-group">                                    
                                            <input id="alt-mobile" type="text" class="form-control" name="expected_ctc" required>
                                            <span class="input-group-addon">lacs</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="alt-mobile" class="col-md-4 control-label">Skills you know</label>
                                    <div class="col-md-8">                                
                                        @forelse (App\Skill::get() as $skill)
                                        <div class="col-md-4">
                                            <input type="checkbox" name = "skill[]" value = "{{ $skill->skill_id }}"> {{ $skill->skill_name }}
                                        </div>                                
                                        @empty
                                        {{-- empty expr --}}
                                        @endforelse                                
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                    <div class="alert alert-success text-center hidden" id = "candidateMessage"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('footer')

<script type="text/javascript" src = "{{ asset('js/candidate.js') }}"></script>
@endpush