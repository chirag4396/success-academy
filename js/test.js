// document.onkeydown = function(){
//   switch (event.keyCode){
//         case 116 : //F5 button
//             event.returnValue = false;
//             event.keyCode = 0;
//             return false;
//         case 82 : //R button
//             if (event.ctrlKey){
//                 event.returnValue = false;
//                 event.keyCode = 0;
//                 return false;
//             }
//     }
// }

$(function(){
    function getUrl(){

        var p = location.href.split('=');
        if(p.length < 2){
            d = 2;
        }else{
            d = parseInt(p[1])+1;
        }
        if(!tlog){
            var url = $urlf;
        }else{        
            if(d > t){
                if(skip > 0){
                    if (snext == 0) {
                        var url = $urll;
                    }else{

                        var url = $urll+"?page="+snext;                
                    }
                }else{
                    var url = $urlf;
                }
            }else{                
                var url = $urll+"?page="+d;
            }
        }

        return url;
    }
    function sendData(url = null){
        // console.log($urll);
        var f = new FormData($('#questionForm')[0]);
        var p = location.href.split('=');
        if(p.length < 2){
            d = 0;
        }else{
            d = parseInt(p[1]);
        }

        f.append('page',d);
        // if(f.get('option') == null){
            // var chk = $.inArray(que,skip);
            // if(chk == -1){
            //     skip.push(que);
            // }
            // $.session.set("skip", skip);
        // }
        $.ajax({
            url : $urll,
            type : 'post',
            data : f,
            processData:false,
            contentType : false,
            success : function(data){            
                // console.log(data);
                if(data.indexOf('OK') >= 0){
                    location.href = url;
                }
            }
        });
    }
    $('#ms_timer').countdowntimer({
        minutes : $min,
        seconds : $sec,
        size : "lg",
        timeUp : timeIsUp
    }); 
    function timeIsUp() {        
        sendData(getUrl());
    }       

    $('#next').on({
        'click' : function(){            
            sendData(getUrl());
        }
    });
    $('#finish').on({
        'click' : function(){
            var f = new FormData($('#questionForm')[0]);
            var p = location.href.split('=');
            if(p.length < 2){
                d = 0;
            }else{
                d = parseInt(p[1]);
            }

            f.append('page',d);
            // if(f.get('option') == null){
                // var chk = $.inArray(que,skip);
                // if(chk == -1){
                //     skip.push(que);
                // }
                // $.session.set("skip", skip);
            // }
            $.ajax({
                url : $urll,
                type : 'post',
                data : f,
                processData:false,
                contentType : false,
                success : function(data){            
                    // console.log(data);
                    if(data.indexOf('OK') >= 0){
                        location.href = $urlf;
                    }
                }
            });
        }
    });    
    $('#palettes a').each(function(index, el) {
        $(el).on({
            'click' : function(){
                var urlLink = $(this).attr('data-link');
                sendData(urlLink);
            }
        });
    });
});  