
$.fn.dataTable.ext.search.push(function( settings, data, dataIndex ) {

    var designation = $('#sDesignation').val();

    if(designation == data[7]){
        return true;
    }
    return false;            
});
function loader(){
    return "Hang on. Waiting for response...";
}
// function timeToString( $inTimestamp ) {
//   $now = time();
//   if( abs( $inTimestamp-$now )<86400 ) {
//     $t = date('g:ia',$inTimestamp);
//     if( date('zY',$now)==date('zY',$inTimestamp) )
//       return 'Today, '.$t;
//     if( $inTimestamp>$now )
//       return 'Tomorrow, '.$t;
//     return 'Yesterday, '.$t;
//   }
//   if( ( $inTimestamp-$now )>0 ) {
//     if( $inTimestamp-$now < 604800 )
//       return date( 'l, g:ia' , $inTimestamp );
//     if( $inTimestamp-$now < 1209600 )
//       return 'Next '.date( 'l, g:ia' , $inTimestamp );
//   } else {
//     if( $now-$inTimestamp < 604800 ) 
//       return 'Last '.date( 'l, g:ia' , $inTimestamp );
//   }

//   return date( 'l jS F, g:ia' , $inTimestamp );
// }
// console.log('2017-07-03 04:52:53'.toDateString());
$(document).ready(function() {

    var table = $('#user_role_table').DataTable({
        processing: true,        
        serverSide: true,
        // scrollX : true,
        ajax: {
            url : '/getClients',
            type : 'post',
            data : function(d){
                d.data = $('#sDesignation').val();
            }
        },
        "language": {
            "processing": loader()
        },
        columns: [
        // { data: 'c_id', name: 'c_id', visible : false },
        { data: 'c_business', name: 'c_business' },
        { data: 'c_name', name: 'c_name' },        
        {},
        {},
        { data: 'c_status', name: 'c_status'},                
        { data: 'c_updated_at', name: 'c_updated_at'},
        {}
        ],
        'columnDefs': [{
            'targets': 1,            
            'render': function (data, type, full, meta){                

                return '<div class="name-edit" onclick = "getName(this.id,$(this).html());" id = "c-'+full.c_id+'">'+full.c_name+'</div>';

                // return '<div onclick = "getStatus(this.id,$(this).html());" id = "can-'+full.c_id+'">'+full.status.sta_name+'</div>';
            }
        },{
            'targets': 2,
            'className': 'dt-body-center',
            'render': function (data, type, full, meta){
                //var mc = full.mobile.length > 1 ? full.mobile.length-1 : full.mobile.length;
                var le = full.mobile.length;
                mc = le === 0 ? le : le-1;
                //var mc = full.mobile.length-1;
                
                
                // m = le === 0 ? '' : full.mobile[mc].cm_number;
                var m;
                if(le > 0){
                    m = full.mobile[mc].cm_number !== null ? full.mobile[mc].cm_number : '-';
                }else{
                    m = '';
                }
                c = mc > 0 ? '<p class="remark-count" id = "pm-'+full.c_id+'" onclick = "getAllMobile('+full.c_id+')">'+mc+'</p>' : '';
                
                return '<div class="col-md-9" id = "mobile'+full.c_id+'Box">'
                +'<div>'+m+'</div>'
                +'</div>'
                +'<div class="col-md-2">'
                +c                
                +'</div>';

                // return '<div onclick = "getStatus(this.id,$(this).html());" id = "can-'+full.c_id+'">'+full.status.sta_name+'</div>';
            }
        },{
            'targets': 3,            
            'render': function (data, type, full, meta){
                // console.log(full);
                var flen = full.remark.length;
                fc = flen === 0 ? flen : flen-1;
                if(flen > 0){
                    f =full.remark[fc].cr_text !== null ? full.remark[fc].cr_text : '';
                }else{
                    f = '';
                }
                
                
                c = fc > 0 ? '<p class="remark-count" id = "p-'+full.c_id+'" onclick = "getAllRemarks('+full.c_id+')">'+fc+'</p>' : '';
                
                return '<div class="col-md-10" id = "remark'+full.c_id+'Box">'
                +'<input type="text" class="form-control" onkeyup="addRemark('+full.c_id+',this.value,'+full.remark[fc].cr_id+');" value="'+f+'">'
                +'</div>'
                +'<div class="col-md-2">'
                +c
                +'<button class="btn btn-add" id = "addEmail" onclick="addInput(\'remark'+full.c_id+'\',\''+full.c_id+'\');" type="button"><i class="fa fa-plus"></i></button>'
                +'</div>';
            }
        },{
            'targets': 4,
            'className': 'dt-body-center',
            'render': function (data, type, full, meta){
                // console.log(full);
                return '<div onclick = "getStatus(this.id,$(this).html());" id = "cl-'+full.c_id+'">'+full.status.sta_name+'</div>';
            }
        },{
            'targets': 6,
            'className': 'dt-body-center',
            'render': function (data, type, full, meta){

                return '<button class = "btn btn-success" onclick = "getData('+full.c_id+');">'
                +'<i class = "fa fa-pencil-square-o"></i>'
                +'</button> | '
                +'<button class = "btn btn-danger" onclick = "deleteT('+full.c_id+',\''+full.c_business+'\');">'
                +'<i class = "fa fa-trash-o"></i>'
                +'</button> | '
                +'<button class = "btn btn-link '+(full.c_called == 0 ? 'callRed' : 'callGreen') +'" onclick="callStatus(\''+full.c_id+'\',\''+full.c_called+'\',this);">'
                +'<i class = "fa fa-phone"></i>'
                +'</button>';
            }
        }
        // {
        //     'targets': ,
        //     'className': 'dt-body-center',
        //     'render': function (data, type, full, meta){

        //         return timeToString( full.c_updated_at );
        //     }
        // }
        ],
        "createdRow": function( row, data, dataIndex ) {

            $(row).attr('id', 'tr-'+data.c_id).addClass(data.status.sta_color);
        }
        /*,"aoColumnsDefs": [
            null,null,null,null,null,null,null,null
            ]*/
        });


$('#sDesignation').on({
    "change": function() {
        table.ajax.reload();
    }
});

$('#example-select-all').on('click', function(){

    var rows = table.rows({ 'search': 'applied' }).nodes();

    $('input[type="checkbox"]', rows).prop('checked', this.checked);
});

$('#example tbody').on('change', 'input[type="checkbox"]', function(){

    if(!this.checked){
        var el = $('#example-select-all').get(0);

        if(el && el.checked && ('indeterminate' in el)){
            el.indeterminate = true;
        }
    }
});
// $('#deleteModal').modal('toggle');
$('#frm-example').on('submit', function(e){
    var form = this;

    table.$('input[type="checkbox"]').each(function(){

        if(!$.contains(document, this)){

            if(this.checked){

                $(form).append(
                    $('<input>')
                    .attr('type', 'hidden')
                    .attr('name', this.name)
                    .val(this.value)
                    );
            }
        }
    });
});
});

function callStatus(id,val,d) {
    // console.log(id,val,d);
    if($('#tr-'+id+' input[type="text"]').val().length > 0){
        $('#tr-'+id+' p').remove();
        $.ajax({
            url : '/client/'+id,
            data : {called : val, '_method' : 'put'},
            type : 'post',
            success : function (data) {
                // console.log(data);
                if(data == 1){
                    $(d).removeClass('callRed').addClass('callGreen');
                }else{
                    $(d).removeClass('callGreen').addClass('callRed');
                }
                $('#user_role_table').DataTable().ajax.reload(null,false);
                fetch_client_notifications();
            }
        });
    }else{
        $('#tr-'+id+' input[type="text"]').after('<p>Please add some remark and than select call option</p>');
    }
}

function addRemark(id,val,rid){
    $.ajax({
        url : '/client/'+id,
        type : 'PUT',
        data : {'remark' : val,'id' : rid},
        success : function(data){
            // console.log(data);
        }
    });
}

function updateName(id,val){    
    $.ajax({
        url : '/client/'+id,
        type : 'PUT',
        data : {'name' : val},
        success : function(data){
            console.log(data);
        }
    });
}
function changeStatus(id,val){
    
    classes = ['','','','','','','','none','blue','green','orange','red', 'violet'];

    $("#tr-"+id).removeAttr('class').attr('class', classes[val]);

    $.ajax({
        url : '/client/'+id,
        type : 'PUT',
        data : {'status' : val},
        success : function(data){
            // console.log(data);
            $('#cl-'+id).html(data);
            $('#cl-'+id).attr('onclick', 'getStatus(this.id);');

        }
    });
}
function getName(id,val){        

    $('#'+id).removeAttr('onclick');

    var n = id.replace('c-','');
    var nam = 'name-'+n;    
    
    $.get('/client/'+n+'/edit', function(data) {

        $('#'+id).html('<input onkeyup = "updateName('+n+',this.value);" id = "nameIn-'+n+'" class = "form-control" type = "text" value = "'+data.c_name+'"/>');
        $('#nameIn-'+n).focus();
        
    },'json');    
}
function getStatus(id,val){

    $('#'+id).removeAttr('onclick');

    var n = id.replace('cl-','');
    var nam = 'status-'+n;    

    var sel = $('<select/>',{class : 'form-control', name : nam , onchange : 'changeStatus('+n+',this.value);'});
    $.get('/getStatus', function(data) {
        var op = ['<option value = "-1">-Select-</option>'];
        $.each(data, function (i, item) {
            op.push('<option value="'+item.sta_id+'">'+item.sta_name+'</option>');
        });
        sel.append(op);
        $('#'+id).html(sel);

    });    
}

$('#addClientBtn').on({
    'click' : function(){
        $('#clientForm')[0].reset();
        $('#formButton').html('<button type="submit" class="btn btn-primary" onclick = "event.preventDefault();sendData(\'add\', \'Successfully Inserted\');" id = "addClient">Register</button>');
        $('#addClientModal').modal('toggle');
        $('#clientForm').attr('onsubmit', 'event.preventDefault();');
        $('#emailBox').html('<input id="email" type="email" class="form-control" name="email[]" value = "">');
        $('#mobileBox').html('<input value = "" type="phone" class="form-control" name="mobile[]" required = "required">');
        $('#remarkBox').html('<input value = "" type="text" class="form-control" name="remark[]">');
        $('#cbtSelection').val('-1');
        $('#cbt').val('');
    }
});

// getData(4);

function getData(id){

    $.get('/client/'+id+'/edit', function(data){
        // console.log(data);
        $('#formButton').html('<button type="submit" class="btn btn-primary" onclick = "event.preventDefault();sendData(\'update\', \'Successfully Updated\');"id = "updateClient" >Update</button>');

        $('#clientForm').attr('onsubmit', 'event.preventDefault();sendData(\'update\', \'Successfully Updated\');');
        
        
        $('#addClientModal').modal('toggle');

        $('#id').val(data.c_id);
        $('#name').val(data.c_name);
        $('#bname').val(data.c_business);
        $('#address').val(data.c_address);
        $('#cbtSelection').val(data.c_business_type);
        $('#cbt').val(data.c_business_type);

        $.each(data.email,function(k,v){
            if(k>0){
                $('#emailBox').append('<input id="email" type="email" value = "'+v.ce_email+'" class="form-control text-margin-top" name="email[]"><input type="hidden" value = "'+v.ce_id+'" name="eid[]">');
            }else{
                $('#emailBox').html('<input id="email" type="email" class="form-control" name="email[]" value = "'+v.ce_email+'"><input type="hidden" value = "'+v.ce_id+'" name="eid[]">');
            }
        });

        $.each(data.mobile,function(k,v){
            num = v.cm_number == null || v.cm_number == '' ? '' : v.cm_number;
            if(k>0){
                $('#mobileBox').append('<input value = "'+num+'" type="phone" class="form-control text-margin-top" name="mobile[]"><input type="hidden" value = "'+v.cm_id+'" name="mid[]">');
            }else{
                $('#mobileBox').html('<input value = "'+num+'" type="phone" class="form-control" name="mobile[]"><input type="hidden" value = "'+v.cm_id+'" name="mid[]">');
            }
        });

        $.each(data.remark,function(k,v){
            if(k>0){
                $('#remarkBox').append('<input value = "'+v.cr_text+'" type="text" class="form-control text-margin-top" name="remark[]"><input type="hidden" value = "'+v.cr_id+'" name="rid[]">');
            }else{
                $('#remarkBox').html('<input value = "'+v.cr_text+'" type="text" class="form-control" name="remark[]"><input type="hidden" value = "'+v.cr_id+'" name="rid[]">');
            }
        });

    },'json');
}

function removeRemarks(id){
    $('#p-'+id).attr('onclick','getAllRemarks('+id+')'); 
    $.get('/getAllRemarks/'+id,function(d){       
        $('#remark'+id+'Box').html('<input type="text" class="form-control" onkeyup="addRemark('+id+',this.value);" value="'+d[0].cr_text+'">');
    });
}

function getAllRemarks(id){
    $('#p-'+id).attr('onclick','removeRemarks('+id+')');
    $.get('/getAllRemarks/'+id,function(d){
        var e = [];

        $.each(d,function(k,v){
            e.push('<input type="text" class="form-control text-margin-top" onkeyup="addRemark('+id+',this.value);" value="'+v.cr_text+'">');
        });
        $('#remark'+id+'Box').html(e);
    });
}

function removeMobiles(id){
    $('#pm-'+id).attr('onclick','getAllMobile('+id+')'); 

    $.get('/getAllMobile/'+id,function(d){               
        $('#mobile'+id+'Box').html('<div>'+d[d.length-1].cm_number+'</div>');
    });
}

function getAllMobile(id){
    $('#pm-'+id).attr('onclick','removeMobiles('+id+')');
    $.get('/getAllMobile/'+id,function(d){
        var e = [];

        $.each(d,function(k,v){
            e.push('<p>'+v.cm_number+'</p>');
        });
        $('#mobile'+id+'Box').html(e);
    });
}