$('#fresher').click(function() {
	if ($(this).is(":checked")) {
		$('#typeSection').removeClass('hidden');
		$('#exprienceSection').addClass('hidden');
	}
});

$('#exprienced').click(function() {        
	if ($(this).is(":checked")) {
		$('#typeSection').removeClass('hidden');
		$('#exprienceSection').removeClass('hidden');
	}
});

$('#highestSelection').on({
	'change' : function(){
		if(this.value == 'other'){
			$('#highestOther').removeClass('hidden');
		}else{
			$('#highestOther').addClass('hidden');
			$('#highest').val(this.value);
		}
	}
});

$('#addDegree').on({
	'click' : function(e){
		e.preventDefault();
		var d = $('#highestOther input').val();
		$.ajax({
			url : $url+'degree',
			type : 'post',
			data : {'degree_name' : d},
			success : function(data){
				$('#highest').val(data.degree_id);				
			}
		});
	}
});

$('#addCandidateModal').on({
	'click' : function(){
		$('#acandidateForm')[0].reset();
		$('#formButton').html('<button type="submit" class="btn btn-primary" id = "addCandidate">Register</button>');
		$('#addCanModal').modal('toggle');
		$('#acandidateForm').attr('onsubmit', 'event.preventDefault();sendData("add", "Successfully Added");');
	}
});

function viewRemark(id){
	$.get($url+'remark/'+id, function(d){
		$('#remarkModal').modal('toggle');
		$('#remarkPName').html(d.can_name);
		$('#remarkBody').html('<h2>'+d.can_remark+'</h2>');
	},'json');

}
$('#candidateForm').on({
	'submit' : function(e){
		e.preventDefault();
		$('#candidateMessage').removeClass('hidden').html('Registering, Please wait...');

		var fd = new FormData(this);
		$.ajax({
			url : '/candidate',
			type : 'post',
			data : fd,
			processData : false,
			contentType : false,
			success : function(data){

				if(data.indexOf('OK') >= 0){
					$('#candidateMessage').html('Successfully registered. <a class = "btn btn-primary" href = "'+$url+'candidate/test/start"> Start Test </a>');
				}else{
					$('#candidateMessage').html('Something went wrong.');
				}
			}
		});
	}
});

$('#mobile').on({
	'keyup' : function () {
		if(this.value.length > 4){
			$.ajax({
				url : $url+'searchMobile',
				type : 'post',
				data : {mobile : this.value},
				success : function (data) {
					if(data > 0){
						$('#mobileMsg').html('<span>Exsist</span>');
					}else{
						$('#mobileMsg').html('');
					}
				}
			});
		}

	}
});
$('#addCandidate').on({
	'click' : function(e){
		e.preventDefault();

		sendData('add', 'Successfully Inserted',e);
	}
});
$('#updateCandidate').on({
	'click' : function(e){
		e.preventDefault();
		sendData('update', 'Successfully Updated',this);
	}
});

function deleteCandidate(id){
	$.ajax({
		url : $url+'candidate/'+id,
		type : 'delete',
		success : function(data){
			if(data.indexOf('OK') >= 0){
				$('#user_role_table').DataTable().ajax.reload();
			}
		}
	});
}

function sendData(p, msg){

	var d = '/candidate';
	
	$('#acandidateMessage').removeClass('hidden').html('Registering, Please wait...');
	
	var fd = new FormData($('#acandidateForm')[0]);
	
	if(p == 'update'){
		fd.append('_method','PUT');
		d += '/'+fd.get('id');
	}
	
	$.ajax({
		url : d,
		type : 'post',
		data : fd,
		processData : false,
		contentType : false,
		success : function(data){
			console.log(data);
			if(data.indexOf('OK') >= 0){
				$('#acandidateMessage').html(msg);
				window.setTimeout(function () {
					$('#acandidateMessage').addClass('hidden');
				},3000);
				$('#acandidateForm')[0].reset();
				if($('#user_role_table').length){
					$('#user_role_table').DataTable().ajax.reload();
				}
				$('#addCanModal').modal('toggle');
			}else if(data.indexOf('exist') >= 0){
				$('#acandidateMessage').html('Record Exist');
			}else{
				$('#acandidateMessage').html('Something went wrong.');
			}
		}
	});
}