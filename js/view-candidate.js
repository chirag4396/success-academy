
$.fn.dataTable.ext.search.push(function( settings, data, dataIndex ) {

    var designation = $('#sDesignation').val();

    if(designation == data[7]){
        return true;
    }
    return false;            
});

$(document).ready(function() {

    var table = $('#user_role_table').DataTable({
        processing: true,        
        serverSide: true,
        // scrollX : true,
        ajax: {
            url : '/getDatatable',
            type : 'post',
            data : function(d){
                d.data = $('#sDesignation').val();
            }
        },
        "language": {
            "processing": "Hang on. Waiting for response..."
        },
        columns: [
        { data: 'can_id', name: 'can_id', visible : false },
        { data: 'can_name', name: 'can_name' },
        { data: 'can_experience', name: 'can_experience' },
        { data: 'can_mobile', name: 'can_mobile' },
        { data: 'can_remark', name: 'can_remark' },
        { data: 'can_status', name: 'can_status'},
            {},
        { data: 'can_designation', name: 'can_designation' , visible : false},
            { data: 'can_updated_at', name: 'can_updated_at' , visible : false}
        ],
        'columnDefs': [{
            'targets': 0,
            'searchable': false,
            'orderable': false,
            'className': 'dt-body-center',
            'render': function (data, type, full, meta){
                return '<input type="checkbox" name="id[]" value="' + $('<div/>').text(data).html() + '">';
            }
        },{
            'targets': 2,
            'className': 'dt-body-center',
            'render': function (data, type, full, meta){

                return full.can_experience == '' || full.can_experience == null || full.can_experience == 0 ? 'Fresher' : full.can_experience;
            }
        },{
            'targets': 4,
            'className': 'dt-body-center',
            'render': function (data, type, full, meta){
                // return '<input type="text" class="form-control" onkeyup="addRemark('+full.can_id+',this.value);" value="'+$('<div/>').text(data).html()+'">';
                return '<div class="input-group">'
                        +'<input type="text" class="form-control" onkeyup="addRemark('+full.can_id+',this.value);" value="'+$('<div/>').text(data).html()+'">'
                            +'<div class="input-group-btn">'
                                +'<button type="button" class="btn btn-default" onclick = "viewRemark('+full.can_id+');" ><i class="fa fa-eye"></i></button>'
                            +'</div>'
                        +'</div> ';
            }
        },{
            'targets': 5,
            'className': 'dt-body-center',
            'render': function (data, type, full, meta){
                return '<div onclick = "getStatus(this.id,$(this).html());" id = "can-'+full.can_id+'">'+full.status.sta_name+'</div>';
            }
        },{
            'targets': 6,
            'className': 'dt-body-center',
            'render': function (data, type, full, meta){

                return '<button class = "btn btn-success" onclick = "getData('+full.can_id+');">'
                        +'<i class = "fa fa-pencil-square-o"></i>'
                    +'</button> | '
                    +'<button class = "btn btn-danger" onclick = "deleteCandidate('+full.can_id+');">'
                        +'<i class = "fa fa-trash-o"></i>'
                    +'</button> | '
                    +'<button class = "btn btn-link '+(full.can_called == 0 ? 'callRed' : 'callGreen') +'" onclick="callStatus('+full.can_id+','+full.can_called+', this);">'
                        +'<i class = "fa fa-phone"></i>'
                    +'</button>';
            }
        }],
        "createdRow": function( row, data, dataIndex ) {

            $(row).attr('id', 'tr-'+data.can_id).addClass(data.status.sta_color);
        }
        /*,"aoColumnsDefs": [
            null,null,null,null,null,null,null,null
        ]*/
    });


    $('#sDesignation').on({
        "change": function() {
            table.ajax.reload();
        }
    });

    $('#example-select-all').on('click', function(){

        var rows = table.rows({ 'search': 'applied' }).nodes();

        $('input[type="checkbox"]', rows).prop('checked', this.checked);
    });

    $('#example tbody').on('change', 'input[type="checkbox"]', function(){

        if(!this.checked){
            var el = $('#example-select-all').get(0);

            if(el && el.checked && ('indeterminate' in el)){
                el.indeterminate = true;
            }
        }
    });

    $('#frm-example').on('submit', function(e){
        var form = this;

        table.$('input[type="checkbox"]').each(function(){

            if(!$.contains(document, this)){

                if(this.checked){

                    $(form).append(
                        $('<input>')
                        .attr('type', 'hidden')
                        .attr('name', this.name)
                        .val(this.value)
                        );
                }
            }
        });
    });
});

function callStatus(id,val,d) {
    if($('#tr-'+id+' input[type="text"]').val().length > 0){
        $('#tr-'+id+' p').remove();
        $.ajax({
            url : '/candidate/'+id,
            data : {called : val, '_method' : 'put'},
            type : 'post',
            success : function (data) {
                if(data == 1){
                    $(d).removeClass('callRed').addClass('callGreen');
                }else{
                    $(d).removeClass('callGreen').addClass('callRed');
                }
                $('#user_role_table').DataTable().ajax.reload();
                fetch_notifications();
            }
        });
    }else{
        $('#tr-'+id+' input[type="text"]').after('<p>Please add some remark and than select call option</p>');
    }
}

function addRemark(id,val){
    $.ajax({
        url : '/candidate/'+id,
        type : 'PUT',
        data : {'remark' : val},
        success : function(data){
            // console.log(data);
        }
    });
}
function changeStatus(id,val){

    classes = ['','none','blue','green','orange','red', 'violet'];

    $("#tr-"+id).removeAttr('class').attr('class', classes[val]);

    $.ajax({
        url : '/candidate/'+id,
        type : 'PUT',
        data : {'status' : val},
        success : function(data){
            // console.log(data);
            $('#can-'+id).html(data);
            $('#can-'+id).attr('onclick', 'getStatus(this.id);');

        }
    });
}

function getStatus(id,val){

    $('#'+id).removeAttr('onclick');

    var n = id.replace('can-','');
    var nam = 'status-'+n;    

    var sel = $('<select/>',{class : 'form-control', name : nam , onchange : 'changeStatus('+n+',this.value);'});
    $.get('/getStatus', function(data) {
        var op = ['<option value = "-1">-Select-</option>'];
        $.each(data, function (i, item) {
            op.push('<option value="'+item.sta_id+'">'+item.sta_name+'</option>');
        });
        sel.append(op);
        $('#'+id).html(sel);

    });    
}
function getData(id){

    $.get('/candidate/'+id+'/edit', function(data){
        $('#formButton').html('<button type="submit" class="btn btn-primary" id = "updateCandidate" >Update</button>');

        $('#acandidateForm').attr('onsubmit', 'event.preventDefault();sendData("update", "Successfully updated");');
        $('#addCanModal').modal('toggle');

        $.each($('#acandidateForm')[0],function(k,v){            
            if(v.id){
                i = "#"+v.id;
                val = data['can_'+v.id];
                if(v.id == "highest" || v.id == "designation" || v.id == "passout_yearSelection"){
                    // console.log($(id)[0]);
                    if(v.id == "passout_yearSelection"){
                        $(i).prop('selectedIndex', val ? val : 0);
                    }else{

                        $(i+'Selection').prop('selectedIndex', val ? val : 0);
                        $(i).val(val);
                    }
                }else{
                    $('#'+v.id).val(val);
                }
            }
        })
    },'json');
}
