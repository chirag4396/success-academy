function addInput(id,i = null){
	if(i != null){
		$.get('/getRemarkBox/'+i, function(data){
			$('#'+id+'Box').append('<input type="text" class="form-control text-margin-top" onkeyup="addRemark(\''+id+'\',this.value,\''+data+'\');" >');

		});
	}else{
		var re = (id == 'mobile')? 'required' : '';
		$('#'+id+'Box').append('<input type="text" class="form-control text-margin-top" '+re+' name="'+id+'[]">');
	}
}
$('#cbtSelection').on({
	'change' : function(){
		if(this.value == 'other'){
			$('#cbtOther').removeClass('hidden');
		}else{
			$('#cbtOther').addClass('hidden');
			$('#cbt').val(this.value);
		}
	}
});

$('#addCbt').on({
	'click' : function(e){
		e.preventDefault();
		var d = $('#cbtOther input').val();
		$.ajax({
			url : '/addBType',
			type : 'post',
			data : {'cbt_title' : d},
			success : function(data){
				
				$('#cbt').val(data.cbt_id);
				$('#cbtSelection').append('<option value = "'+data.cbt_id+'">'+data.cbt_title+'</option>');				
				$('#cbtSelection').val(data.cbt_id).change();
				$('#cbtOther input').val('');
				$('#cbtOther').addClass('hidden');			
			}
		});
	}
});
function deleteT(d,e){
	
	$('#deleteData').html(e);
	$('#deleteModal').modal('toggle');
	$('#delete').attr('data-value',d);
}
function deleteClient(id){
	
	$.ajax({
		url : '/client/'+id,
		type : 'delete',
		success : function(data){
			if(data.indexOf('OK') >= 0){

                $('#user_role_table').DataTable().ajax.reload();
                $('#deleteModal').modal('toggle');
			}
		}
	});
}


function sendData(p, msg){

    var d = '/client';
	
	$('#clientMessage').removeClass('hidden').html('Registering, Please wait...');
	
	var fd = new FormData($('#clientForm')[0]);

	if(p == 'update'){
		fd.append('_method','PUT');
		d += '/'+fd.get('id');
	}

	if(fd.get('mobile[]') == ''){
        $('#mobileBox').append('<span style="color: #d00404;">Mobile Number is Mandatory</span>');
        window.setTimeout(function () {
            $('#mobileBox span').remove();
        }, 4000);
	}else {


        $.ajax({
            url: d,
            type: 'post',
            data: fd,
            processData: false,
            contentType: false,
            success: function (data) {
                console.log(data);
                if (data.indexOf('OK') >= 0) {
                    $('#clientMessage').removeClass('hidden').html(msg);
                    $('#clientForm')[0].reset();
                    $('#addClientModal').modal('toggle');
                    if ($('#user_role_table').length) {
                        $('#user_role_table').DataTable().ajax.reload();
                    }
                } else {
                    $('#clientMessage').removeClass('hidden').html('Something went wrong.');
                }

                window.setTimeout(function () {
                    $('#clientMessage').addClass('hidden');
                }, 4000);
            }, error: function (e) {
                $('#clientMessage').removeClass('hidden').html('Something went wrong.');
                window.setTimeout(function () {
                    $('#clientMessage').addClass('hidden');
                }, 4000);
            }
        });
    }
	// $.ajax({
	// 	url : d,
	// 	type : 'post',
	// 	data : fd,
	// 	processData : false,
	// 	contentType : false,
	// 	success : function(data){
	// 		console.log(data);
	// 		if(data.indexOf('OK') >= 0){
	// 			$('#acandidateMessage').html(msg);
 //                window.setTimeout(function () {
 //                    $('#acandidateMessage').addClass('hidden');
 //                },3000);
 //                $('#acandidateForm')[0].reset();
 //                if($('#user_role_table').length){
 //                    $('#user_role_table').DataTable().ajax.reload();
 //                }
	// 			$('#addCanModal').modal('toggle');
	// 		}else if(data.indexOf('exist') >= 0){
 //                $('#acandidateMessage').html('Record Exist');
 //            }else{
	// 			$('#acandidateMessage').html('Something went wrong.');
	// 		}
	// 	}
	// });
}
// $('#clientForm').on({
// 	'submit' : function(e){
// 		e.preventDefault();

// 		var fd = new FormData(this);
// 		$.ajax({
// 			url : '/client',
// 			type : 'post',
// 			data : fd,
// 			processData : false,
// 			contentType : false,
// 			success : function(data){
// 				if(data.indexOf('OK') >= 0){
// 					$('#clientMessage').removeClass('hidden').html('Successfully registered.');
// 					$('#clientForm')[0].reset();
// 				}else{
// 					$('#clientMessage').removeClass('hidden').html('Something went wrong.');
// 				}

// 				window.setTimeout(function(){
// 					$('#clientMessage').addClass('hidden');
// 				},4000);
// 			}
// 		});
// 	}
// });
// $('#addClient').on({
// 	'click' : function(e){
//         e.preventDefault();

//         sendData('add', 'Successfully Inserted');
// 	}
// });
// $('#updateClient').on({
// 	'click' : function(e){
// 		e.preventDefault();
// 		console.log('hi');
// 		sendData('update', 'Successfully Updated');
// 	}
// });
$('#email').on({
	'keyup' : function(){
		$.ajax({
			url : '/checkEmail',
			type : 'post',
			data : {val : this.value},
			success : function(data){
				if(data[1].indexOf('OK') >= 0){
					$('#emailBox span').html('Email is already Registered with <b>'+data[0].client.c_business+'</b>');
				}else{
					$('#emailBox span').html('');
				}
			}
		});
	}
});