$('#batchSelection').on({
	'change' : function(){
		if(this.value == 'other'){
			$('#batchOther').removeClass('hidden');
		}else{
			$('#batchOther').addClass('hidden');
			$('#batch').val(this.value);
		}
	}
});

$('#addBatch').on({
    'click' : function(e){
        e.preventDefault();
        $('#batchMsg').removeClass('hidden').html('Check for Batch name, Please wait!!');

        var d = $('#batchOther input').val();
        $.ajax({
            url : $url+'/batch',
            type : 'post',
            data : {'batch_name' : d},
            success : function(data){
                if(data == "exist"){
                    $('#batchMsg').removeClass('hidden').html('Batch name already exist!!');
                }else{
                    $('#batchMsg').addClass('hidden').html('');
                    $('#batch').val(data.batch_id);
                    $('#batchSelection').append('<option value="'+data.batch_id+'">'+data.batch_name+'</option>');
                    $('#batchSelection').val(data.batch_id);
                    $('#batchOther').addClass('hidden');
                }
            }
        });
    }
});

$('#candidateForm').on({
	'submit' : function(e){
		e.preventDefault();
		$('#candidateMessage').removeClass('hidden').html('Registering, Please wait...');

		var fd = new FormData(this);
        if(fd.get('batch') == -1){
            $('#candidateMessage').removeClass('hidden').html('Please select Batch for these new Student');
        }else {
            $.ajax({
                url: $url + 'register',
                type: 'post',
                data: fd,
                processData: false,
                contentType: false,
                success: function (data) {
                    console.log(data);
                    if (data.indexOf('OK') >= 0) {
                        $('#candidateMessage').html('Successfully registered.');
                        window.setTimeout(function () {
                            location.href = $url+'candidate/test';
                        }, 2000);

                    } else {
                        $('#candidateMessage').html('Something went wrong.');
                    }
                }
            });
        }
	}
});
$('#editForm').on({
    'submit' : function(e){
        e.preventDefault();
        $('#editMessage').removeClass('hidden').html('Registering, Please wait...');

        var fd = new FormData(this);
        if(fd.get('batch') == -1){
            $('#editMessage').removeClass('hidden').html('Please select Batch for these existing Student');
        }else {
            $.ajax({
                url: $url + '/candidates/test/edit',
                type: 'post',
                data: fd,
                processData: false,
                contentType: false,
                success: function (data) {
                    // console.log(data);
                    if (data.indexOf('OK') >= 0) {
                        $('#editMessage').html('Successfully Updated.');
                        window.setTimeout(function () {
                            location.reload();
                        }, 2000);

                    } else {
                        $('#editMessage').html('Something went wrong.');
                    }
                }
            });
        }
    }
});

// $('#mobile').on({
// 	'keyup' : function () {
// 		if(this.value.length > 4){
// 			$.ajax({
// 				url : $url+'candidate/searchMobile',
// 				type : 'post',
// 				data : {mobile : this.value},
// 				success : function (data) {
// 					// console.log(data);
// 					if(data > 0){
// 						$('#mobileMsg').removeClass('hidden').html('Exist');
// 					}else{
// 						$('#mobileMsg').addClass('hidden').html('');
// 					}
// 				}
// 			});
// 		}

// 	}
// });

function deleteCandidate(id){
	$.ajax({
		url : $url+'/student/delete/'+id,
		type : 'delete',
		success : function(data){
			if(data.indexOf('OK') >= 0){
				$('#user_role_table').DataTable().ajax.reload();
			}
		}
	});
}