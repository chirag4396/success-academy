$('#questionForm').on({
	'submit' : function(e){
		e.preventDefault();
		var value = CKEDITOR.instances['question'].getData();
		var sol = CKEDITOR.instances['solution'].getData();
		var value1 = CKEDITOR.instances['editor1'].getData();
		var value2 = CKEDITOR.instances['editor2'].getData();
		var value3 = CKEDITOR.instances['editor3'].getData();
		var value4 = CKEDITOR.instances['editor4'].getData();
		// console.log(value);
		$('#questionMessage').removeClass('hidden').html('Adding, Please wait...');
		// $('#questionMessage').removeClass('hidden').html(value);

		var fd = new FormData(this);
		fd.append('detail',value);
		fd.append('solution',sol);
		fd.append('option[]',value1);
		fd.append('option[]',value2);
		fd.append('option[]',value3);
		fd.append('option[]',value4);

		if(fd.get('answer') == null){
			$('#questionMessage').html('Please select Correct Answer by selecting Radio Button.');
			
		}else{

			$.ajax({
				url : $path,
				type : 'post',
				data : fd,
				processData : false,
				contentType : false,
				success : function(data){

					console.log(data);

					if(data.indexOf('OK') >= 0){
						$('#questionMessage').html('Successfully Added.');
						window.setTimeout(function(){
							$('#questionMessage').addClass('hidden');
							$('#questionForm')[0].reset();
							location.reload();
						},1000);
					}else{
						$('#questionMessage').html('Something went wrong.');
					}
				}
			});
		}

	}
});
$('#editForm').on({
	'submit' : function(e){
		e.preventDefault();		
		$('#editMessage').removeClass('hidden').html('Editing, Please wait...');

		var fd = new FormData(this);
		var value = CKEDITOR.instances['question'].getData();
		var sol = CKEDITOR.instances['solution'].getData();

		$('#options textarea').each(function (k,v) {            		    
			var value = CKEDITOR.instances[this.id].getData();
			fd.append('option['+this.id.split('-')[1]+']',value);			
		}); 		
		
		fd.append('detail',value);		
		fd.append('solution',sol);
		fd.append('_method','PATCH');
		$.ajax({
			url : $url+'/question/'+fd.get('qus_id'),
			type : 'post',
			data : fd,
			processData : false,
			contentType : false,
			success : function(data){

				// console.log(data);
				
				if(data.indexOf('OK') >= 0){
					$('#editMessage').html('Successfully Edited.');
					window.setTimeout(function(){
						$('#editMessage').addClass('hidden');
						location.reload();						
					},1000);
				}else{
					$('#editMessage').html('Something went wrong.');
				}
			}
		});
	}
});
