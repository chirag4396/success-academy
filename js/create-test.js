$('#batchSelection').on({
	'change' : function(){
		if(this.value == 'other'){
			$('#batchOther').removeClass('hidden');
		}else{
			$('#batchOther').addClass('hidden');
			$('#batch').val(this.value);

		}
	}
});

$('#addBatch').on({
	'click' : function(e){
		e.preventDefault();
        $('#batchMsg').removeClass('hidden').html('Check for Batch name, Please wait!!');

        var d = $('#batchOther input').val();
		$.ajax({
			url : $url+'/batch',
			type : 'post',
			data : {'batch_name' : d},
			success : function(data){
				if(data == "exist"){
					$('#batchMsg').removeClass('hidden').html('Batch name already exist!!');
				}else{
                    $('#batchMsg').addClass('hidden').html('');
                    $('#batch').val(data.batch_id);
                    $('#batchSelection').append('<option value="'+data.batch_id+'">'+data.batch_name+'</option>');
                    $('#batchSelection').val(data.batch_id);
                    $('#batchOther').addClass('hidden');
                }
			}
		});
	}
});


$('#patternSelection').on({
	'change' : function(){
		if(this.value == 1){
			$('#patternMark').removeClass('hidden');
			$('#markBox').attr('required', true);
		}else{
			$('#patternMark').addClass('hidden');
            $('#markBox').attr('required', false);
		}
	}
});
// $('#testForm').on({
// 	'submit' : function (e) {
// 		e.preventDefault();

// 		var fd = new FormData(this);

// 		$('#testMessage').removeClass('hidden').html('Creating Test, Please wait...');
// 		if(fd.get('batch') == -1){
//             $('#testMessage').removeClass('hidden').html('Please select Batch for these Test');
// 		}else {

//             $.ajax({
//                 url: $url + '/test',
//                 type: 'post',
//                 data: fd,
//                 processData: false,
//                 contentType: false,
//                 success: function (data) {
//                     console.log(data);
//                     if (data != 'NO') {
//                         $('#testMessage').html('Successfully Created. <a href = "' + $url + '/add-question/' + data + '" class = "btn btn-success">Now add some question</a>');
//                         // window.setTimeout(function () {
//                         // 	location.reload();
//                         // },2000);

//                     } else {
//                         $('#testMessage').html('Something went wrong.');
//                     }
//                 }
//             });
//         }
// 	}
// });