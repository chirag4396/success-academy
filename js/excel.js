$('#subBtn').on('click', function(){
	$("#loader").show();
});
$('#designationSelection').on({
	'change' : function(){
		if(this.value == 'other'){
			$('#desOther').removeClass('hidden');
		}else{
			$('#desOther').addClass('hidden');
			$('#designation').val(this.value);
		}
	}
});

$('#addDes').on({
	'click' : function(e){
		e.preventDefault();
		var d = $('#desOther input').val();
		$.ajax({
			url : '/designation',
			type : 'post',
			data : {'des_name' : d},
			success : function(data){
				// console.log(data);
				$('#designation').val(data.des_id);
				// $('#highestSelection').append(newOption);
				// $('#highestSelection').trigger("chosen:updated");
			}
		});
	}
});