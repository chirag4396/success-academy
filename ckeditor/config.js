/**
* @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
* For licensing, see LICENSE.md or http=//ckeditor.com/license
*/

//  CKEDITOR.editorConfig = function( config ) {
// 	// Define changes to default configuration here. For example=
// 	// config.language = 'fr';
// 	// config.uiColor = '#AADC6E';
// 	config.extraPlugins = 'uploadimage';
// 	// config.uploadUrl = '/ckeditor/upload.php?type=Images&responseType=json';
// 	config.imageUploadUrl = '/ckeditor/upload.php?command=QuickUpload&type=Images&responseType=json';

// 	config.filebrowserUploadUrl = '/ckeditor/upload.php';
// };

CKEDITOR.editorConfig = function( config ) {
	config.toolbarGroups = [
	{ name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
	{ name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
	{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
	{ name: 'forms', groups: [ 'forms' ] },
	{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
	{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
	{ name: 'links', groups: [ 'links' ] },
	{ name: 'insert', groups: [ 'insert' ] },
	{ name: 'styles', groups: [ 'styles' ] },
	{ name: 'colors', groups: [ 'colors' ] },
	{ name: 'tools', groups: [ 'tools' ] },
	{ name: 'others', groups: [ 'others' ] },
	{ name: 'about', groups: [ 'about' ] }
	];

	config.removeButtons = 'Source,Save,NewPage,Preview,Print,Templates,Cut,PasteText,Copy,Redo,Undo,SelectAll,Form,Checkbox,Radio,TextField,Textarea,Select,Button,HiddenField,CreateDiv,Language,BidiLtr,BidiRtl,Flash,HorizontalRule,Smiley,PageBreak,Iframe,ShowBlocks,About';
	config.extraPlugins += (config.extraPlugins.length == 0 ? '' : ',') + 'ckeditor_wiris,font,filebrowser,uploadimage,image2,autogrow';
	// config.height= 300;
	config.autoGrow_minHeight = 20;
	config.autoGrow_maxHeight = 600;
	// config.fontSize_sizes = '20px;';
	/*Upload images to a CKFinder connector (note that the response type is set to JSON).*/
	config.uploadUrl= '/ckeditor/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json';	
	/*Configure your file manager integration. This example uses CKFinder 3 for PHP.*/
	config.filebrowserBrowseUrl= '/ckeditor/ckfinder.html';
	config.filebrowserImageBrowseUrl= '/ckeditor/ckfinder.html?type=Images';
	config.filebrowserUploadUrl= '/ckeditor/core/connector/php/connector.php?command=QuickUpload&type=Files';
	config.filebrowserImageUploadUrl= '/ckeditor/core/connector/php/connector.php?command=QuickUpload&type=Images';
	/*The following options are notnecessary and are used here for presentation purposes only.
	They configure the Styles drop-down list and widgets to use classes.*/

	stylesSet= [
	{ name: 'Narrow image', type:'widget', widget: 'image', attributes: { 'class': 'image-narrow' } },
	{ name: 'Wide image', type: 'widget', widget: 'image', attributes: { 'class': 'image-wide' } }
	];
	config.allowedContent = {
	$1: {
	    elements: CKEDITOR.dtd,
	    attributes: true,
	    styles: true,
	    classes: true
	   }   
	};
	config.disallowedContent = 'p h1 h2{line*,margin*,text-align*}; *[dir]';
	/*Load the default contents.css file plus customizations for this sample.*/
	contentsCss= [ CKEDITOR.basePath + 'contents.css', 'https://sdk.ckeditor.com/samples/assets/css/widgetstyles.css' ];

// Configure the Enhanced Image plugin to use classes instead of styles and to disable the
// resizer (because image size is controlled by widget styles or the image takes maximum
// 100% of the editor width).
config.image2_alignClasses= [ 'image-align-left', 'image-align-center', 'image-align-right' ];
// config.image2_disableResizer= true;
} ;